<div id="section-header">
	<div class="background-grey">
		<div class="container" style="width: 100%">
			<div class="row">
				<div class="col-xs-12">
					<div class="up15 margin-bottom-15">
						<h6 class="free-shipping-note margin-bottom-15 white font6">FREE SHIPPING WITH MINIMUM PURCHASE OF IDR 500,000!</h6>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="header-nav background-white hidden-xs">
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-sm-2 col-xs-12">
					<a href="index2.php" class="margin-left-50">
						<img src="<?php echo $global['absolute-url'];?>img/source/hh.png" width="80px" height="80px" class="up5">
					</a>		
				</div>
				<div class="row">
					<div class="col-md-6 col-sm-8 col-xs-12 padding-left-50">
						<div class="nav marg2">
							<ul class="margin-left-min40 center pad0">
								<li>
									<a href="#"><img src="<?php echo $global['absolute-url'];?>img/line.png" class="icon"></a>
								</li>
								<li class="list1 margin-left-10">
									|
								</li>
								<li class="list1 margin-left-10">
									<a href="#">Sign Up</a>
								</li>
								<li class="list1 margin-left-10">
									|
								</li>
								<li class="list1 margin-left-10">
									<a href="#">Log In</a>
								</li>
								<li class="list1 margin-left-10">
									|
								</li>
								<li class="list1 margin-left-10">
									<a href="#"><img src="<?php echo $global['absolute-url'];?>img/cart.png" class="icon"> Cart</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="row padding-top-65 margin-bottom-xs-0 navMenu2">
						<div class="col-md-9 col-sm-10 col-xs-12">
							<div class="navMenu up4 padding-top-5 margin-top-xs-min30">											
								<ul>
									<li class="list2"><a href="">ABOUT US</a></li>
									<li class="list2 margin-left-20"><a href="lookbook.php">LOOKBOOK</a></li>
									<li class="list2 margin-left-20"><a href="blog.php">BLOG</a></li>
									<li class="dropdown dropdown-product closed list2 margin-left-20">
										<a href="#" data-toggle="dropdown">SHOP<i class="nav-caret"></i></a>
										<ul class="dropdown-menu dropdown-menuproduct">
											<li>
												<a href="#">Blush On</a>
											</li>
											<li>
												<a href="#">Hand Lotion</a>
											</li>
											<li>
												<a href="#">Lipstick</a>
											</li>
											<li>
												<a href="#">Make Up Powder</a>
											</li>
										</ul>
									</li>
									<li class="list2 margin-left-20"><a href="#">WOMAN TALK</a></li>
									<li class="dropdown dropdown-search tools-search closed list2 margin-left-20">
										<a href="#" data-toggle="dropdown" class="search-link" aria-expanded="true">
											<i class="fa fa-search"></i>
										</a>
										<div class="dropdown-menu dropdown-menusearch">
											<form name="search" class="form-search" enctype="multipart/form-data" method="get">
												<div class="desktop-search">
													<input type="text" id="input-search" class="input-search" name="q" placeholder="Search Product" required="">
													<i img src="<?php echo $global['absolute-url'];?>img/search.png"></i>
												</div>
											</form>
										</div>
									</li>
								</ul>
							</div>
						</div>	
					</div>	
				</div>
			</div>
		</div>
	</div>
	<div class="header-mobile visible-xs">
		<div id="mobile-nav">
			<div class="mobile-top">
				<div class="row">
					<div class="col-xs-6">
						<div class="logo-mobile">
							<a href="index2.php">
								<img src="<?php echo $global['absolute-url'];?>img/source/hh.png" alt="They Talk About">
							</a>
						</div>
					</div>
					<div class="col-xs-6">
						<div class="menu-nav right">
							<a href="#" class="icon">
								<img src="<?php echo $global['absolute-url'];?>img/line.png" class="icon margin-right-5">
							</a>
							<a href="#" class="icon">
								<img src="<?php echo $global['absolute-url'];?>img/cart.png" class="icon margin-right-5">
							</a>
							<a id="menu" href="javascript:void(0);">
								<img id="menu-bar" src="<?php echo $global['absolute-url'];?>img/menu-button.png" height="20px" width="20px" class="menu-icon up03 margin-right-5" alt="icon">
								<img id="menu-cross" src="<?php echo $global['absolute-url'];?>img/multiply.png" height="20px" width="20px" class="menu-icon up03 hide" alt="icon">
							</a>
						</div>
					</div>
				</div>
			</div>
			<div id="mobile-menu" class="mobile-content in" style="display: none;">
				<div class="mobile-list">
					<ul class="bold">
						<li>
							<a href="#" class="mobile-link">LOG IN</a>
						</li>
						<li>
							<a href="#" class="mobile-link">SIGN UP</a>
						</li>
						<li>
							<a href="#" class="mobile-link">ABOUT US</a>
						</li>
						<li>
							<a href="lookbook.php" class="mobile-link">LOOKBOOK</a>
						</li>
						<li>
							<a href="blog.php" class="mobile-link">BLOG</a>
						</li>
						<li>
							<a href="#" class="mobile-link">SHOP</a>
						</li>
						<li>
							<a href="#" class="mobile-link">WOMAN TALK</a>
						</li>
					</ul>
				</div>
				<div class="dropdown-mobsearch">
					<form name="search" enctype="multipart/form-data" method="get">
						<input type="text" class="input-search" name="q" placeholder="What you want to find" required="">
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$('#menu').click(function(){
		$(".menu-icon").toggleClass("hide");
		$("#mobile-menu").slideToggle();
		$("#mobile-menu").toggleClass("in");
	});
	$('#mobile-shop').click(function(){
		$(".menu-icon").toggleClass("hide");
		$("#mobile-menu").slideToggle();
		$("#mobile-menu").toggleClass("in");
		$(".slide-shoplayer").toggle();
		$("#slide-shop").show("slide", { direction: "left" }, 500);
	});
	$(".close-shop").click(function(){
		$("#slide-shop").hide("slide", { direction: "left" }, 500);
		$(".slide-shoplayer").toggle();
	});
	$(".slide-shoplayer").click(function(){
		$("#slide-shop").hide("slide", { direction: "left" }, 500);
		$(".slide-shoplayer").toggle();
	});
	$('.dropdown-menu').click(function(event){
		event.stopPropagation();
	});
	$(document).ready(function() {
		$(".select-brand").select2({
			placeholder: "Search Brand",
			allowClear: true
		});
		$(document.body).on("change",".select-brand",function(){
			if(this.value != "" && this.value != null){
				window.location.href = this.value;
			}
		});
	});
	$(".dropdown-search a").click(function(){
		setTimeout(function(){
			if($(".dropdown-search").hasClass("closed")){
				$("#text-search").focus();
			}
		}, 300);
	});
;</script>