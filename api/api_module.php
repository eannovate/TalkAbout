<?php 
header('content-type: application/json');
header("access-control-allow-origin: *");

if(isset($_GET['action'])){//start gate

	require_once("../model/Connection.php");
	$obj_connect = new Connection();

	require_once("../model/Module.php");
	$obj_module = new Module();

	require_once("../model/Tag.php");
	$obj_tag = new Tag();

	//===================================== get homepage ========================================
	//start get homepage
	if($_GET['action'] == 'get_homepage'){//get_homepage
		$obj_connect->up();
		$R_message = array("status" => "400", "message" => "Data not found");

		$N_page = isset($_REQUEST['page']) ? mysql_real_escape_string($_REQUEST['page']) : 1;
		$result = $obj_module->get_homepage($N_page);
		//var_dump($result);
		if(is_array($result)){
			$total_data = $result[0]['total_data_all'];
			if($N_page <= 1){
				$itemperpage = 6;
				$remaining = $total_data - ((($N_page-1) * $itemperpage) + count($result));
			}else{
				$itemperpage = 3;
				$remaining = $total_data - ((($N_page-1) * $itemperpage + $itemperpage) + count($result));
			}	
			$R_message = array("status" => "200", "message" => "Data Exist", "num_data" => count($result), "remaining" => $remaining, "data" => $result);
		}	
	
		$obj_connect->down();		
		echo json_encode($R_message);
	}//end get homepage

	//===================================== get search ========================================
	//start get search
	else if($_GET['action'] == 'get_search'){//get_search
		$obj_connect->up();
		$R_message = array("status" => "400", "message" => "Data not found");

		$N_page = isset($_REQUEST['page']) ? mysql_real_escape_string($_REQUEST['page']) : 1;
		$N_keyword = mysql_real_escape_string($_REQUEST['keyword']);

		$result = $obj_module->get_search($N_page, $N_keyword);
		//var_dump($result);
		if(is_array($result)){
			$total_data = $result[0]['total_data_all'];
			if($N_page <= 1){
				$itemperpage = 6;
				$remaining = $total_data - ((($N_page-1) * $itemperpage) + count($result));
			}else{
				$itemperpage = 3;
				$remaining = $total_data - ((($N_page-1) * $itemperpage + $itemperpage) + count($result));
			}	
			$R_message = array("status" => "200", "message" => "Data Exist", "num_data" => count($result), "remaining" => $remaining, "data" => $result);
		}	
	
		$obj_connect->down();		
		echo json_encode($R_message);
	}//end get search

	//===================================== get tag ========================================
	//start get tag
	else if($_GET['action'] == 'get_tag'){//get_tag
		$obj_connect->up();
		$R_message = array("status" => "400", "message" => "Data not found");

		$N_page = isset($_REQUEST['page']) ? mysql_real_escape_string($_REQUEST['page']) : 1;
		$N_slug = mysql_real_escape_string($_REQUEST['slug']);

		$tag_id = $obj_tag->get_id_by_slug($N_slug);
		$result = $obj_module->get_tag($N_page, $tag_id);
		//var_dump($result);
		if(is_array($result)){
			$total_data = $result[0]['total_data_all'];
			if($N_page <= 1){
				$itemperpage = 6;
				$remaining = $total_data - ((($N_page-1) * $itemperpage) + count($result));
			}else{
				$itemperpage = 3;
				$remaining = $total_data - ((($N_page-1) * $itemperpage + $itemperpage) + count($result));
			}	
			$R_message = array("status" => "200", "message" => "Data Exist", "num_data" => count($result), "remaining" => $remaining, "data" => $result);
		}
	
		$obj_connect->down();		
		echo json_encode($R_message);
	}//end get tag

	else{
		$R_message = array("status" => "404", "message" => "Action Not Found");
		echo json_encode($R_message);
	}
}//end gate
else{
	$R_message = array("status" => "404", "message" => "Not Found");
	echo json_encode($R_message);
}
?>