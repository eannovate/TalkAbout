<?php
header('content-type: application/json');
header("access-control-allow-origin: *"); 

if(isset($_GET['action'])){//START GATE
	require_once("../model/Connection.php");
	$obj_con = new Connection();

	require_once("../model/Category.php");
	$obj_cat = new Category();

	if($_GET['action'] == "get_child" && isset($_REQUEST['id'])){
		$obj_con->up();
		$R_message = array("status" => "404", "message" => "No Data");
		
		$N_id = mysql_real_escape_string($_REQUEST['id']);
		$result = $obj_cat->get_child_category($N_id);
		//var_dump($result);
		if(is_array($result)){
			$R_message = array("status" => "200", "message" => "Fetch data success", "data" => $result);
		}

		$obj_con->down();
		echo json_encode($R_message);
	}
	else{
		echo "error";
	}

}//END GATE
?>