<?php 
header('content-type: application/json');
header("access-control-allow-origin: *");

if(isset($_GET['action'])){//start gate

	require_once("../model/Connection.php");
	$obj_connect = new Connection();

	require_once("../model/Work.php");
	$obj_work = new Work();

	//===================================== get work ========================================
	//start get work
	if($_GET['action'] == 'get_work'){//get_work
		$obj_connect->up();
		$R_message = array("status" => "400", "message" => "Data not found");

		$N_page = isset($_REQUEST['page']) ? mysql_real_escape_string($_REQUEST['page']) : 1;
		$result = $obj_work->get_work($N_page);
		if(is_array($result)){
			$itemperpage = 6;
			$total_data = $result[0]['total_data_all'];
			$remaining = $total_data - ((($N_page-1) * $itemperpage) + count($result));
			$R_message = array("status" => "200", "message" => "Data Exist", "num_data" => count($result), "remaining" => $remaining, "data" => $result);
		}	
	
		$obj_connect->down();		
		echo json_encode($R_message);
	}//end get work

	else{
		$R_message = array("status" => "404", "message" => "Action Not Found");
		echo json_encode($R_message);
	}
}//end gate
else{
	$R_message = array("status" => "404", "message" => "Not Found");
	echo json_encode($R_message);
}
?>