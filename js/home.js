$(document).ready(function(){
    var swiperHome = new Swiper('#swiper-home', {
        autoplay :  8000,
        speed: 800,
        slidesPerView: 1,
        spaceBetween: 0,
        loop: true,
        autoplayDisableOnInteraction:false,
    });
    getSize();
    function getSize(){
        var maxHeight = 0;

        $(".col-sproduct").each(function(){
         var thisH = $(this).height();
         if (thisH > maxHeight) { maxHeight = thisH; }
     });
        $(".col-sproduct").height(maxHeight);
        console.log(maxHeight);
    }
    $(window).resize(function(){
        $(".col-sproduct").height("auto");
        getSize();
    });
});