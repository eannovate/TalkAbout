function validateFormContact(){
	var name = $("#contact-name").val();
	var email = $("#contact-email").val();
	var phone = $("#contact-phone").val();
	var subject = $("#contact-subject").val();
	var message = $("#contact-message").val();
	var captcha = $("#contact-captcha").val();
	var numformat = /^[0-9]+$/;
	var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

	if(name != ""){
		$("#form-alert").html("");
		$("#form-alert").hide();
	} else {
		$("#form-alert").html("<span class='glyphicon glyphicon-exclamation-sign'></span> Please insert your name!");
		$("#form-alert").show();
		$("#contact-name").focus();
		return false;
	}
	if(email != ""){
		if(email.match(mailformat)){ 
			$("#form-alert").html("");
			$("#form-alert").hide();
		} else {
			$("#form-alert").html("<span class='glyphicon glyphicon-exclamation-sign'></span> Incorrect email format!");
			$("#form-alert").show();
			$("#contact-email").focus();
			return false;
		}
	} else {
		$("#form-alert").html("<span class='glyphicon glyphicon-exclamation-sign'></span> Please insert your email!");
		$("#form-alert").show();
		$("#contact-email").focus();
		return false;
	}
	if(phone != ""){
		if(phone.match(numformat)){ 
			$("#form-alert").html("");
			$("#form-alert").hide();
		} else {
			$("#form-alert").html("<span class='glyphicon glyphicon-exclamation-sign'></span> Please input number format only!");
			$("#form-alert").show();
			$("#contact-phone").focus();
			return false;
		}
	} else {
		$("#form-alert").html("<span class='glyphicon glyphicon-exclamation-sign'></span> Please insert your phone number!");
		$("#form-alert").show();
		$("#contact-phone").focus();
		return false;
	}
	if(subject != ""){
		$("#form-alert").html("");
		$("#form-alert").hide();
	} else {
		$("#form-alert").html("<span class='glyphicon glyphicon-exclamation-sign'></span> Please insert your subject!");
		$("#form-alert").show();
		$("#contact-subject").focus();
		return false;
	}
	if(message != ""){
		$("#form-alert").html("");
		$("#form-alert").hide();
	} else {
		$("#form-alert").html("<span class='glyphicon glyphicon-exclamation-sign'></span> Please insert your message!");
		$("#form-alert").show();
		$("#contact-message").focus();
		return false;
	}
	if(captcha != ""){
		$("#form-alert").html("");
		$("#form-alert").hide();
	} else {
		$("#form-alert").html("<span class='glyphicon glyphicon-exclamation-sign'></span> Please insert your captcha!");
		$("#form-alert").show();
		$("#contact-captcha").focus();
		return false;
	}
}