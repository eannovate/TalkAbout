function validateResetPass() {
	var password = $("#password").val();
	var repassword = $("#repassword").val();

	if(password != ""){
		$("#alert-password").hide();
		$("#alert-password").text("");
	} else {
		$("#password").focus();
		$("#alert-password").show();
		$("#alert-password").text("masukan password anda!");
		return false;
	}
	if(repassword != ""){
		$("#alert-repassword").hide();
		$("#alert-repassword").text("");
	} else {
		$("#repassword").focus();
		$("#alert-repassword").show();
		$("#alert-repassword").text("ketik ulang password anda!");
		return false;
	}
	if(password == repassword){
		$("#alert-repassword").hide();
		$("#alert-repassword").text("");
	} else {
		$("#repassword").focus(); 
		$("#alert-repassword").show();
		$("#alert-repassword").text("password yang anda ketik tidak sama!");
		return false;
	}
	return true;
}