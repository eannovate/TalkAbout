$(document).ready(function(){
    getSize();
    function getSize(){
        var maxHeight = 0;

        $(".col-team").each(function(){
         var thisH = $(this).height();
         if (thisH > maxHeight) { maxHeight = thisH; }
     });
        $(".col-team").height(maxHeight);
        console.log(maxHeight);
    }
    $(window).resize(function(){
        $(".col-team").height("auto");
        getSize();
    });
});