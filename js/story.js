var swiper = new Swiper('#swiper-story', {
	autoplay :  5000,
	speed: 800,
	parallax: false,
	slidesPerView: 2,
	paginationClickable: true,
	spaceBetween: 0,
	loop: false,
	nextButton: '.swiper-button-next',
	prevButton: '.swiper-button-prev',
});
var swiperXS = new Swiper('#swiper-storyxs', {
	autoplay :  5000,
	speed: 800,
	parallax: false,
	slidesPerView: 1,
	paginationClickable: true,
	spaceBetween: 0,
	loop: true,
});