$(".mobile-menu").click(function(){
    $("#mobile-search").hide();
    $("#mobile-search").removeClass("in");
    $("#mobile-account").hide();
    $("#mobile-account").removeClass("in");
    $(".mobile-menu > i").toggleClass("fa-bars");
    $(".mobile-menu > i").toggleClass("fa-times");
    $("#mobile-dropdown").slideToggle();
    $("#mobile-dropdown").toggleClass("in");
});
$(".mobile-child").click(function(){
    if($(this).find(".dropdown-child").hasClass('in')){
        $(".dropdown-child").slideUp();
        $(this).find(".dropdown-child").removeClass("in");
    } else {
        $(".dropdown-child").removeClass("in");
        $(".dropdown-child").slideUp();
        $(this).find(".dropdown-child").slideDown();
        $(this).find(".dropdown-child").addClass("in");
    }
});
$(".msearch-link").click(function(){
    $("#mobile-account").hide();
    $("#mobile-account").removeClass("in");
    $(".mobile-menu > i").addClass("fa-bars");
    $(".mobile-menu > i").removeClass("fa-times");
    $("#mobile-dropdown").hide();
    $("#mobile-dropdown").removeClass("in");
    $("#mobile-search").slideToggle();
    $("#mobile-search").toggleClass("in");
    $(".minput-search").focus();
});
$(".maccount-active").click(function(){
    $(".mobile-menu > i").addClass("fa-bars");
    $(".mobile-menu > i").removeClass("fa-times");
    $("#mobile-dropdown").hide();
    $("#mobile-dropdown").removeClass("in");
    $("#mobile-search").hide();
    $("#mobile-search").removeClass("in");
    $("#mobile-account").slideToggle();
    $("#mobile-account").toggleClass("in");
});
function validateSearch(){
    var keywords = $(".-search").val();
    if(keywords != ""){
        //nothing
    } else {
        $("#minput-search").focus();
        return false;
    }
}
function validateMobileSearch(){
    var keywords = $(".input-search").val();
    if(keywords != ""){
        //nothing
    } else {
        $("#minput-search").focus();
        return false;
    }
}
function validateSubscribe(){
    var email = $("#input-subscribe").val();
    var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if(email != ""){
        if(email.match(mailformat)){ 
            $("#alert-subscibe").html("");
            $("#alert-subscibe").hide();
        } else {
            $("#alert-subscibe").html("Incorrect email format!");
            $("#alert-subscibe").show();
            $("#input-subscibe").focus();
            return false;
        }
    } else {
        $("#alert-subscibe").html("Please insert your email!");
        $("#alert-subscibe").show();
        $("#input-subscibe").focus();
        return false;
    }
}