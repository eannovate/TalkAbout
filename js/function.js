function charString(variable,length){
  variable = htmlspecialchars_decode(variable, 'ENT_QUOTES');
  var strDecode = strip_tags(variable,'<p>');
  strDecode = strip_tags(variable,'<div>');
  var strLength = strDecode.length;
  if(strLength > length){
  	var result = strDecode.substr(0, length)+"...";
  } else {
  	var result = strDecode;
  }
  return result;
}

function encode(variable){

  var regex = /\\/g;
  var replaceTitle = variable.replace(regex, "").replace(/&quot;/g, "");
  var lowercase = replaceTitle.toLowerCase();
  var result = lowercase.replace(/\s+/g, '-');

  return result;
}

function strip_tags(input, allowed){

	allowed = (((allowed || '') + '')
		.toLowerCase()
		.match(/<[a-z][a-z0-9]*>/g) || [])
    .join(''); // making sure the allowed arg is a string containing only tags in lowercase (<a><b><c>)
    var tags = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi,
    commentsAndPhpTags = /<!--[\s\S]*?-->|<\?(?:php)?[\s\S]*?\?>/gi;
    return input.replace(commentsAndPhpTags, '')
    .replace(tags, function($0, $1) {
    	return allowed.indexOf('<' + $1.toLowerCase() + '>') > -1 ? $0 : '';
    });
}
function htmlspecialchars_decode(string, quote_style) {
	var optTemp = 0,
	i = 0,
	noquotes = false;
	if (typeof quote_style === 'undefined') {
		quote_style = 2;
	}
	string = string.toString()
	.replace(/&lt;/g, '<')
	.replace(/&gt;/g, '>');
	var OPTS = {
		'ENT_NOQUOTES': 0,
		'ENT_HTML_QUOTE_SINGLE': 1,
		'ENT_HTML_QUOTE_DOUBLE': 2,
		'ENT_COMPAT': 2,
		'ENT_QUOTES': 3,
		'ENT_IGNORE': 4
	};
	if (quote_style === 0) {
		noquotes = true;
	}
          if (typeof quote_style !== 'number') { // Allow for a single string or an array of string flags
          	quote_style = [].concat(quote_style);
          	for (i = 0; i < quote_style.length; i++) {
              // Resolve string input to bitwise e.g. 'PATHINFO_EXTENSION' becomes 4
              if (OPTS[quote_style[i]] === 0) {
              	noquotes = true;
              } else if (OPTS[quote_style[i]]) {
              	optTemp = optTemp | OPTS[quote_style[i]];
              }
          }
          quote_style = optTemp;
      }
      if (quote_style & OPTS.ENT_HTML_QUOTE_SINGLE) {
            string = string.replace(/&#0*39;/g, "'"); // PHP doesn't currently escape if more than one 0, but it should
            // string = string.replace(/&apos;|&#x0*27;/g, "'"); // This would also be useful here, but not a part of PHP
        }
        if (!noquotes) {
        	string = string.replace(/&quot;/g, '"');
        }
          // Put this in last place to avoid escape being double-decoded
          string = string.replace(/&amp;/g, '&');

          return string;
      }
function contentDisplay(varcontent,size){
  var resultContent = "";
  var content = "";
  var regex = /(<([^>]+)>)/ig;

  content = varcontent.replace(regex,"");
  variable = htmlspecialchars_decode(content, 'ENT_QUOTES');
  var strDecode = strip_tags(variable,'<p>');
  strDecode = strip_tags(variable,'<div>');
  strDecode = strip_tags(variable,'<img>');
  var strLength = strDecode.length;
  if(strLength > size){
    resultContent = strDecode.substr(0, size)+"...";
  } else {
    resultContent = strDecode;
  }

  return resultContent;
}
      function timeDifference(current, previous) {

      	var msPerMinute = 60 * 1000;
      	var msPerHour = msPerMinute * 60;
      	var msPerDay = msPerHour * 24;
      	var msPerMonth = msPerDay * 30;
      	var msPerYear = msPerDay * 365;

      	var elapsed = current - previous;

      	if (elapsed < msPerMinute) {
      		return Math.round(elapsed/1000) + ' SECONDS AGO';   
      	}

      	else if (elapsed < msPerHour) {
      		return Math.round(elapsed/msPerMinute) + ' MINUTES AGO';   
      	}

      	else if (elapsed < msPerDay ) {
      		return Math.round(elapsed/msPerHour ) + ' HOURS AGO';   
      	}

      	else if (elapsed < msPerMonth) {
      		return Math.round(elapsed/msPerDay) + ' DAYS AGO';   
      	}

      	else if (elapsed < msPerYear) {
      		return Math.round(elapsed/msPerMonth) + ' MONTHS AGO';   
      	}

      	else {
      		return Math.round(elapsed/msPerYear ) + ' YEARS AGO';   
      	}
      }