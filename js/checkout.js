function change_url(val) {
    window.location=val;
}
function convertToRupiah(angka)
{   
    var rupiah = '';        
    // var money = angka.toFixed(0)
    var angkarev = angka.toString().split('').reverse().join('');
    for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+',';
    return 'IDR '+rupiah.split('',rupiah.length-1).reverse().join('');
}
loadProvince();
function loadingBtn(param) {
	if(param != "loading"){
		$("#tes-btn").html("Confirm Order <i class='glyphicon glyphicon-circle-arrow-right'></i>");
	} else {
		$("#tes-btn").html("Loading <i class='fa fa-spinner fa-spin'></i>");
	}
}
function validateCheckout(){
	var address = $("#po-address").val();
	var service = $("#po-service").val();
	var payment = $("#po-payment").val();
	//$("#btn-checkout").addClass("disable-state");

	if(address != ""){
		$("#alert-address").html("");
		$("#alert-address").hide();
		$("#btn-checkout").addClass("disable-state");
		loadingBtn("loading");
	} else {
		$("#alert-address").show();
		$("#alert-address").html("<i class='fa fa-warning'></i> Please choose your shipping address!.");
		$(".radio-address").focus();
		$("#btn-checkout").removeClass("disable-state");
		loadingBtn("not");
		return false;
	}
	if(service != ""){
		$("#alert-service").html("");
		$("#alert-service").hide();
		$("#btn-checkout").addClass("disable-state");
		loadingBtn("loading");
	} else {
		$("#alert-service").show();
		$("#alert-service").html("<i class='fa fa-warning'></i> Please choose JNE service!.");
		$("#service-address").focus();
		$("#btn-checkout").removeClass("disable-state");
		loadingBtn("not");
		return false;
	}
	if(payment != ""){
		$("#alert-payment").html("");
		$("#alert-payment").hide();
		$("#btn-checkout").addClass("disable-state");
		loadingBtn("loading");
	} else {
		$("#alert-payment").show();
		$("#alert-payment").html("<i class='fa fa-warning'></i> Please choose your payment method!.");
		$(".radio-payment").focus();
		$("#btn-checkout").removeClass("disable-state");
		loadingBtn("not");
		return false;
	}
}
$("input[name='address-thumb']").change(function(){
	var service = $("#po-service").val();
	var addressID = $(this).val();
	var cityID = $(this).data('city');
	var cityName = $(this).data('cityname');
	$("#po-address").val(addressID);
	$("#po-cityid").val(cityID);
	$("#po-cityname").val(cityName);
	if(service != ""){
		countOngkir(service,cityName);
	}
});
$("input[name='radio-payment']").change(function(){
	var payment = $(this).val();
	$("#po-payment").val(payment);
});
$("#service-address").on("change", function () {
	var service = $(this).val();
	var cityName = $("#po-cityname").val();
	$("#po-service").val(service);
	countOngkir(service,cityName);
});
$("#input-city").on("change", function () {
	var txt = $(this).find(':checked').text();
	$("#input-cityname").val(txt);
});
$("#input-state").on("change", function () {
	var txt = $(this).find(':checked').text();
	var province_id =  $("#input-state").val();
	if(province_id != ""){
		$("#input-city").html("<option value=''>Loading...</option>");
		loadCity(province_id);
		$("#input-statename").val(txt);
	} else {
		$("#input-city").html("<option value=''>--Choose City--</option>");
	}
});
function checkPromoCity(city){
	var resultCity = 0;
	var lower = city.toLowerCase();
	if(lower.indexOf('jakarta') > -1){
	  	resultCity = 1;
	} else {
		if(lower.indexOf('bogor') > -1){
			resultCity = 1;
		} else {
			if(lower.indexOf('depok') > -1){
				resultCity = 1;
			} else {
				if(lower.indexOf('tangerang') > -1){
					resultCity = 1;
				} else {
					if(lower.indexOf('bekasi') > -1){
						resultCity = 1;
					} else {
						resultCity = 0;
					}
				}
			}
		}
	}
	return resultCity;
}
function checkOngkirPromo(promo_active,subtotal,service,city,ship){
	var promo = 500000;//promo price
	var valid_city = checkPromoCity(city);
	if(promo_active == "true"){
		if(subtotal >= promo){
			if(valid_city != 0){
				if(service == "REG"){
					var totalprice = eval(subtotal);
					var formatprice = convertToRupiah(totalprice);
					$("#po-shipping").val(ship);
					$("#po-promo").val("yes");
					$("#table-ship").text("Promo Free Shipping");
					$("#table-total").text(formatprice);
				} else {
					var shipping_price = convertToRupiah(ship);
					var totalprice = eval(subtotal) + eval(ship);
					var formatprice = convertToRupiah(totalprice);
					$("#po-shipping").val(ship);
					$("#po-promo").val("no");
					$("#table-ship").text(shipping_price);
					$("#table-total").text(formatprice);
				}
			} else {
				var shipping_price = convertToRupiah(ship);
				var totalprice = eval(subtotal) + eval(ship);
				var formatprice = convertToRupiah(totalprice);
				$("#po-shipping").val(ship);
				$("#po-promo").val("no");
				$("#table-ship").text(shipping_price);
				$("#table-total").text(formatprice);
			}
		} else {
			var shipping_price = convertToRupiah(ship);
			var totalprice = eval(subtotal) + eval(ship);
			var formatprice = convertToRupiah(totalprice);
			$("#po-shipping").val(ship);
			$("#po-promo").val("no");
			$("#table-ship").text(shipping_price);
			$("#table-total").text(formatprice);
		}
	} else {
		var shipping_price = convertToRupiah(ship);
		var totalprice = eval(subtotal) + eval(ship);
		var formatprice = convertToRupiah(totalprice);
		$("#po-shipping").val(ship);
		$("#po-promo").val("no");
		$("#table-ship").text(shipping_price);
		$("#table-total").text(formatprice);
	}
}
function validateFormAddress(){
	var name = $("#input-name").val();
	var address = $("#input-address").val();
	var city = $("#input-city").val();
	var zip = $("#input-zip").val();
	var country = $("#input-country").val();

	if(name != ""){
		$("#error-name").html("");
		$("#error-name").hide();
		$("#input-name").removeClass("input-error");
	} else {
		$("#error-name").show();
		$("#error-name").html("<i class='fa fa-warning'></i> This field is required.");
		$("#input-name").addClass("input-error");
		return false;
	}
	if(address != ""){
		$("#error-address").html("");
		$("#error-address").hide();
		$("#input-address").removeClass("input-error");
	} else {
		$("#error-address").show();
		$("#error-address").html("<i class='fa fa-warning'></i> This field is required.");
		$("#input-address").addClass("input-error");
		return false;
	}
	if(city != ""){
		$("#error-city").html("");
		$("#error-city").hide();
		$("#input-city").removeClass("input-error");
	} else {
		$("#error-city").show();
		$("#error-city").html("<i class='fa fa-warning'></i> This field is required.");
		$("#input-city").addClass("input-error");
		return false;
	}
	if(zip != ""){
		$("#error-zip").html("");
		$("#error-zip").hide();
		$("#input-zip").removeClass("input-error");
	} else {
		$("#error-zip").show();
		$("#error-zip").html("<i class='fa fa-warning'></i> This field is required.");
		$("#input-zip").addClass("input-error");
		return false;
	}
	if(country != ""){
		$("#error-country").html("");
		$("#error-country").hide();
		$("#input-country").removeClass("input-error");
	} else {
		$("#error-country").show();
		$("#error-country").html("<i class='fa fa-warning'></i> This field is required.");
		$("#input-country").addClass("input-error");
		return false;
	}
	return true;
}