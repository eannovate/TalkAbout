<?php 
require_once($global['root-url']."model/Connection.php");
$obj_con = new Connection();

require_once($global['root-url']."model/Team.php");
$obj_team = new Team();

if(!isset($_GET['action'])){
    $obj_con->up();
    
    $datas = $obj_team->get_data();
    $total_data = is_array($datas) ? count($datas) : 0;
  
    if(isset($_SESSION['status'])){
        $message = $_SESSION['status'];
        unset($_SESSION['status']);
    } else {
        $message = "";
    }
    if(isset($_SESSION['alert'])){
        $alert = $_SESSION['alert'];
        unset($_SESSION['alert']);
    } else {
        $alert = "";
    }
    
    $obj_con->down();

} else if(isset($_GET['action'])){

    if($_GET['action'] == "add"){
        $obj_con->up();
        $N_name = mysql_real_escape_string(check_input($_POST['name']));
        $N_position = mysql_real_escape_string(check_input($_POST['position']));
        $N_desc = str_replace("'", "’", $_POST['desc']);
        $N_facebook = mysql_real_escape_string(check_input($_POST['facebook']));
        $N_instagram = mysql_real_escape_string(check_input($_POST['instagram']));
        $N_twitter = mysql_real_escape_string(check_input($_POST['twitter']));
        $N_publish = mysql_real_escape_string(check_input($_POST['publish']));

        //insert photo product
        $i = count($_FILES['photo']['name']);
        for ($k = 0; $k < $i; $k++) {
            if($k == 0){
                $N_photoMain = '1';
            }else{
                $N_photoMain = '0';
            }
            $ran = rand();
            $timestamp = time();
            $Photo_ImgLink = "uploads/team/" . $timestamp . $ran . cleanSpace($_FILES["photo"]["name"][$k]);
            $ThmbPhoto_ImgLink = "uploads/team-thmb/" . $timestamp . $ran . cleanSpace($_FILES["photo"]["name"][$k]);
           
            if ((($_FILES["photo"]["type"][$k] == "image/gif") || ($_FILES["photo"]["type"][$k] == "image/jpeg") || ($_FILES["photo"]["type"][$k] == "image/jpg") || ($_FILES["photo"]["type"][$k] == "image/JPG") || ($_FILES["photo"]["type"][$k] == "image/png")) && ($_FILES["photo"]["size"][$k] < 10048576) && ($_FILES["photo"]["name"][0] != "")) {
                if ($_FILES["photo"]["error"][$k] > 0) {
                    //echo "Return Code: " . $_FILES["file"]["error"][$k] . "<br />";
                } else {
                    $file = $_FILES["photo"]["tmp_name"][$k];
                    //save image in server
                    $file_loc = $global['root-url'].$Photo_ImgLink;
                    $file_locThmb = $global['root-url'].$ThmbPhoto_ImgLink;

                    //Resizing images
                    if(move_uploaded_file($file, $file_loc))
                    {
                        $image = new SimpleImage();
                        $image->load($file_loc);
                        $image->resize(200,200);
                        $image->save($file_locThmb);
                    }

                    $result = $obj_team->insert_data($N_name, $N_position, $N_desc, $N_facebook, $N_instagram, $N_twitter, $Photo_ImgLink, $ThmbPhoto_ImgLink, $N_publish);
                }
            } else if ($_FILES['photo']['name'][$k] != "") {
                $message .= "Something is incorrent with photo " . ($k + 1) . " that you upload, please make sure that the format is (jpg, png, gif) and size less than 9.5MB.<br /";
                $_SESSION['alert'] = "error";
            }
        }//For closing

        if (!$result) {
            $message = "Something is wrong with your submission.";
            $_SESSION['alert'] = "error";
        } else if ($result) {
            $message = "Team <i><b>'" . $N_name . "'</b></i> has been succesfully added.<br />";
            $_SESSION['alert'] = "success";
        }

        $_SESSION['status'] = $message;
        header("Location:index.php");
        $obj_con->down();
    
    } else if($_GET['action'] == "delete"){
        $obj_con->up();
        $O_id = mysql_real_escape_string(check_input($_GET['id']));
        $O_title = mysql_real_escape_string(check_input($_GET['title']));
        
        $result = $obj_team->delete_data($O_id, $global['root-url']);
        if($result <= 0){
            $message = "Something is wrong while deleting the Data<br />";
            $_SESSION['alert'] = "error";
        }else if($result == 1){
            $message = "Team <b><i>'" . $O_title . "'</i></b> has been deleted successfully.<br />";
            $_SESSION['alert'] = "success";
        }

        $_SESSION['status'] = $message;
        header("Location:index.php");
        $obj_con->down();
    }
}
?>