<?php 
require_once($global['root-url']."model/Connection.php");
$obj_con = new Connection();
require_once($global['root-url']."model/Homepage.php");
$obj_homepage = new Homepage(); 
if(!isset($_GET['action']) && $_GET['id'] != ""){
    $obj_con->up();
    
    $O_id = mysql_real_escape_string(check_input($_GET['id']));
    $datas = $obj_homepage->get_data_detail($O_id);
    //var_dump($datas);
    
    $obj_con->down();
} else if(isset($_GET['action'])){
    if($_GET['action'] == "edit"){
        $obj_con->up();
        $N_id = mysql_real_escape_string(check_input($_POST['id']));
        $N_title = mysql_real_escape_string(check_input($_POST['title']));
        $N_link = mysql_real_escape_string(check_input($_POST['link']));
        $N_publish = mysql_real_escape_string(check_input($_POST['publish']));
        if($_FILES["image"]["name"]!=null){
            $ran = rand();
            $timestamp = time();
            $Photo_ImgLink = "uploads/homepage/" . $timestamp . $ran . cleanSpace($_FILES["image"]["name"]);
            $ThmbPhoto_ImgLink = "uploads/homepage-thmb/" . $timestamp . $ran . cleanSpace($_FILES["image"]["name"]);
            if ((($_FILES["image"]["type"] == "image/gif")|| ($_FILES["image"]["type"] == "image/jpeg")|| ($_FILES["image"]["type"] == "image/jpg")||($_FILES["image"]["type"] == "image/JPG")||($_FILES["image"]["type"] == "image/png")) && ($_FILES["image"]["size"] < 10048576) && ($_FILES["image"]["name"] != "")){
                require_once("../../packages/imageProcessor.php");
                if ($_FILES["image"]["error"] > 0){
                    //echo "Return Code: " . $_FILES["file"]["error"][$k] . "<br />";
                }else{
                    $file = $_FILES["image"]["tmp_name"];
                    
                    //Resizing images
                    if (true !== ($pic_error = @resampimage(1300,600,"$file","../../../$Photo_ImgLink",0))) {
                        echo $pic_error;
                        unlink($Photo_ImgLink);
                    }
                    
                    if (true !== ($pic_error1 = @resampimagethmb("$file","../../../$ThmbPhoto_ImgLink",0))) {
                        echo $pic_error1;
                        unlink($ThmbPhoto_ImgLink);
                    }
                    $result = $obj_homepage->update_data_image($N_id, $N_title, $Photo_ImgLink, $ThmbPhoto_ImgLink, $N_link, $N_publish);
                    if($result <= 0){
                        $message = "Something is wrong with your submission.<br />";
                        $_SESSION['alert'] = "error";
                    }else if($result == 1){
                        $message = "Homepage <i><b>'" . $N_title . "'</b></i> has been succesfully edited.<br />";
                        $_SESSION['alert'] = "success";
                    }else{
                        $_SESSION['alert'] = "error";
                        die();
                    }
                }
            }
            else if($_FILES["image"]["name"] != ""){
                $message = "Something is wrong with your uploaded homepage image, please make sure that the format is (jpg, png, gif) and the size is less than 9.5MB.<br />";
                $_SESSION['alert'] = "error";
            }
        }else{
            $result = $obj_homepage->update_data($N_id, $N_title, $N_link, $N_publish);
            if($result <= 0){
                $message = "Something is wrong with your submission.<br />";
                $_SESSION['alert'] = "error";
            }else if($result == 1){
                $message = "Homepage Image <i><b>'" . $N_title . "'</b></i> has been succesfully edited.<br />";
                $_SESSION['alert'] = "success";
            }else{
                $_SESSION['alert'] = "error";
                die();
            }
        }
    
        $_SESSION['status'] = $message;
        header("Location:index.php");
        $obj_con->down();
    }
}
?>