<?php 
include("../../packages/require.php");
include("../../controller/controller_tracking.php");
include("../../packages/check_login.php");//USED BY ALL PAGE BUT index.php
$curpage="tracking";
?>
<!DOCTYPE html>
<html lang="en">
<!-- start: HEAD -->
<head>
	<title>Palette | Module Tracking Resi</title>
	<?php include("../../packages/module-head.php");?>
	<!-- Add fancyBox -->
	<link rel="stylesheet" href="<?=$global['absolute-url-admin'];?>packages/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
	<style type="text/css">
		.account-wrap{
			width: 700px;
			max-width: 100%;
			margin: 0 auto;
		}
		.tracking-head {
		    font-size: 16px;
		    font-weight: bold;
		    color: #404141;
		}
		.tracking-stat {
		    text-align: center;
		    font-weight: bold;
		    background-color: #f0b345;
		    color: #FFF;
		    padding: 6px;
		    font-size: 16px;
		    margin: 20px 0;
		    letter-spacing: 0.5px;
		}
		.title-table {
			font-weight: bold;
		}
		.tracking-detail td {
		    font-size: 14px;
		}
		.tracking-detail th {
		    font-weight: bold;
		    font-size: 14px;
		}
		.tracking-thead {
		    font-weight: bold;
		    font-size: 14px;
		    margin: 20px 0 5px 0;
		}
		.resi-error {
		    font-weight: bold;
		    font-size: 16px;
		    margin: 30px 0;
		    text-align: center;
		}
	</style>
</head>
<!-- end: HEAD -->
<!-- start: BODY --> 
<body>
	<!-- start: SLIDING BAR (SB) -->
	<?php include("../../parts/part-sliding_bar.php");?>
	<!-- end: SLIDING BAR -->
	<div class="main-wrapper">
		<!-- start: TOPBAR -->
		<?php include("../../parts/part-top_bar.php");?>
		<!-- end: TOPBAR -->
		<!-- start: PAGESLIDE LEFT -->
		<?php include("../../parts/part-pageslide_left.php");?>
		<!-- end: PAGESLIDE LEFT -->
		<!-- start: PAGESLIDE RIGHT --> 
		<?php include("../../parts/part-pageslide_right.php");?>
		<!-- end: PAGESLIDE RIGHT -->
		<!-- start: MAIN CONTAINER -->
		<div class="main-container inner">
			<!-- start: PAGE -->
			<div class="main-content">
				<div class="container">
					<!-- start: PAGE HEADER -->
					<div class="toolbar row">
						<div class="col-sm-6">
							<div class="page-header">
								<h1>
									Tracking Resi 
									<small>Tracking your resi here.</small>
								</h1>
							</div>
						</div>
						<div class="col-sm-6 col-xs-12"></div>
					</div>
					<!-- end: PAGE HEADER -->
					<!-- start: BREADCRUMB -->
					<div class="row">
						<div class="col-md-12">
							<ol class="breadcrumb">
								<li>
									<a href="<?=$path['order'];?>">
										Order History Management
									</a>
								</li>
								<li class="active">
									Tracking Detail
								</li>
							</ol>
						</div>
					</div>
					<!-- end: BREADCRUMB -->
					<!-- start: PAGE CONTENT -->
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-white">
								<div class="panel-heading border-light">
									<div class="row">
										<div class="col-sm-3 col-xs-12 hidden-xs">
											<a href="edit.php?id=<?=$O_id;?>" type="reset" class="btn btn-default">
												<i class="fa fa-arrow-circle-left"></i> Back
											</a>
										</div>
										<div class="col-sm-6 col-xs-12 text-center">
											<h4 class="panel-title bold">Tracking <?=$O_resi;?></h3>
										</div>
									</div>
								</div>
								<div class="panel-body">
									<div class="account-wrap">
										<?php if($data_resi != "error"){ ?>
										<?php if($data_stat['code'] != "400"){ ?>
										<div class="tracking-head">PAKET <?php echo $O_resi;?> - JNE</div>
										<div class="tracking-stat"><?php echo $data_summary['status'];?></div>
										<div class="tracking-detail table-responsive">
											<table class="table table-bordered">
												<tr class="active">
													<td class="title-table">Nomor resi</td>
													<td><?php echo $O_resi;?></td>
												</tr>
												<tr >
													<td class="title-table">Jenis Layanan</td>
													<td><?php echo $data_summary['service_code'];?></td>
												</tr>
												<tr class="active">
													<td class="title-table">Tanggal pengiriman</td>
													<td><?php echo date('d M Y',strtotime($data_detail['waybill_date']))." ".$data_detail['waybill_time'];?></td>
												</tr> 
												<tr>
													<td class="title-table">Berat kiriman</td>
													<td><?php echo $data_detail['weight'];?> kg</td>
												</tr> 
												<tr class="active">
													<td class="title-table">Nama pengirim</td>
													<td><?php echo $data_detail['shippper_name'];?></td>
												</tr> 
												<tr>
													<td class="title-table">Kota asal pengirim</td>
													<td><?php echo $data_detail['shipper_city'];?></td>
												</tr> 
												<tr class="active">
													<td class="title-table">Nama penerima</td>
													<td><?php echo $data_detail['receiver_name'];?></td>
												</tr> 	
												<tr>
													<td class="title-table">Alamat penerima</td>
													<td><?php echo $data_detail['receiver_address1'].", ".$data_detail['receiver_address2'].". ".$data_detail['receiver_address3'].", ".$data_detail['receiver_city'];?></td>
												</tr>
											</table>
										</div>
										<div class="tracking-thead">Status Pengiriman</div>
										<div class="tracking-detail table-responsive">
											<table class="table table-bordered">
												<tr class="active">
													<td class="title-table">Status</td>
													<td><?php echo $data_delivery['status'];?></td>
												</tr>
												<?php if($data_delivery['status'] == "DELIVERED") { ?>
												<tr>
													<td class="title-table">Nama penerima</td>
													<td><?php echo $data_delivery['pod_receiver'];?></td>
												</tr> 
												<tr class="active">
													<td class="title-table">Tanggal penerima</td>
													<td><?php echo date('d M Y',strtotime($data_delivery['pod_date']))." ".$data_delivery['pod_time'];?></td>
												</tr> 
												<?php } ?>
											</table>
										</div>
										<div class="tracking-thead">Riwayat Pengiriman</div>
										<div class="tracking-detail table-responsive">
											<table class="table table-bordered">
												<tr>
													<th>Tanggal</th>
													<?php if($data_manifest[0]['city_name'] != ""){ ?>
													<th>Kota</th>
													<?php } ?>
													<th>Keterangan</th>
												</tr>
												<?php foreach ($data_manifest as $data) { ?>
												<tr>
													<td><?php echo date('d M Y',strtotime($data['manifest_date']))." ".$data['manifest_time'];?></td>
													<?php if($data_manifest[0]['city_name'] != ""){ ?>
													<td><?php echo $data['city_name'];?></td>
													<?php } ?>
													<td><?php echo $data['manifest_description'];?></td>
												</tr>
												<?php } ?>
											</table>
										</div>
										<?php } else { ?>
										<div class="resi-error">
											Sorry your resi is not found or not registered yet!
										</div>
										<?php } ?>
										<?php } else { ?>
										<div class="resi-error">
											Sorry our tracking network is temporary error!
										</div>
										<?php } ?>
									</div>
								</div>
								<div class="panel-footer up3">
									
								</div>
							</div>
						</div>
					</div>
					<!-- end: PAGE CONTENT-->
				</div>
				<div class="subviews">
					<div class="subviews-container"></div>
				</div>
			</div>
			<!-- end: PAGE -->
		</div>
		<!-- end: MAIN CONTAINER -->
		<!-- start: FOOTER -->
		<?php include("../../parts/part-footer.php");?>
		<!-- end: FOOTER -->
		<!-- start: SUBVIEW SAMPLE CONTENTS -->
		<?php include("../../parts/part-sample_content.php");?>
		<!-- end: SUBVIEW SAMPLE CONTENTS -->
	</div>
	<?php include("../../packages/footer-js.php");?>
	<!-- Add fancyBox -->
	<script type="text/javascript" src="<?=$global['absolute-url-admin'];?>packages/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
	</body>
	<!-- end: BODY -->
	</html>