<?php 
require_once($global['root-url']."model/Connection.php");
$obj_con = new Connection();

require_once($global['root-url']."model/Product.php");
$obj_product = new Product(); 

require_once($global['root-url']."model/Category.php");
$obj_category = new Category(); 

require_once($global['root-url']."model/Photo.php");
$obj_photo = new Photo(); 

if(!isset($_GET['action']) && $_GET['id'] != ""){
    $obj_con->up();
    $O_id = mysql_real_escape_string(check_input($_GET['id']));
    $O_page = ((isset($_GET['page']))) ? mysql_real_escape_string(check_input($_GET['page'])) : 1;
    $O_tag = ((isset($_GET['tag']))) ? mysql_real_escape_string(check_input($_GET['tag'])) : "all";
    $O_cat = ((isset($_GET['cat']))) ? mysql_real_escape_string(check_input($_GET['cat'])) : null;

    //get list brands
    $categorys = $obj_category->get_index();
    $datas = $obj_product->get_data_detail($O_id);
    //var_dump($data_photos);
    
    if(isset($_SESSION['status'])){
        $message = $_SESSION['status'];
        unset($_SESSION['status']);
    } else {
        $message = "";
    }

    if(isset($_SESSION['alert'])){
        $alert = $_SESSION['alert'];
        unset($_SESSION['alert']);
    } else {
        $alert = "";
    }
    
    $obj_con->down();

} else if(isset($_GET['action'])){

    if($_GET['action'] == "edit"){
        $obj_con->up();
        
        $N_id = mysql_real_escape_string(check_input($_POST['id']));
        $N_categoryID = mysql_real_escape_string(check_input($_POST['category']));
        $N_name = mysql_real_escape_string(check_input($_POST['name']));
        $N_desc = $_POST['desc'];
        $N_price = mysql_real_escape_string(check_input($_POST['price']));
        $N_specialPrice = mysql_real_escape_string(check_input($_POST['specialPrice']));
        $N_weight = mysql_real_escape_string(check_input($_POST['weight']));
        $N_shortDesc = $_POST['shortDesc'];
        $N_tag = mysql_real_escape_string(check_input($_POST['tag']));
        $N_trending = (isset($_POST['tagTrending'])) ? 1 : 0;
        $N_best_seller = (isset($_POST['tagBestSeller'])) ? 1 : 0;
        $N_limitedStock = (isset($_POST['tagOutOfStock'])) ? 1 : 0;
        $N_productMain = mysql_real_escape_string(check_input($_POST['radioPrimary']));
        $N_publish = mysql_real_escape_string(check_input($_POST['publish']));

        //start update product
        $result = $obj_product->update_data($N_id, $N_categoryID, $N_name, $N_desc, $N_price, $N_specialPrice, $N_weight, $N_shortDesc, $N_tag, $N_trending, $N_best_seller, $N_limitedStock, $N_publish);
        if($result == 1){
            $message = "Changes on <i><b>'" . $N_name . "'</b></i> has been saved successfully.<br><br>";
            $_SESSION['alert'] = "success";
        }else{
            $message = "There is no change in product<br><br>";
            $_SESSION['alert'] = "success";
        }
    
        //start set photo primary
        if ($N_productMain) {
            $unset_main = $obj_photo->unset_main($N_id);
            if($unset_main >= 0)
            {
                $set_main = $obj_photo->set_main($N_id, $N_productMain);
                if($set_main == 1){
                    $message .= "One of the photo has been set as primary.";
                }
            }
        }
        //end set photo primary

        //start update photo title in T_PHOTO
        $photoID = count($_POST['photoID']);
        for($pi=0; $pi < $photoID; $pi++){
            $update_photo_title = $obj_photo->update_title($_POST['photoID'][$pi], $_POST['photoTitle'][$pi]);
        }
        //end update photo title in T_PHOTO
        
        $_SESSION['status'] = $message;
        header("Location:".$_POST['url']);
        $obj_con->down();

    }else if($_GET['action'] == "addPhoto"){
        $obj_con->up();
        $N_id = mysql_real_escape_string(check_input($_POST['id']));
            
        $i = count($_FILES["photo"]["name"]);
        //To insert the photo product
        for ($k=0;$k<$i;$k++)
        {
            $ran = rand ();
            $timestamp = time();
            $Photo_ImgLink = "uploads/product/". $timestamp . $ran . cleanSpace($_FILES["photo"]["name"][$k]);
            $ThmbPhoto_ImgLink = "uploads/product-thmb/". $timestamp . $ran . cleanSpace($_FILES["photo"]["name"][$k]);
            
            if ((($_FILES["photo"]["type"][$k] == "image/gif")|| ($_FILES["photo"]["type"][$k] == "image/jpeg")|| ($_FILES["photo"]["type"][$k] == "image/jpg")||($_FILES["photo"]["type"][$k] == "image/JPG")||($_FILES["photo"]["type"][$k] == "image/png")) && ($_FILES["photo"]["size"][$k] < 10048576) && ($_FILES["photo"]["name"][0] != ""))
            {
                if ($_FILES["photo"]["error"][$k] > 0)
                {
                    //echo "Return Code: " . $_FILES["photo"]["error"][$k] . "<br />";
                }
                else
                {
                    //require_once("../../packages/imageProcessor.php");
                    $file = $_FILES["photo"]["tmp_name"][$k];
                    //save image in server
                    $file_loc = $global['root-url'].$Photo_ImgLink; 
                    $file_locThmb = $global['root-url'].$ThmbPhoto_ImgLink;
                    
                    //Resizing images
                    if(move_uploaded_file($file, $file_loc))
                    {
                        $image = new SimpleImage();
                        $image->load($file_loc);
                        $image->resize(200,200);
                        $image->save($file_locThmb);
                    }

                    $result = $obj_photo->insert_sub_data($Photo_ImgLink, $ThmbPhoto_ImgLink, $N_id, $_POST['photoTitle'][$k]);
                    if($result >= 1){
                        $message = $i . " photo is uploaded successfully<br/>";
                        $_SESSION['alert'] = "success";
                    }       
                }
            }
            else if ($_FILES["photo"]["name"][$k] != "")
            {
                $message = "Something is wrong with photo-" . ($k+1) . ", please check the format (jpg, png, gif) and make sure that the file size is less than 9.5MB.<br />";
                $_SESSION['alert'] = "error";
            }
        }//For closing
        
        $_SESSION['status'] = $message;
        header("Location:".$_POST['url']);
        $obj_con->down();

    }else if($_GET['action'] == "deletePhoto"){
        $obj_con->up();
        $N_product_id = mysql_real_escape_string(check_input($_REQUEST['id']));
        $N_title = mysql_real_escape_string(check_input(urldecode($_REQUEST['title'])));
        $N_id = mysql_real_escape_string(check_input($_REQUEST['photo_id']));
        
        $result = $obj_photo->delete_data($N_product_id, $N_id, $global['root-url']);
        if($result <= 0){
            $message = "Something is wrong with the photo deletion";
            $_SESSION['alert'] = "error";
        }else{
            $message = "One of the photos for <i><b>'" . $N_title . "'</b></i> has been deleted successfully.";
            $_SESSION['alert'] = "success";
        }

        $_SESSION['status'] = $message;
        header("Location:edit.php?id=".$N_product_id."&page=".$_GET['page']."&tag=".$_GET['tag']."&cat=".$_GET['cat']);
        $obj_con->down();
    }
}
?>