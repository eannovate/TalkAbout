<?php 
require_once($global['root-url']."model/Connection.php");
$obj_con = new Connection();

require_once($global['root-url']."model/Product.php");
$obj_product = new Product();

require_once($global['root-url']."model/Category.php");
$obj_category = new Category();

require_once($global['root-url']."model/Photo.php");
$obj_photo = new Photo(); 

if(!isset($_GET['action'])){
    $obj_con->up();
    error_reporting(E_ALL^E_NOTICE); //remove notice

    $O_page = ((isset($_GET['page']))) ? mysql_real_escape_string(check_input($_GET['page'])) : 1;
    $O_tag = ((isset($_GET['tag']))) ? mysql_real_escape_string(check_input($_GET['tag'])) : "all";
    $O_cat = ((isset($_GET['cat']))) ? mysql_real_escape_string(check_input($_GET['cat'])) : null;

    //get list brand
    $categorys = $obj_category->get_index();
    $datas = $obj_product->get_data_by_page($O_page, $O_tag, $O_cat);
    //var_dump($datas);
    $total_data = is_array($datas) ? $datas[0]['total_data_all'] : 0;
    $total_page = is_array($datas) ? $datas[0]['total_page'] : 0;

    if(isset($_SESSION['status'])){
        $message = $_SESSION['status'];
        unset($_SESSION['status']);
    } else {
        $message = "";
    }

    if(isset($_SESSION['alert'])){
        $alert = $_SESSION['alert'];
        unset($_SESSION['alert']);
    } else {
        $alert = "";
    }
    
    $obj_con->down();

} else if(isset($_GET['action'])){

    if($_GET['action'] == "add"){
        $obj_con->up();
       
        $N_categoryID = mysql_real_escape_string(check_input($_POST['category']));
        $N_name = mysql_real_escape_string(check_input($_POST['name']));
        $N_desc = $_POST['desc'];
        $N_price = mysql_real_escape_string(check_input($_POST['price']));
        $N_specialPrice = mysql_real_escape_string(check_input($_POST['specialPrice']));
        $N_weight = mysql_real_escape_string(check_input($_POST['weight']));
        $N_shortDesc = $_POST['shortDesc'];
        $N_tag = mysql_real_escape_string(check_input($_POST['tag']));
        $N_publish = mysql_real_escape_string(check_input($_POST['publish']));
        $N_trending = (isset($_POST['tagTrending'])) ? 1 : 0;
        $N_best_seller = (isset($_POST['tagBestSeller'])) ? 1 : 0;
        $N_limitedStock = (isset($_POST['tagOutOfStock'])) ? 1 : 0;

        $result = $obj_product->insert_data($N_categoryID, $N_name, $N_desc, $N_price, $N_specialPrice, $N_weight, $N_shortDesc, $N_tag, $N_trending, $N_best_seller, $N_limitedStock, $N_publish);
        if (!$result) {
            $message = "Something is wrong with your submission.";
            $_SESSION['alert'] = "error";
        } else if ($result) {
            $message = "Product <i><b>'" . $N_name . "'</b></i> has been succesfully added.<br />";
            $_SESSION['alert'] = "success";

            //insert photo product
            $i = count($_FILES['photo']['name']);
            for ($k = 0; $k < $i; $k++) {
                if($k == 0){
                    $N_photoMain = '1';
                }else{
                    $N_photoMain = '0';
                }
                $ran = rand();
                $timestamp = time();
                $Photo_ImgLink = "uploads/product/" . $timestamp . $ran . cleanSpace($_FILES["photo"]["name"][$k]);
                $ThmbPhoto_ImgLink = "uploads/product-thmb/" . $timestamp . $ran . cleanSpace($_FILES["photo"]["name"][$k]);
               
                if ((($_FILES["photo"]["type"][$k] == "image/gif") || ($_FILES["photo"]["type"][$k] == "image/jpeg") || ($_FILES["photo"]["type"][$k] == "image/jpg") || ($_FILES["photo"]["type"][$k] == "image/JPG") || ($_FILES["photo"]["type"][$k] == "image/png")) && ($_FILES["photo"]["size"][$k] < 10048576) && ($_FILES["photo"]["name"][0] != "")) {
                    //require_once("../../packages/imageProcessor.php");
                    if ($_FILES["photo"]["error"][$k] > 0) {
                        //echo "Return Code: " . $_FILES["file"]["error"][$k] . "<br />";
                    } else {
                        $file = $_FILES["photo"]["tmp_name"][$k];
                        //save image in server
                        $file_loc = $global['root-url'].$Photo_ImgLink; 
                        $file_locThmb = $global['root-url'].$ThmbPhoto_ImgLink;

                        //Resizing images
                        if(move_uploaded_file($file, $file_loc))
                        {
                            $image = new SimpleImage();
                            $image->load($file_loc);
                            $image->resize(200,200);
                            $image->save($file_locThmb);
                        }

                        $save_photo = $obj_photo->insert_data($Photo_ImgLink, $ThmbPhoto_ImgLink, $result, $N_photoMain, $_POST['photoTitle'][$k]);
                    }
                } else if ($_FILES['photo']['name'][$k] != "") {
                    $message = "Something is incorrent with photo " . ($k + 1) . " that you upload, please make sure that the format is (jpg, png, gif) and size less than 9.5MB.<br /";
                    $_SESSION['alert'] = "error";
                }
            }//For closing
        }
        
        $_SESSION['status'] = $message;
        header("Location:index.php");
        $obj_con->down();

    } else if($_GET['action'] == "update"){
        $obj_con->up();
        $num = 1;
        $array = count($_POST['id']);
        for($i=0; $i < $array; $i++){
            $N_trending       = (isset($_POST['tagTrending-'.$num])) ? 1 : 0;
            $N_best_seller    = (isset($_POST['tagBestSeller-'.$num])) ? 1 : 0;
            $N_limitedStock   = (isset($_POST['tagOutOfStock-'.$num])) ? 1 : 0;
            
            $result = $obj_product->update_index($_POST['id'][$i], $N_trending, $N_best_seller, $N_limitedStock, $_POST['price'][$i], $_POST['specialPrice'][$i]);
            //var_dump($result);
            if($result == 1){
                $message = "Product <i><b>'" . $_POST['name'][$i] . "'</b></i> has been succesfully edited.<br><br>";
                $_SESSION['alert'] = "success";
            }
            $num++;
        }

        $_SESSION['status'] = $message;
        header("Location:".$_POST['url']);
        $obj_con->down();

    } else if($_GET['action'] == "delete"){
        $obj_con->up();
        $O_id = mysql_real_escape_string(check_input($_GET['id']));
        $O_title = mysql_real_escape_string(check_input($_GET['title']));    
            
        $result = $obj_product->delete_data($O_id, $global['root-url']);
        if($result <= 0){
            $message = "Something is wrong while deleting the Data<br />";
            $_SESSION['alert'] = "error";
        }else if($result == 1){
            $message = "Product <b><i>'" . $O_title . "'</i></b> has been deleted successfully.<br />";
            $_SESSION['alert'] = "success";
        }

        $_SESSION['status'] = $message;
        header("Location:index.php?page=".$_GET['page']."&tag=".$_GET['tag']."&cat=".$_GET['cat']);
        $obj_con->down();
    }
}
?>