<?php 
	include("../../packages/require.php");
	include("../../packages/check_login.php");//USED BY ALL PAGE BUT index.php
	include("controller/controller_user.php");
	$curpage="user";
?>
<!DOCTYPE html>
<html lang="en">
	<!-- start: HEAD -->
	<head>
		<title><?=$title['user'];?></title>
		<?php include("../../packages/module-head.php");?>
	</head>
	<!-- end: HEAD -->

	<!-- start: BODY -->
	<body>
		<!-- start: SLIDING BAR (SB) -->
		<?php include("../../parts/part-sliding_bar.php");?>
		<!-- end: SLIDING BAR -->

		<div class="main-wrapper">
			<!-- start: TOPBAR -->
			<?php include("../../parts/part-top_bar.php");?>
			<!-- end: TOPBAR -->

			<!-- start: PAGESLIDE LEFT -->
			<?php include("../../parts/part-pageslide_left.php");?>
			<!-- end: PAGESLIDE LEFT -->

			<!-- start: PAGESLIDE RIGHT -->
			<?php include("../../parts/part-pageslide_right.php");?>
			<!-- end: PAGESLIDE RIGHT -->

			<!-- start: MAIN CONTAINER -->
			<div class="main-container inner">
				<!-- start: PAGE -->
				<div class="main-content">
					<div class="container">
						<!-- start: PAGE HEADER -->
						<div class="toolbar row">
							<div class="col-sm-6">
								<div class="page-header">
									<h1>
										User Management 
										<small>Adding and Editing your user admin here.</small>
									</h1>
								</div>
							</div>
							<div class="col-sm-6 col-xs-12"></div>
						</div>
						<!-- end: PAGE HEADER -->

						<!-- start: BREADCRUMB -->
						<div class="row">
							<div class="col-md-12">
								<ol class="breadcrumb">
									<li class="active">
										User Management
									</li>
								</ol>
							</div>
						</div>
						<!-- end: BREADCRUMB -->

						<!-- start: PAGE CONTENT -->
						<div class="row">
							<div class="col-md-12">
								<div class="panel panel-white">
									<div class="panel-heading border-light text-right">
										<a class="btn btn-light-azure" href="#panel-add" data-toggle="modal" data-target="#panel-add">
											<i class="fa fa-plus"></i> Add New Data
										</a>
									</div>
									<div class="panel-body">
										<div style="text-align:right;margin: 10px 0;">
											Total Data : <span class="label label-info"><?=$total_data;?></span>
										</div>
										<div class="table-responsive">
											<table class="table table-striped table-bordered">
											  	<tr>
											  		<th class="text-center">#</th>
											  		<th>Email</th>
											  		<th>Username</th>
											  		<th>Action</th>
											  	</tr>
											  	<?php if(is_array($datas)) { $num=1;foreach($datas as $data) { ?>
											  	<tr>
											  		<td class="text-center"><?=$num;?>.</td>
											  		<td><?=$data['user_email'];?></td>
											  		<td><?=$data['user_name'];?></td>
											  		<td>
											  			<div class="text-center">
															<a href="<?=$path['user-edit'].$data['user_ID'];?>" class="btn btn-xs btn-blue tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
															<a href="javascript:void(0)" onclick="confirmDelete('<?=$data['user_ID'];?>', '<?=$data['user_name'];?>');" class="btn btn-xs btn-red tooltips" data-placement="top" data-original-title="Remove"><i class="fa fa-trash-o fa fa-white"></i></a>
														</div>
											  		</td>
											  	</tr>
											  	<?php $num++;} } else { ?>
											  	<tr class="warning">
											  		<td colspan="5" class="bold">There is no data right now!</td>
											  	</tr>
											  	<?php } ?>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- end: PAGE CONTENT-->
					</div>
					<div class="subviews">
						<div class="subviews-container"></div>
					</div>

					<!-- start: PANEL ADD MODAL FORM -->
					<div class="modal fade" id="panel-add" tabindex="-1" role="dialog" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
										&times;
									</button>
									<h4 class="modal-title">Add New User</h4>
								</div>
								<form name="addUser" action="index.php?action=add" enctype="multipart/form-data" method="post" onsubmit="return validateForm();" >
									<div class="modal-body">
										<div class="row">
		                                    <div class="col-sm-4 col-xs-12 up1 form-label"><strong>Email <span class="symbol required"></span></strong></div>
		                                    <div class="col-sm-8 col-xs-12 up1">
		                                    	<input id="input-email" name="email" type="text" class="form-control" placeholder="user email" />
		                                    	<div id="error-email" class="is-error"></div>
		                                    </div>
		                                </div>
		                                <div class="row">
		                                    <div class="col-sm-4 col-xs-12 up1 form-label"><strong>Username <span class="symbol required"></span></strong></div>
		                                    <div class="col-sm-8 col-xs-12 up1">
		                                    	<input id="input-username" name="username" type="text" class="form-control" placeholder="username" />
		                                    	<div id="error-username" class="is-error"></div>
		                                    </div>
		                                </div>
		                                <div class="row">
		                                    <div class="col-sm-4 col-xs-12 up1 form-label"><strong>Password <span class="symbol required"></span></strong></div>
		                                    <div class="col-sm-8 col-xs-12 up1">
		                                    	<input id="input-password" name="password" type="password" class="form-control" placeholder="user password" />
		                                    	<div id="error-password" class="is-error"></div>
		                                    </div>
		                                </div>
		                                <div class="row">
		                                    <div class="col-sm-4 col-xs-12 up1 form-label"><strong>Re-Enter Password <span class="symbol required"></span></strong></div>
		                                    <div class="col-sm-8 col-xs-12 up1">
		                                    	<input id="input-repassword" type="password" class="form-control" placeholder="re enter user password" />
		                                    	<div id="error-repassword" class="is-error"></div>
		                                    </div>
		                                </div>
									</div>
									<div class="modal-footer f5-bg">
										<div class="btn-group">
											<button type="reset" class="btn btn-default" data-dismiss="modal">
												<i class="fa fa-times"></i> Cancel
											</button>
											<button type="submit" class="btn btn-success">
												<i class="fa fa-check fa fa-white"></i> Create
											</button>
										</div>
									</div>
								</form>
							</div>
							<!-- /.modal-content -->
						</div>
						<!-- /.modal-dialog -->
					</div>
					<!-- /.modal -->
					<!-- end: SPANEL CONFIGURATION MODAL FORM -->
				</div>
				<!-- end: PAGE -->
			</div>
			<!-- end: MAIN CONTAINER -->

			<!-- start: FOOTER -->
			<?php include("../../parts/part-footer.php");?>
			<!-- end: FOOTER -->

			<!-- start: SUBVIEW SAMPLE CONTENTS -->
			<?php include("../../parts/part-sample_content.php");?>
			<!-- end: SUBVIEW SAMPLE CONTENTS -->
		</div>
		<?php include("../../packages/footer-js.php");?>
		<script type="text/javascript">
			function validateForm(){
				var email = $("#input-email").val();
				var username = $("#input-username").val();
				var password = $("#input-password").val();
				var repassword = $("#input-repassword").val();
				var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

				if(email != ""){
					if(email.match(mailformat)){
						$("#error-email").html("");
						$("#error-email").hide();
						$("#input-email").removeClass("input-error");
					} else {
						$("#error-email").show();
						$("#error-email").html("<i class='fa fa-warning'></i> Your email address must be in the format of name@domain.com");
						$("#input-email").addClass("input-error");
						return false;
					}
				} else {
					$("#error-email").show();
					$("#error-email").html("<i class='fa fa-warning'></i> This field is required.");
					$("#input-email").addClass("input-error");
					return false;
				}
				if(username != ""){
					$("#error-username").html("");
					$("#error-username").hide();
					$("#input-username").removeClass("input-error");
				} else {
					$("#error-username").show();
					$("#error-username").html("<i class='fa fa-warning'></i> This field is required.");
					$("#input-username").addClass("input-error");
					return false;
				}
				if(password != ""){
					$("#error-password").html("");
					$("#error-password").hide();
					$("#input-password").removeClass("input-error");
				} else {
					$("#error-password").show();
					$("#error-password").html("<i class='fa fa-warning'></i> This field is required.");
					$("#input-password").addClass("input-error");
					return false;
				}
				if(repassword != ""){
					$("#error-repassword").html("");
					$("#error-repassword").hide();
					$("#input-repassword").removeClass("input-error");
				} else {
					$("#error-repassword").show();
					$("#error-repassword").html("<i class='fa fa-warning'></i> Please re-enter your password.");
					$("#input-repassword").addClass("input-error");
					return false;
				}
				if(password == repassword){
					$("#error-repassword").html("");
					$("#error-repassword").hide();
					$("#input-repassword").removeClass("input-error");
				} else {
					$("#error-repassword").show();
					$("#error-repassword").html("<i class='fa fa-warning'></i> Your password do not match.");
					$("#input-repassword").addClass("input-error");
					return false;
				}
			}

			<?php if($message != "") { ?>
			//use session here for alert success/failed
			var alertText = "<?=$message;?>"; //teks for alert
				<?php if($alert != "success"){ ?>
					//error alert
					errorAlert(alertText);
				<?php } else { ?>
					//success alert
					successAlert(alertText); 
				<?php } ?>
			<?php } ?>

			//function confirmation delete
			function confirmDelete(num, text){
				swal({
	                title: "Are you sure?",
	                text: "You will not be able to recover this file!",
	                type: "warning",
	                showCancelButton: true,
	                confirmButtonColor: "#DD6B55",
	                confirmButtonText: "Delete ! ",
	                cancelButtonText: "Cancel !",
	                closeOnConfirm: false,
	                closeOnCancel: true
	            },
	            function (isConfirm) {
	                if (isConfirm) {
	                    window.location.href = "index.php?action=delete&id="+num+"&title="+text;
	                } else {
	                    //nothing
	                }
	            });
			}
		</script>
	</body>
	<!-- end: BODY -->
</html>