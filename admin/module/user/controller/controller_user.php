<?php 
require_once($global['root-url']."model/Connection.php");
$obj_con = new Connection();

require_once($global['root-url']."model/User.php");
$obj_user = new User(); 

if(!isset($_GET['action'])){
    $obj_con->up();

    $datas = $obj_user->get_data();
    $total_data = is_array($datas) ? count($datas) : 0;

    if(isset($_SESSION['status'])){
        $message = $_SESSION['status'];
        unset($_SESSION['status']);
    } else {
        $message = "";
    }

    if(isset($_SESSION['alert'])){
        $alert = $_SESSION['alert'];
        unset($_SESSION['alert']);
    } else {
        $alert = "";
    }

    $obj_con->down();

} else if(isset($_GET['action'])){

    if($_GET['action'] == "add"){
        $obj_con->up();

        $N_email = mysql_real_escape_string(check_input($_POST['email']));
        $N_username = mysql_real_escape_string(check_input($_POST['username']));
        $N_password = mysql_real_escape_string(check_input($_POST['password']));

        $result = $obj_user->insert_data($N_email, $N_username, $N_password);
        if($result <= 0){
            $message = "User <i><b>'" . $N_username . "'</b></i> registration failed.<br />";
            $_SESSION['alert'] = "error";
        }else if($result == 1){
            $message = "User <i><b>'" . $N_username . "'</b></i> succesfully registered.<br />";
            $_SESSION['alert'] = "success";
        }else{
            $_SESSION['alert'] = "error";
            die();
        }

        $_SESSION['status'] = $message;
        header("Location:index.php");
        $obj_con->down();

    } else if($_GET['action'] == "delete"){
        $obj_con->up();

        $O_id = mysql_real_escape_string(check_input($_GET['id']));
        $O_title = mysql_real_escape_string(check_input($_GET['title']));

        $result = $obj_user->delete_data($O_id);
        if($result <= 0){
            $message = "Something is wrong while deleting the Data<br />";
            $_SESSION['alert'] = "error";
        }else if($result == 1){
            $message = "User <b><i>'" . $O_title . "'</i></b> has been deleted successfully.<br />";
            $_SESSION['alert'] = "success";
        }

        $_SESSION['status'] = $message;
        header("Location:index.php");
        $obj_con->down();
    }
}
?>