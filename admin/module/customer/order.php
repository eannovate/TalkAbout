<?php 
include("../../packages/require.php");
include("../../packages/check_login.php");//USED BY ALL PAGE BUT index.php
include("../../controller/controller_customer_order.php");
$curpage="customer";
?>
<!DOCTYPE html>
<html lang="en">
<!-- start: HEAD -->
<head>
	<title><?=$title['customer'];?></title>
	<?php include("../../packages/module-head.php");?>
	<!-- Add fancyBox -->
	<link rel="stylesheet" href="<?=$global['absolute-url-admin'];?>packages/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
</head>
<!-- end: HEAD -->
<!-- start: BODY -->
<body>
	<!-- start: SLIDING BAR (SB) -->
	<?php include("../../parts/part-sliding_bar.php");?>
	<!-- end: SLIDING BAR -->

	<div class="main-wrapper">

		<!-- start: TOPBAR -->
		<?php include("../../parts/part-top_bar.php");?>
		<!-- end: TOPBAR -->

		<!-- start: PAGESLIDE LEFT -->
		<?php include("../../parts/part-pageslide_left.php");?>
		<!-- end: PAGESLIDE LEFT -->

		<!-- start: PAGESLIDE RIGHT -->
		<?php include("../../parts/part-pageslide_right.php");?>
		<!-- end: PAGESLIDE RIGHT -->

		<!-- start: MAIN CONTAINER -->
		<div class="main-container inner">
			<!-- start: PAGE -->
			<div class="main-content">

				<div class="container">

					<!-- start: PAGE HEADER -->
					<div class="toolbar row">
						<div class="col-sm-6">
							<div class="page-header">
								<h1>
									Customer Management 
									<small>Editing your customer order here.</small>
								</h1>
							</div>
						</div>
						<div class="col-sm-6 col-xs-12"></div>
					</div>
					<!-- end: PAGE HEADER -->

					<!-- start: BREADCRUMB -->
					<div class="row">
						<div class="col-md-12">
							<ol class="breadcrumb">
								<li>
									<a href="<?=$path['customer'];?>">
										Customer Management
									</a>
								</li>
								<li>
									<a href="<?=$path['customer-edit'].$datas[0]['customer_ID'];?>">
										<?=$datas[0]['customer_fname'];?>
									</a>
								</li>
								<li class="active">
									<?=$datas[0]['customer_fname']."'s Order";?>
								</li>
							</ol>
						</div>
					</div>
					<!-- end: BREADCRUMB -->

					<!-- start: PAGE CONTENT -->
					<?php if(is_array($datas)){?>
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-white">
								<div class="panel-heading border-light">
									<div class="row">
										<div class="col-sm-3 col-xs-12 hidden-xs">
											<a href="<?=$path['customer-edit'].$datas[0]['customer_ID'];?>" type="reset" class="btn btn-default">
												<i class="fa fa-arrow-circle-left"></i> Back
											</a>
										</div>
										<div class="col-sm-6 col-xs-12 text-center">
											<h4 class="panel-title bold"><?=$datas[0]['customer_fname'];?>'s Order History Detail</h3>
										</div>
									</div>
								</div>
								<form name="editCustomer" action="edit.php?action=edit" enctype="multipart/form-data" method="post" >
									<div class="panel-body">
										<div class="form-body">
											<div class="row up2">
						                        <div class="col-xs-12 text-center">
						                            <h4 class="hidden-xs"><span class="label label-danger"><?=$datas[0]['po_name'];?></span>, Date <span class="label label-inverse"><?=$datas[0]['po_date'];?></span></h4>
						                            <h5 class="visible-xs"><span class="label label-danger"><?=$datas[0]['po_name'];?></span>, Date <span class="label label-inverse"><?=$datas[0]['po_date'];?></span></h5>
						                        </div>
						                    </div>

						                    <div class="row up2">
                            					<div class="col-xs-12">
                            						<?php if($datas[0]['po_status'] == "IN PROCESS"){?>
                                					<h5 class="up15 bold">Status: <span class="label label-info"><?=$datas[0]['po_status'];?></span></h5>
                                					<?php }else{?>
                                					<h5 class="up15 bold">Status: <span class="label label-success"><?=$datas[0]['po_status'];?></span></h5>
                                					<?php }?>
                                                    <h5 class="up15 bold">No Resi: <span class="label label-inverse"><?php if($datas[0]['po_resi'] != ""){echo $datas[0]['po_resi'];}else{echo "-";}?></span></h5>
                                                </div>
                    						</div>

                    						<div class="row up3">
                    							<div class="col-sm-6 col-xs-12">
                    								<div class="thumbnail">
	                    								<div class="well bold">BILLING INFO</div>
	                    								<div style="padding: 0 20px 20px 20px;">
		                    								<div><span class="bold">Full Name</span> : <?=$datas[0]['customer_fname'];?></div>
		                    								<div class="up1"><span class="bold">Phone</span> : <?=$datas[0]['customer_phone'];?></div>
		                    								<div class="up1"><span class="bold">E-Mail</span> : <?=$datas[0]['customer_email'];?></div>
	                    								</div>
                    								</div>
                    							</div>
                    							<div class="col-sm-6 col-xs-12">
                    								<div class="thumbnail">
	                    								<div class="well bold">SHIP TO</div>
	                    								<address style="padding: 0 20px 20px 20px;margin: 0;">
	                                                        <?=$datas[0]['po_addressStreet1'];?><br>
	                                                        <?php if($datas[0]['po_addressStreet2'] != ""){echo $datas[0]['po_addressStreet2']."<br>";}?>
	                                                        <?=$datas[0]['po_addressCity'].". ".$datas[0]['po_addressState'].".";?><br>
	                                                        <?=$datas[0]['po_addressCountry'].". ".$datas[0]['po_addressZip'].".";?>
	                                                  	</address>
                                                  	</div>
                    							</div>
                    						</div>
                    						<?php if($datas[0]['counter_product'] != 0){?>
											<div style="text-align:right;margin: 30px 0 10px 0;">
												Total Product : <span class="label label-info"><?=$datas[0]['counter_product'];?></span>
											</div>
											<div class="table-responsive">
												<table class="table table-striped table-bordered">
												  	<tr>
												  		<th>#</th>
												  		<th>Product ID</th>
												  		<th>Product Name</th>
												  		<th>QTY</th>
												  		<th>Price</th>
												  		<th>Line Total</th>
												  	</tr>
												  	<?php $num=1; for($i=0; $i<$datas[0]['counter_product']; $i++){?>
												  	<tr>
												  		<td><?=$num;?></td>
												  		<td>#<?=$datas[0][$i]['atpp_productID'];?></td>
												  		<td><?=$datas[0][$i]['atpp_productName'];?></td>
												  		<td><?=$datas[0][$i]['atpp_qty'];?></td>
												  		<td>Rp <?=number_format(floatval($datas[0][$i]['atpp_price']),0,',','.');?></td>
												  		<td class="bold">Rp <?=number_format(floatval($datas[0][$i]['atpp_qty'] * $datas[0][$i]['atpp_price']),0,',','.');?></td>
												  	</tr>
												  	<?php $num++;}?>
												  	<tr>
												  		<td colspan="5" class="bold text-right">Sub Amount</td>
												  		<td class="bold">Rp <?=number_format(floatval($datas[0]['po_amount']),0,',','.');?></td>
												  	</tr>
												  	<tr>
												  		<td colspan="5" class="bold text-right">Shipping</td>
												  		<td class="bold">Rp <?=number_format(floatval($datas[0]['po_shippingCost']),0,',','.');?></td>
												  	</tr>
												  	<tr>
												  		<td colspan="5" class="bold text-right">Total Amount</td>
												  		<td class="bold">Rp <?=number_format(floatval($datas[0]['po_totalAmount']),0,',','.');?></td>
												  	</tr>
												</table>
											</div>
											<?php } ?>
										</div>
									</div>

								</form>
							</div>
						</div>
					</div>
					<?php }else{?>
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-white">
								<div class="panel-heading border-light">
									<h4 class="panel-title">There is no data order in this page!</h4>
								</div>
							</div>
						</div>
					</div>
					<?php } ?>
					<!-- end: PAGE CONTENT-->

				</div>
				<div class="subviews">
					<div class="subviews-container"></div>
				</div>

			</div>
			<!-- end: PAGE -->
		</div>
		<!-- end: MAIN CONTAINER -->

		<!-- start: FOOTER -->
		<?php include("../../parts/part-footer.php");?>
		<!-- end: FOOTER -->

		<!-- start: SUBVIEW SAMPLE CONTENTS -->
		<?php include("../../parts/part-sample_content.php");?>
		<!-- end: SUBVIEW SAMPLE CONTENTS -->

	</div>

	<?php include("../../packages/footer-js.php");?>
	<!-- Add fancyBox -->
	<script type="text/javascript" src="<?=$global['absolute-url-admin'];?>packages/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$(".fancybox").fancybox({
				padding : 0
			});
		});
		//use session here for alert success/failed
		var alertText = "success/error text"; //teks for alert

		//success alert
		//successAlert(alertText); 

		//error alert
		//errorAlert(alertText); 

		//function confirmation delete
		function confirmDelete(num){
			swal({
				title: "Are you sure?",
				text: "You will not be able to recover this file!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Delete ! ",
				cancelButtonText: "Cancel !",
				closeOnConfirm: false,
				closeOnCancel: true
			},
			function (isConfirm) {
				if (isConfirm) {
					window.location.href = "index.php?action=delete&id="+num;
				} else {
	                   //nothing
	               }
	           });
		}
		</script>

	</body>
	<!-- end: BODY -->
	</html>