<?php 
include("../../packages/require.php");
include("../../packages/check_login.php");//USED BY ALL PAGE BUT index.php
include("../../controller/controller_customer.php");
$curpage="customer";
$page_name = "index.php";
?>
<!DOCTYPE html>
<html lang="en">
<!-- start: HEAD -->
<head>
	<title><?=$title['customer'];?></title>
	<?php include("../../packages/module-head.php");?>
</head>
<!-- end: HEAD -->
<!-- start: BODY -->
<body>
	<!-- start: SLIDING BAR (SB) -->
	<?php include("../../parts/part-sliding_bar.php");?>
	<!-- end: SLIDING BAR -->
	<div class="main-wrapper">
		<!-- start: TOPBAR -->
		<?php include("../../parts/part-top_bar.php");?>
		<!-- end: TOPBAR -->
		<!-- start: PAGESLIDE LEFT -->
		<?php include("../../parts/part-pageslide_left.php");?>
		<!-- end: PAGESLIDE LEFT -->
		<!-- start: PAGESLIDE RIGHT -->
		<?php include("../../parts/part-pageslide_right.php");?>
		<!-- end: PAGESLIDE RIGHT -->
		<!-- start: MAIN CONTAINER -->
		<div class="main-container inner">
			<!-- start: PAGE -->
			<div class="main-content">
				
				<div class="container">
					<!-- start: PAGE HEADER -->
					<div class="toolbar row">
						<div class="col-sm-6">
							<div class="page-header">
								<h1>
									Customer Management 
									<small>Editing your customer order here.</small>
								</h1>
							</div>
						</div>
						<div class="col-sm-6 col-xs-12"></div>
					</div>
					<!-- end: PAGE HEADER -->
					<!-- start: BREADCRUMB -->
					<div class="row">
						<div class="col-md-12">
							<ol class="breadcrumb">
								<li class="active">
									Customer Management
								</li>
							</ol>
						</div>
					</div>
					<!-- end: BREADCRUMB -->
					<!-- start: PAGE CONTENT -->
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-white">
								<div class="panel-heading">
									<div style="text-align:left;">
										Total Customer : <span class="label label-info"><?=$total_data;?></span>
									</div>
									<div class="panel-tools">
										<div class="dropdown">
											<a data-toggle="dropdown" class="btn btn-xs dropdown-toggle btn-transparent-grey" aria-expanded="false">
												<i class="fa fa-cog"></i>
											</a>
											<ul class="dropdown-menu dropdown-light pull-right" role="menu" style="display: none;">
												<li>
													<a class="panel-collapse collapses" href="table_data.html#"><i class="fa fa-angle-up"></i> <span>Collapse</span> </a>
												</li>
												<li>
													<a class="panel-refresh" href="table_data.html#">
														<i class="fa fa-refresh"></i> <span>Refresh</span>
													</a>
												</li>
												<li>
													<a class="panel-config" href="table_data.html#panel-config" data-toggle="modal">
														<i class="fa fa-wrench"></i> <span>Configurations</span>
													</a>
												</li>
												<li>
													<a class="panel-expand" href="table_data.html#">
														<i class="fa fa-expand"></i> <span>Fullscreen</span>
													</a>
												</li>
											</ul>
										</div>
									</div>
								</div>
								<div class="panel-body">
									<div class="table-responsive">
										<table class="table table-striped table-bordered table-hover table-full-width">
											<tr>
												<th>#</th>
												<th>Full Name</th>
												<th>Phone</th>
												<th>E-Mail</th>
												<th>Last Purchase</th>
												<th>Action</th>
											</tr>
											<?php if(is_array($datas)) { $num=1;foreach($datas as $data) { ?>
											<tr>
												<td><?=($O_page-1)*10+$num;?></td>
												<td><?=$data['customer_fname'];?></td>
												<td><?=$data['customer_phone'];?></td>
												<td><?=$data['customer_email'];?></td>
												<td><?php if($data['po_date'] != null){echo date('F j, Y',strtotime($data['po_date']));}else{echo "not yet";}?></td>
												<td>
													<div class="text-center">
														<a href="<?=$path['customer-edit'].$data['customer_ID'];?>" class="btn btn-xs btn-blue tooltips" data-placement="top" data-original-title="View"><i class="fa fa-search"></i></a>
													</div>
												</td>
											</tr>
											<?php $num++;} } else { ?>
											<tr class="warning">
												<td colspan="6" class="bold">There is no data right now!</td>
											</tr>
											<?php } ?>
										</table>
								</div>
										<!-- start pagination -->
										<div class="part-pagination part-pagination-customer" style="">
											<ul class="pagination pagination-blue margin-bottom-10">
												<?php
												$batch = getBatch($O_page);
												if($batch < 1){$batch = 1;}
												$prevLimit = 1 +(10*($batch-1));
												$nextLimit = 10 * $batch;
												if($nextLimit > $total_page){
													$nextLimit = $total_page;
												}
												if ($total_page > 1 && $O_page > 1) {
													echo "<li><a href='".$page_name."?page=1'><i class='fa fa-chevron-left'></i><i class='fa fa-chevron-left'></i> First</a></li>";
												}
												if($batch > 1 && $O_page > 10){
													echo "<li><a href='".$page_name."?page=".($prevLimit-1)."><i class='fa fa-chevron-left'></i> Previous 10</a></li>";
												}
												for($mon = $prevLimit; $mon <= $nextLimit;$mon++){ ?>
												<li class="<?php if($mon == $O_page){echo 'active';}?>"><a href="<?php echo $page_name."?page=".$mon;?>" ><?php echo $mon;?></a></li>
												<?php if($mon == $nextLimit && $mon < $total_page){
													if($mon >= $total_page){ 
														$mon -=1;
													}
													echo "<li><a href='".$page_name."?page=".($mon+1)."'>Next 10 <i class='fa fa-chevron-right'></i></a></li>";
												}                               
											}
											if ($total_page > 1 &&  ($O_page != $total_page)) {
												echo "<li><a href='".$page_name."?page=".$total_page."'>Last <i class='fa fa-chevron-right'></i><i class='fa fa-chevron-right'></i></a></li>";
											}
											?>
										</ul>
									</div>
									<!-- end pagination -->
							</div>
						</div>
					</div>
				</div>
				<!-- end: PAGE CONTENT-->
			</div>
			<div class="subviews">
				<div class="subviews-container"></div>
			</div>
		</div>
		<!-- end: PAGE -->
	</div>
	<!-- end: MAIN CONTAINER -->
	<!-- start: FOOTER -->
	<?php include("../../parts/part-footer.php");?>
	<!-- end: FOOTER -->
	<!-- start: SUBVIEW SAMPLE CONTENTS -->
	<?php include("../../parts/part-sample_content.php");?>
	<!-- end: SUBVIEW SAMPLE CONTENTS -->
</div>
<?php include("../../packages/footer-js.php");?>
<script type="text/javascript">
			//use session here for alert success/failed
			var alertText = "success/error text"; //teks for alert
			//success alert
			//successAlert(alertText); 
			//error alert
			//errorAlert(alertText); 
			//function confirmation delete
			function confirmDelete(num){
				swal({
					title: "Are you sure?",
					text: "You will not be able to recover this file!",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Delete ! ",
					cancelButtonText: "Cancel !",
					closeOnConfirm: false,
					closeOnCancel: true
				},
				function (isConfirm) {
					if (isConfirm) {
						window.location.href = "index.php?action=delete&id="+num;
					} else {
	                    //nothing
	                }
	            });
			}
		</script>
	</body>
	<!-- end: BODY -->
	</html>