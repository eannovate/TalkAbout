<?php 
include("../../packages/require.php");
include("../../packages/check_login.php");//USED BY ALL PAGE BUT index.php
include("../../controller/controller_customer_wishlist.php");
$curpage="customer";
$page_name = "wishlist.php";
?>
<!DOCTYPE html>
<html lang="en">
<!-- start: HEAD -->
<head>
	<title><?=$title['customer'];?></title>
	<?php include("../../packages/module-head.php");?>
	<!-- Add fancyBox -->
	<link rel="stylesheet" href="<?=$global['absolute-url-admin'];?>packages/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
</head>
<!-- end: HEAD -->
<!-- start: BODY -->
<body>
	<!-- start: SLIDING BAR (SB) -->
	<?php include("../../parts/part-sliding_bar.php");?>
	<!-- end: SLIDING BAR -->

	<div class="main-wrapper">

		<!-- start: TOPBAR -->
		<?php include("../../parts/part-top_bar.php");?>
		<!-- end: TOPBAR -->

		<!-- start: PAGESLIDE LEFT -->
		<?php include("../../parts/part-pageslide_left.php");?>
		<!-- end: PAGESLIDE LEFT -->

		<!-- start: PAGESLIDE RIGHT -->
		<?php include("../../parts/part-pageslide_right.php");?>
		<!-- end: PAGESLIDE RIGHT -->

		<!-- start: MAIN CONTAINER -->
		<div class="main-container inner">
			<!-- start: PAGE -->
			<div class="main-content">
				
				<div class="container">

					<!-- start: PAGE HEADER -->
					<div class="toolbar row">
						<div class="col-sm-6">
							<div class="page-header">
								<h1>
									Customer Management 
									<small>Your customer wishlist here.</small>
								</h1>
							</div>
						</div>
						<div class="col-sm-6 col-xs-12"></div>
					</div>
					<!-- end: PAGE HEADER -->

					<!-- start: BREADCRUMB -->
						<div class="row">
							<div class="col-md-12">
								<ol class="breadcrumb">
									<li>
									<a href="<?=$path['customer'];?>">
											Customer Management
										</a>
									</li>
									<li>
										<a href="<?=$path['customer-edit'].$O_id;?>">
											<?=$O_name;?>
										</a>
									</li>
									<li class="active">
										<?=$O_name."'s Wishlist";?>
									</li>
								</ol>
							</div>
						</div>
						<!-- end: BREADCRUMB -->

					<!-- start: PAGE CONTENT -->
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-white">
								<div class="panel-heading">
									<div style="text-align:left;">
										Total Wishlist : <span class="label label-info"><?=$total_data;?></span>
									</div>
									<div class="panel-tools">
										<div class="dropdown">
											<a data-toggle="dropdown" class="btn btn-xs dropdown-toggle btn-transparent-grey" aria-expanded="false">
												<i class="fa fa-cog"></i>
											</a>
											<ul class="dropdown-menu dropdown-light pull-right" role="menu" style="display: none;">
												<li>
													<a class="panel-collapse collapses" href="table_data.html#"><i class="fa fa-angle-up"></i> <span>Collapse</span> </a>
												</li>
												<li>
													<a class="panel-refresh" href="table_data.html#">
														<i class="fa fa-refresh"></i> <span>Refresh</span>
													</a>
												</li>
												
												<li>
													<a class="panel-expand" href="table_data.html#">
														<i class="fa fa-expand"></i> <span>Fullscreen</span>
													</a>
												</li>
											</ul>
										</div>
									</div>
								</div>
								<div class="panel-body">
									<div class="table-responsive">
										<table class="table table-striped table-bordered table-hover table-full-width">
											<tr>
												<th>#</th>
												<th>Product Name</th>
												<th>Category</th>
												<th>Price</th>
												<th>Image</th>
												<th>Add date</th>												
											</tr>
											<?php if(is_array($datas)) { $num=1;foreach($datas as $data) { ?>
											<tr>
												<td><?=($O_page-1)*10+$num;?></td>
												<td><?=$data['product_name'];?></td>
												<td><?=$data['category_name'];?></td>
												<?php if($data['product_specialPrice'] != 0) { ?>
													<td>
													<font color="red"><strike>Rp. <?=$data['product_price'];?> </strike> </font> <br>
													Rp. <?=$data['product_specialPrice'];?></td>
												<?php } else { ?>
													<td>Rp. <?=$data['product_price'];?></td>
												<?php } ?>
												<td>
												  	<a class="fancybox" href="<?=$global['absolute-url'].$data['photo_imgLoc'];?>">
												  		<img style="width: 40px;" class="img-circle" src="<?=$global['absolute-url'].$data['photo_imgLocThmb'];?>">
												  	</a>
											  	</td>

											  	<td><?php echo date("d/m/Y",strtotime($data['aw_timestamp'])) ;?></td>
															
												
											</tr>
											<?php $num++;} } else { ?>
											<tr class="warning">
												<td colspan="6" class="bold">There is no data right now!</td>
											</tr>
											<?php } ?>
										</table>
								</div>
										<!-- start pagination -->
										<div class="part-pagination part-pagination-customer" style="">
											<ul class="pagination pagination-blue margin-bottom-10">
												<?php
												$batch = getBatch($O_page);
												if($batch < 1){$batch = 1;}
												$prevLimit = 1 +(10*($batch-1));
												$nextLimit = 10 * $batch;

												if($nextLimit > $total_page){
													$nextLimit = $total_page;
												}
												if ($total_page > 1 && $O_page > 1) {
													echo "<li><a href='".$page_name."?page=1'><i class='fa fa-chevron-left'></i><i class='fa fa-chevron-left'></i> First</a></li>";
												}
												if($batch > 1 && $O_page > 10){
													echo "<li><a href='".$page_name."?page=".($prevLimit-1)."><i class='fa fa-chevron-left'></i> Previous 10</a></li>";
												}
												for($mon = $prevLimit; $mon <= $nextLimit;$mon++){ ?>
												<li class="<?php if($mon == $O_page){echo 'active';}?>"><a href="<?php echo $page_name."?page=".$mon;?>" ><?php echo $mon;?></a></li>
												<?php if($mon == $nextLimit && $mon < $total_page){
													if($mon >= $total_page){ 
														$mon -=1;
													}
													echo "<li><a href='".$page_name."?page=".($mon+1)."'>Next 10 <i class='fa fa-chevron-right'></i></a></li>";
												}                               
											}
											if ($total_page > 1 &&  ($O_page != $total_page)) {
												echo "<li><a href='".$page_name."?page=".$total_page."'>Last <i class='fa fa-chevron-right'></i><i class='fa fa-chevron-right'></i></a></li>";
											}
											?>
										</ul>
									</div>
									<!-- end pagination -->
							</div>
								<div class="panel-footer">
										<div class="row">
											<div class="col-xs-12 pad0 text-right">
												<div class="btn-group text-right hidden-xs">
													<a href="<?=$path['customer-edit'].$O_id;?>" type="reset" class="btn btn-default">
														<i class="fa fa-times"></i> Back
													</a>
													
												</div>
												<div class="btn-group visible-xs">
													<div class="row">
														<div class="col-xs-12 text-right">
															<a href="<?=$path['customer-edit'].$O_id;?>" type="reset" class="btn btn-default">
																<i class="fa fa-times"></i> Back
															</a>
															
														</div>
													</div>

												</div>
											</div>
										</div>
								</div>
						</div>
					</div>
				</div>
				<!-- end: PAGE CONTENT-->

			</div>
			<div class="subviews">
				<div class="subviews-container"></div>
			</div>

		</div>
		<!-- end: PAGE -->
	</div>
	<!-- end: MAIN CONTAINER -->

	<!-- start: FOOTER -->
	<?php include("../../parts/part-footer.php");?>
	<!-- end: FOOTER -->

	<!-- start: SUBVIEW SAMPLE CONTENTS -->
	<?php include("../../parts/part-sample_content.php");?>
	<!-- end: SUBVIEW SAMPLE CONTENTS -->

</div>

<?php include("../../packages/footer-js.php");?>

<!-- Add fancyBox -->
		<script type="text/javascript" src="<?=$global['absolute-url-admin'];?>packages/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>

		<script type="text/javascript">
			$(document).ready(function() {
				$(".fancybox").fancybox({
					padding : 0
				});
			});
		</script>

<script type="text/javascript">
			//use session here for alert success/failed
			var alertText = "success/error text"; //teks for alert

			//success alert
			//successAlert(alertText); 

			//error alert
			//errorAlert(alertText); 

			//function confirmation delete
			function confirmDelete(num){
				swal({
					title: "Are you sure?",
					text: "You will not be able to recover this file!",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Delete ! ",
					cancelButtonText: "Cancel !",
					closeOnConfirm: false,
					closeOnCancel: true
				},
				function (isConfirm) {
					if (isConfirm) {
						window.location.href = "index.php?action=delete&id="+num;
					} else {
	                    //nothing
	                }
	            });
			}
		</script>
	</body>
	<!-- end: BODY -->
	</html>