<?php 
include("../../packages/require.php");
include("../../controller/controller_wishlist.php");
include("../../packages/check_login.php");//USED BY ALL PAGE BUT index.php
$curpage="wishlist";
$page_name = "index.php";
?>
<!DOCTYPE html>
<html lang="en">
<!-- start: HEAD -->
<head>
	<title><?=$title['wishlist'];?></title>
	<?php include("../../packages/module-head.php");?>
</head>
<!-- end: HEAD -->
<!-- start: BODY -->
<body>
	<!-- start: SLIDING BAR (SB) -->
	<?php include("../../parts/part-sliding_bar.php");?>
	<!-- end: SLIDING BAR -->
	
	<div class="main-wrapper">
		<!-- start: TOPBAR -->
		<?php include("../../parts/part-top_bar.php");?>
		<!-- end: TOPBAR -->
		<!-- start: PAGESLIDE LEFT -->
		<?php include("../../parts/part-pageslide_left.php");?>
		<!-- end: PAGESLIDE LEFT -->
		<!-- start: PAGESLIDE RIGHT -->
		<?php include("../../parts/part-pageslide_right.php");?>
		<!-- end: PAGESLIDE RIGHT -->
		<!-- start: MAIN CONTAINER -->
		<div class="main-container inner">
			<!-- start: PAGE -->
			<div class="main-content">
				
				<div class="container">
					<!-- start: PAGE HEADER -->
					<div class="toolbar row">
						<div class="col-sm-6">
							<div class="page-header">
								<h1>
									WISH LIST
									<small>Check customer Wish List here.</small>
								</h1>
							</div>
						</div>
						<div class="col-sm-6 col-xs-12"></div>
					</div>
					<!-- end: PAGE HEADER -->
					<!-- start: BREADCRUMB -->
					<div class="row">
						<div class="col-md-12">
							<ol class="breadcrumb">
								<li class="active">
									Wish List Management
								</li>
							</ol>
						</div>
					</div>
					<!-- end: BREADCRUMB -->
					<!-- start: PAGE CONTENT -->
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-white">
								<div class="panel-heading">
									<div style="text-align:left;">
										Total Wish List : <span class="label label-info"><?=$total_data;?></span>
									</div>
									<div class="panel-tools">
										<div class="dropdown">
											<a data-toggle="dropdown" class="btn btn-xs dropdown-toggle btn-transparent-grey" aria-expanded="false">
												<i class="fa fa-cog"></i>
											</a>
											<ul class="dropdown-menu dropdown-light pull-right" role="menu" style="display: none;">
												<li>
													<a class="panel-collapse collapses" href="table_data.html#"><i class="fa fa-angle-up"></i> <span>Collapse</span> </a>
												</li>
												<li>
													<a class="panel-refresh" href="table_data.html#">
														<i class="fa fa-refresh"></i> <span>Refresh</span>
													</a>
												</li>
												<li>
													<a class="panel-config" href="table_data.html#panel-config" data-toggle="modal">
														<i class="fa fa-wrench"></i> <span>Configurations</span>
													</a>
												</li>
												<li>
													<a class="panel-expand" href="table_data.html#">
														<i class="fa fa-expand"></i> <span>Fullscreen</span>
													</a>
												</li>
											</ul>
										</div>
									</div>
								</div>
								<div class="panel-body">
									<div class="table-responsive">
										<table class="table table-striped table-bordered table-hover table-full-width">
											<tr>
												<th style = "text-align: center">#</th>
												<th style = "text-align: center">Product Name</th>
												<th style = "text-align: center">Product Image</th>
												<th style = "text-align: center">Number of Wish</th>	
											</tr>
											<?php if(is_array($data_wishlists)) { $num=1;foreach($data_wishlists as $data_wishlist) { ?>
											<tr class="<?php orderRow($data_wishlist['number_wish']);?> ">
												<td style = "text-align: center"><?=$num;?>.</td>
												<td><?=$data_wishlist['product_name'];?></td>
												<td style = "text-align: center">
													<a class="fancybox" href="<?=$global['absolute-url'].$data_wishlist['photo_imgLoc'];?>">
												  				<img style="width: 40px;" align="middle" class="img-circle" src="<?=$global['absolute-url'].$data_wishlist['photo_imgLocThmb'];?>">
												  	</a>
											  	</td>
												<td style = "text-align: center">
												 	<?=$data_wishlist['number_wish'];?>
												 		
												</td>
												
											</tr>
											<?php $num++;} } else { ?>
											<tr class="warning">
												<td colspan="8" class="bold">There is no data right now!</td>
											</tr>
											<?php } ?>
										</table>
									</div>
									<!-- start pagination -->
										<div class="part-pagination part-pagination-customer" style="">
											<ul class="pagination pagination-blue margin-bottom-10">
												<?php
												$batch = getBatch($O_page);
												if($batch < 1){$batch = 1;}
												$prevLimit = 1 +(5*($batch-1));
												$nextLimit = 5 * $batch;
												if($nextLimit > $total_page){
													$nextLimit = $total_page;
												}
												if($O_page > 1){
													echo "<li><a href='".$page_name."?page=".($O_page-1)."'><i class='fa fa-chevron-left'></i> Prev</a></li>";
												}
												for($mon = $prevLimit; $mon <= $nextLimit;$mon++){ 
												?>
												<li class="<?php if($mon == $O_page){echo 'active';}?>"><a href="<?php echo $page_name."?page=".$mon;?>" ><?php echo $mon;?></a></li>
												<?php 
												}
												if($total_page > 1 && $O_page != $total_page){
													echo "<li><a href='".$page_name."?page=".($O_page+1)."'>Next <i class='fa fa-chevron-right'></i></a></li>";
												}
												?>
											</ul>
										</div>
									<!-- end pagination -->
								</div>
							</div>
						</div>
					</div>
					<!-- end: PAGE CONTENT-->
				</div>
				<div class="subviews">
					<div class="subviews-container"></div>
				</div>
			</div>
			<!-- end: PAGE -->
		</div>
		<!-- end: MAIN CONTAINER -->
		<!-- start: FOOTER -->
		<?php include("../../parts/part-footer.php");?>
		<!-- end: FOOTER -->
		<!-- start: SUBVIEW SAMPLE CONTENTS -->
		<?php include("../../parts/part-sample_content.php");?>
		<!-- end: SUBVIEW SAMPLE CONTENTS -->
	</div>
	<?php include("../../packages/footer-js.php");?>
	<script type="text/javascript">
		function transferPOid(x){
			var currentPOid = $(x).attr("data-poid");
			var currentPOresi = $(x).attr("data-poresi");
			var currentPOName = $(x).attr("data-poName");
			var currentCustName = $(x).attr("data-custName");
			var currentCustEmail = $(x).attr("data-custEmail");
			$('#PO_ID_modal').val(currentPOid);
			$('#PO_resi_modal').val(currentPOresi);
			$('#PO_name_modal').val(currentPOName);
			$('#Cust_name_modal').val(currentCustName);
			$('#Cust_email_modal').val(currentCustEmail);			
			
				
			<?php if($data['po_status'] == 3 or $data['po_status'] == 4 ) {	?>		
			$('#PO_resi_modal').attr("disabled", false);
			$('#btn-submit').attr("disabled", false);
			<?php } else { ?>
			$('#PO_resi_modal').attr("disabled", true);
			<?php } ?>
			     
		}
			function validateResi(){
                var resi = $("#PO_resi_modal").val();
                
                if(resi != ""){
                    $("#error-resi").html("");
                    $("#error-resi").hide();
                    $("#PO_resi_modal").removeClass("input-error");
                } else {
                    $("#error-resi").show();
                    $("#error-resi").html("<i class='fa fa-warning'></i> This field is required.");
                    $("#PO_resi_modal").addClass("input-error");
                    return false;
                }
            }
			<?php if($message != "") { ?>
            //use session here for alert success/failed
            var alertText = "<?=$message;?>"; //teks for alert
            
                <?php if($alert != "success"){ ?>
                    //error alert
                    errorAlert(alertText);
                <?php } else { ?>
                    //success alert
                    successAlert(alertText); 
                <?php } ?>
             
            <?php } ?>
			//errorAlert(alertText); 
			//function confirmation delete
			function confirmDelete(num){
				swal({
					title: "Are you sure?",
					text: "You will not be able to recover this file!",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Delete ! ",
					cancelButtonText: "Cancel !",
					closeOnConfirm: false,
					closeOnCancel: true
				},
				function (isConfirm) {
					if (isConfirm) {
						window.location.href = "index.php?action=delete&id="+num;
					} else {
	                    //nothing
	                }
	            });
			}
		</script>
	</body>
	<!-- end: BODY -->
	</html>