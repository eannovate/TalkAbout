<?php 
	include("../../packages/require.php");
	include("../../packages/check_login.php");//USED BY ALL PAGE BUT index.php
	include("../../controller/controller_gallery.php");
	$curpage="gallery";
	$page_name = "index.php";
?>
<!DOCTYPE html>
<html lang="en">
	<!-- start: HEAD -->
	<head>
		<title><?=$title['gallery'];?></title>
		<?php include("../../packages/module-head.php");?>
		<!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
		<link rel="stylesheet" href="<?=$global['absolute-url-admin'];?>assets/plugins/lightbox2/css/lightbox.css">
		<!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
	</head>
	<!-- end: HEAD -->
	<!-- start: BODY -->
	<body>
		<!-- start: SLIDING BAR (SB) -->
		<?php include("../../parts/part-sliding_bar.php");?>
		<!-- end: SLIDING BAR -->

		<div class="main-wrapper">

			<!-- start: TOPBAR -->
			<?php include("../../parts/part-top_bar.php");?>
			<!-- end: TOPBAR -->

			<!-- start: PAGESLIDE LEFT -->
			<?php include("../../parts/part-pageslide_left.php");?>
			<!-- end: PAGESLIDE LEFT -->

			<!-- start: PAGESLIDE RIGHT -->
			<?php include("../../parts/part-pageslide_right.php");?>
			<!-- end: PAGESLIDE RIGHT -->

			<!-- start: MAIN CONTAINER -->
			<div class="main-container inner">
				<!-- start: PAGE -->
				<div class="main-content">
					
					<div class="container">

						<!-- start: PAGE HEADER -->
						<div class="toolbar row">
							<div class="col-sm-6">
								<div class="page-header">
									<h1>
										Gallery Management 
										<small>Managing your gallery photo here.</small>
									</h1>
								</div>
							</div>
							<div class="col-sm-6 col-xs-12"></div>
						</div>
						<!-- end: PAGE HEADER -->

						<!-- start: BREADCRUMB -->
						<div class="row">
							<div class="col-md-12">
								<ol class="breadcrumb">
									<li class="active">
										Gallery Management
									</li>
								</ol>
							</div>
						</div>
						<!-- end: BREADCRUMB -->

						<!-- start: PAGE CONTENT -->
						<div class="row">
							<div class="col-md-12">
								<div class="panel panel-white">
									<div class="panel-heading border-light text-right">
										<a class="btn btn-light-azure" href="#panel-add" data-toggle="modal" data-target="#panel-add">
											<i class="fa fa-plus"></i> Add New Data
										</a>
									</div>
									<?php if(is_array($datas)){?>
									<div class="panel-body">
										<div style="text-align:right;margin: 10px 0;">
											Total Data : <span class="label label-info"><?=$total_data;?></span>
										</div>

										<ul id="Grid" class="list-unstyled up3">
											<?php $num=1; foreach($datas as $data){?>
											<li class="col-md-3 col-sm-6 col-xs-6 mix category_<?=$num;?> gallery-img" data-cat="<?=$num;?>" style="display: inline-block;">
												<div class="portfolio-item">
													<a class="thumb-info" href="<?=$global['absolute-url'].$data['portfolio_logo'];?>" data-lightbox="gallery" data-title="<?=$data['portfolio_name'];?>">
														<img src="<?=$global['absolute-url'].$data['portfolio_logoThmb'];?>" class="img-responsive" alt="<?=$data['portfolio_name'];?>" width="100%">
														<span class="thumb-info-title"> <?=$data['portfolio_name'];?> </span>
													</a>
													<div class="tools tools-bottom">
														<a href="<?=$path['gallery-edit'].$data['portfolio_ID'];?>">
															<i class="fa fa-pencil"></i>
														</a>
														<a href="javascript:void(0)" onclick="confirmDelete('<?=$data['portfolio_ID'];?>', '<?=$data['portfolio_name'];?>');">
															<i class="fa fa-trash-o"></i>
														</a>
													</div>
												</div>
											</li>
											<?php $num++;}?>
											<li class="gap"></li>
											<!-- "gap" elements fill in the gaps in justified grid -->
										</ul>

										<!-- start pagination -->
										<div class="part-pagination">
											<ul class="pagination pagination-blue margin-bottom-10">
					                            <?php
					                            $batch = getBatch($O_page);
					                            if($batch < 1){$batch = 1;}
					                            $prevLimit = 1 +(10*($batch-1));
					                            $nextLimit = 10 * $batch;

					                            if($nextLimit > $total_page){
					                                $nextLimit = $total_page;
					                            }
					                            if ($total_page > 1 && $O_page > 1) {
					                                echo "<li><a href='".$page_name."?page=1'><i class='fa fa-chevron-left'></i><i class='fa fa-chevron-left'></i> First</a></li>";
					                            }
					                            if($batch > 1 && $O_page > 10){
					                                echo "<li><a href='".$page_name."?page=".($prevLimit-1)."><i class='fa fa-chevron-left'></i> Previous 10</a></li>";
					                            }
					                            for($mon = $prevLimit; $mon <= $nextLimit;$mon++){ ?>
					                                <li class="<?php if($mon == $O_page){echo 'active';}?>"><a href="<?php echo $page_name."?page=".$mon;?>" ><?php echo $mon;?></a></li>
					                                <?php if($mon == $nextLimit && $mon < $total_page){
					                                    if($mon >= $total_page){ 
					                                        $mon -=1;
					                                    }
					                                    echo "<li><a href='".$page_name."?page=".($mon+1)."'>Next 10 <i class='fa fa-chevron-right'></i></a></li>";
					                                }                               
					                            }
					                            if ($total_page > 1 &&  ($O_page != $total_page)) {
					                                echo "<li><a href='".$page_name."?page=".$total_page."'>Last <i class='fa fa-chevron-right'></i><i class='fa fa-chevron-right'></i></a></li>";
					                            }
					                            ?>
					                        </ul>
										</div>
										<!-- end pagination -->

									</div>
									<?php }else{?>
									<div class="panel-body">
										<div style="text-align:left;margin: 10px 0;">
											There is no data right now!
										</div>
									</div>
									<?php }?>
								</div>
							</div>
						</div>
						<!-- end: PAGE CONTENT-->

					</div>
					<div class="subviews">
						<div class="subviews-container"></div>
					</div>

					<!-- start: PANEL ADD MODAL FORM -->
					<div class="modal fade" id="panel-add" tabindex="-1" role="dialog" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
										&times;
									</button>
									<h4 class="modal-title">Add New Gallery Image</h4>
								</div>
								<form name="addGallery" action="index.php?action=add" enctype="multipart/form-data" method="post" onsubmit="return validateForm();" >
									<div class="modal-body">
										<div class="row">
		                                    <div class="col-sm-4 col-xs-12 up1 form-label"><strong>Gallery Name <span class="symbol required"></span></strong></div>
		                                    <div class="col-sm-8 col-xs-12 up1">
		                                    	<input id="input-title" name="title" type="text" class="form-control" placeholder="gallery name" />
		                                    	<div id="error-title" class="is-error"></div>
		                                    </div>
		                                </div>
		                                <div class="row">
		                                    <div class="col-sm-4 col-xs-12 up1 form-label">Gallery Description </div>
		                                    <div class="col-sm-8 col-xs-12 up1">
		                                    	<textarea name="desc" class="form-control" placeholder="gallery description" rows="5"></textarea>
		                                    </div>
		                                </div>
		                                <div class="row">       
		                                    <div class="col-sm-4 col-xs-12 up1 form-label"><strong>Image Upload <span class="symbol required"></span></strong></div>
		                                    <div class="col-sm-8 col-xs-12 up1">
		                                    	<input id="input-image" name="image" type="file" class="text" placeholder="gallery image" />
		                                    	<div id="error-image" class="is-error"></div>
		                                    </div>
		                                </div>
		                                <div class="row">
		                                	<div class="col-sm-4 hidden-xs up1"></div>
		                                    <div class="col-sm-8 col-xs-12 up1"><small><span class="help-block"><i class="fa fa-info-circle"></i> <?php echo $infoSize['gallery'];?></span></small></div>
		                                </div>
									</div>
									<div class="modal-footer f5-bg">
										<div class="btn-group">
											<button type="reset" class="btn btn-default" data-dismiss="modal">
												<i class="fa fa-times"></i> Cancel
											</button>
											<button type="submit" class="btn btn-success">
												<i class="fa fa-check fa fa-white"></i> Create
											</button>
										</div>
									</div>
								</form>
							</div>
							<!-- /.modal-content -->
						</div>
						<!-- /.modal-dialog -->
					</div>
					<!-- /.modal -->
					<!-- end: SPANEL CONFIGURATION MODAL FORM -->

				</div>
				<!-- end: PAGE -->
			</div>
			<!-- end: MAIN CONTAINER -->

			<!-- start: FOOTER -->
			<?php include("../../parts/part-footer.php");?>
			<!-- end: FOOTER -->

			<!-- start: SUBVIEW SAMPLE CONTENTS -->
			<?php include("../../parts/part-sample_content.php");?>
			<!-- end: SUBVIEW SAMPLE CONTENTS -->

		</div>

		<?php include("../../packages/footer-js.php");?>
		<!-- Add fancyBox -->
		<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
		<script src="<?=$global['absolute-url-admin'];?>assets/plugins/mixitup/src/jquery.mixitup.js"></script>
		<script src="<?=$global['absolute-url-admin'];?>assets/plugins/lightbox2/js/lightbox.min.js"></script>
		<script src="<?=$global['absolute-url-admin'];?>assets/js/pages-gallery.js"></script>
		<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
		<script type="text/javascript">
			function validateForm(){
				var title = $("#input-title").val();
				var image = $("#input-image").val();
				
				if(title != ""){
					$("#error-title").html("");
					$("#error-title").hide();
					$("#input-title").removeClass("input-error");
				} else {
					$("#error-title").show();
					$("#error-title").html("<i class='fa fa-warning'></i> This field is required.");
					$("#input-title").addClass("input-error");
					return false;
				}
				if(image != ""){
					$("#error-image").html("");
					$("#error-image").hide();
					$("#input-image").removeClass("input-error");
				} else {
					$("#error-image").show();
					$("#error-image").html("<i class='fa fa-warning'></i> Please upload your image.");
					$("#input-image").addClass("input-error");
					return false;
				}
			}
			<?php if($message != "") { ?>
			//use session here for alert success/failed
			var alertText = "<?=$message;?>"; //teks for alert
			
				<?php if($alert != "success"){ ?>
					//error alert
					errorAlert(alertText);
				<?php } else { ?>
					//success alert
					successAlert(alertText); 
				<?php } ?>
			 
			<?php } ?>

			//function confirmation delete
			function confirmDelete(num, text){
				swal({
	                title: "Are you sure?",
	                text: "You will not be able to recover this file!",
	                type: "warning",
	                showCancelButton: true,
	                confirmButtonColor: "#DD6B55",
	                confirmButtonText: "Delete ! ",
	                cancelButtonText: "Cancel !",
	                closeOnConfirm: false,
	                closeOnCancel: true
	            },
	            function (isConfirm) {
	                if (isConfirm) {
	                    window.location.href = "index.php?action=delete&id="+num+"&title="+text;
	                } else {
	                    //nothing
	                }
	            });
			}
		</script>
	</body>
	<!-- end: BODY -->
</html>