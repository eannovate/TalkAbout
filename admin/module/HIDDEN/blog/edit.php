<?php 
	include("../../packages/require.php");
	include("../../packages/check_login.php");//USED BY ALL PAGE BUT index.php
	include("../../controller/controller_blog_detail.php");
	$curpage="blog";
?>
<!DOCTYPE html>
<html lang="en">
<!-- start: HEAD -->
<head>
	<title><?=$title['blog'];?></title>
	<?php include("../../packages/module-head.php");?>
	<!-- Add fancyBox -->
	<link rel="stylesheet" href="<?=$global['absolute-url-admin'];?>packages/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
</head>
<!-- end: HEAD -->
<!-- start: BODY -->
<body>
	<!-- start: SLIDING BAR (SB) -->
	<?php include("../../parts/part-sliding_bar.php");?>
	<!-- end: SLIDING BAR -->
	<div class="main-wrapper">
		<!-- start: TOPBAR -->
		<?php include("../../parts/part-top_bar.php");?>
		<!-- end: TOPBAR -->
		<!-- start: PAGESLIDE LEFT -->
		<?php include("../../parts/part-pageslide_left.php");?>
		<!-- end: PAGESLIDE LEFT -->
		<!-- start: PAGESLIDE RIGHT -->
		<?php include("../../parts/part-pageslide_right.php");?>
		<!-- end: PAGESLIDE RIGHT -->
		<!-- start: MAIN CONTAINER -->
		<div class="main-container inner">
			<!-- start: PAGE -->
			<div class="main-content">
				<div class="container">
					<!-- start: PAGE HEADER -->
					<div class="toolbar row">
						<div class="col-sm-6">
							<div class="page-header">
								<h1>
									Blog News Management 
									<small>Editing your blog news here.</small>
								</h1>
							</div>
						</div>
						<div class="col-sm-6 col-xs-12"></div>
					</div>
					<!-- end: PAGE HEADER -->
					<!-- start: BREADCRUMB -->
					<div class="row">
						<div class="col-md-12">
							<ol class="breadcrumb">
								<li>
									<a href="<?=$path['blog'];?>">
										Blog News Management
									</a>
								</li>
								<li class="active">
									<?=$datas[0]['blog_title'];?>
								</li>
							</ol>
						</div>
					</div>
					<!-- end: BREADCRUMB -->
					<!-- start: PAGE CONTENT -->
					<?php if(is_array($datas)){?>
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-white">
								<div class="panel-heading border-light">
									<h4 class="panel-title">Blog <span class="text-bold">(<?=$datas[0]['blog_title'];?>)</span></h4>
								</div>
								<form name="editBlog" action="edit.php?action=edit" enctype="multipart/form-data" method="post" onsubmit="return validateForm();" >
									<div class="panel-body">
										<div class="form-body">
											<div class="row">
												<div class="col-sm-3 col-xs-12 up1 form-label"><strong>Category <span class="symbol required"></span></strong> :</div>
												<div class="col-sm-6 col-xs-12 up1">
													<select id="input-category" name="category" class="form-control">
														<option value="">Choose Category</option>
														<?php if(is_array($data_categorys)){ foreach($data_categorys as $data){ ?>
														<option <?php if($datas[0]['blog_categoryID'] == $data['blogcat_ID']){echo "selected";}?> value="<?=$data['blogcat_ID'];?>"><?=$data['blogcat_name'];?></option>
														<?php } } ?>
													</select>
													<div id="error-category" class="is-error"></div>
												</div>
											</div>
											<div class="row">
												<div class="col-sm-3 col-xs-12 up1 form-label"><strong>Title <span class="symbol required"></span></strong> :</div>
												<div class="col-sm-6 col-xs-12 up1">
													<input id="input-title" name="title" type="text" class="form-control" placeholder="blog title" value="<?=$datas[0]['blog_title'];?>" />
													<div id="error-title" class="is-error"></div>
												</div>
											</div>
											<div class="row">
												<div class="col-sm-3 col-xs-12 up1 form-label"><strong>Content <span class="symbol required"></span></strong> :</div>
												<div class="col-sm-7 col-xs-12 up1">
													<textarea id="input-desc" name="desc" class="tinymce form-control" rows="5" placeholder="blog description"><?=correctDisplay($datas[0]['blog_desc']);?></textarea>
	                                                <div id="error-desc" class="is-error"></div>											
												</div>
											</div>
											<div class="row">       
												<div class="col-sm-3 col-xs-12 up1 form-label">Image :</div>
												<div class="col-sm-6 col-xs-12 up1">
													<a class="fancybox" href="<?=$global['absolute-url'].$datas[0]['blog_img'];?>">
														<img style="width: 40px;" class="img-circle" src="<?=$global['absolute-url'].$datas[0]['blog_imgThmb'];?>">
													</a>
													<input name="image" type="file" class="text up05" maxlength="256" placeholder="blog image" />
													<div id="error-photo" class="is-error"></div>
													<small><span class="help-block up1"><i class="fa fa-info-circle"></i> <?php echo $infoSize['blog'];?> </span></small>
												</div>
											</div>
											<div class="row">
												<div class="col-sm-3 col-xs-12 up1 form-label">Author :</div>
												<div class="col-sm-6 col-xs-12 up1">
													<input id="input-writer" name="writer" type="text" class="form-control" placeholder="blog author" value="<?=$datas[0]['blog_author'];?>"/>
												</div>
											</div>
											<div class="row">
												<div class="col-sm-3 col-xs-12 up1 form-label">Author link :</div>
												<div class="col-sm-6 col-xs-12 up1">
													<input id="input-writerlink" name="writer_link" type="text" class="form-control" placeholder="blog author link" value="<?=$datas[0]['blog_author_link'];?>"/>
												</div>
											</div>
											<div class="row">
												<div class="col-sm-3 col-xs-12 up1 form-label">Video url :</div>
												<div class="col-sm-6 col-xs-12 up1">
													<input id="input-video" name="video" type="text" class="form-control" placeholder="blog video url" value="<?=$datas[0]['blog_url_video'];?>"/>
												</div>
											</div>
			                                <div class="row">       
			                                    <div class="col-sm-3 col-xs-12 up1 form-label"><strong>Publish <span class="symbol required"></span></strong></div>
			                                    <div class="col-sm-6 col-xs-12 up1">
			                                    	<select id="input-publish" name="publish" class="form-control" >
			                                    		<option <?php if($datas[0]['blog_publish'] == "Publish"){ echo "selected";}?> value="Publish">Publish</option>
			                                    		<option <?php if($datas[0]['blog_publish'] == "Not Publish"){ echo "selected";}?> value="Not Publish">Not Publish</option>
			                                    	</select>
			                                    	<div id="error-publish" class="is-error"></div>
			                                    </div>
			                                </div>
										</div>
									</div>
									<div class="panel-footer">
										<div class="row">
											<input type="hidden" name="id" value="<?=$datas[0]['blog_ID'];?>">
											<div class="col-sm-2 col-xs-12 pad0">
												<div class="link-delete">
													<a href="javascript:void(0)" onclick="confirmDelete('<?=$datas[0]['blog_ID'];?>', '<?=$datas[0]['blog_title'];?>');">
														Delete
													</a>
												</div>
											</div>
											<div class="col-sm-8 col-xs-12 pad0 text-right">
												<div class="btn-group text-right hidden-xs">
													<a href="<?=$path['blog'];?>" type="reset" class="btn btn-default">
														<i class="fa fa-times"></i> Cancel
													</a>
													<button type="submit" class="btn btn-success">
														<i class="fa fa-check fa fa-white"></i> Update
													</button>
												</div>
												<div class="btn-group visible-xs">
													<div class="row">
														<div class="col-xs-12 text-right">
															<a href="<?=$path['blog'];?>" type="reset" class="btn btn-default">
																<i class="fa fa-times"></i> Cancel
															</a>
															<button type="submit" class="btn btn-success">
																<i class="fa fa-check fa fa-white"></i> Update
															</button>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					<?php }else{?>
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-white">
								<div class="panel-heading border-light">
									<h4 class="panel-title">There is no data in this page!</h4>
								</div>
							</div>
						</div>
					</div>
					<?php } ?>
					<!-- end: PAGE CONTENT-->
				</div>
				<div class="subviews">
					<div class="subviews-container"></div>
				</div>
			</div>
			<!-- end: PAGE -->
		</div>
		<!-- end: MAIN CONTAINER -->
		<!-- start: FOOTER -->
		<?php include("../../parts/part-footer.php");?>
		<!-- end: FOOTER -->
		<!-- start: SUBVIEW SAMPLE CONTENTS -->
		<?php include("../../parts/part-sample_content.php");?>
		<!-- end: SUBVIEW SAMPLE CONTENTS -->
	</div>
	<?php include("../../packages/footer-js.php");?>
	<!-- Add fancyBox -->
	<script type="text/javascript" src="<?=$global['absolute-url-admin'];?>packages/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
	<script src="../../packages/tinymce/tinymce.min.js"></script>
  	<script>
    tinymce.init({
        selector: ".tinymce",
        plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste"
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
        relative_urls: false,
        forced_root_block : false,
        height : 200
    });
	</script>
	<script type="text/javascript">
		function validateForm(){
			var category = $("#input-category").val();
            var title = $("#input-title").val();
            var desc = tinyMCE.get('input-desc').getContent();
            
            if(category != ""){
				$("#error-category").html("");
				$("#error-category").hide();
				$("#input-category").removeClass("input-error");
			} else {
				$("#error-category").show();
				$("#error-category").html("<i class='fa fa-warning'></i> This field is required.");
				$("#input-category").addClass("input-error");
				return false;
			}
            if(title != ""){
                $("#error-title").html("");
                $("#error-title").hide();
                $("#input-title").removeClass("input-error");
            } else {
                $("#error-title").show();
                $("#error-title").html("<i class='fa fa-warning'></i> This field is required.");
                $("#input-title").addClass("input-error");
                return false;
            }
            if(desc != ""){
                $("#error-desc").html("");
                $("#error-desc").hide();
            } else {
                $("#error-desc").show();
                $("#error-desc").html("<i class='fa fa-warning'></i> This field is required.");
                return false;
            }
        }

		$(document).ready(function() {
			$(".fancybox").fancybox({
				padding : 0
			});
		});
		//use session here for alert success/failed
		var alertText = "success/error text"; //teks for alert
		//success alert
		//successAlert(alertText); 
		//error alert
		//errorAlert(alertText); 
		//function confirmation delete
		function confirmDelete(num, text){
			swal({
				title: "Are you sure?",
				text: "You will not be able to recover this file!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Delete ! ",
				cancelButtonText: "Cancel !",
				closeOnConfirm: false,
				closeOnCancel: true
			},
			function (isConfirm) {
				if (isConfirm) {
					window.location.href = "index.php?action=delete&id="+num+"&title="+text;
				} else {
	                   //nothing
	               }
	           });
		}
	</script>
</body>
<!-- end: BODY -->
</html>