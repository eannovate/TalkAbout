<?php 
	include("../../packages/require.php");
	include("../../packages/check_login.php");//USED BY ALL PAGE BUT index.php
	include("controller/controller_banner.php");
	$curpage="banner";
?>
<!DOCTYPE html>
<html lang="en">
	<!-- start: HEAD -->
	<head>
		<title><?=$title['banner'];?></title>
		<?php include("../../packages/module-head.php");?>
		<!-- Add fancyBox -->
		<link rel="stylesheet" href="<?=$global['absolute-url-admin'];?>packages/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
	</head>
	<!-- end: HEAD -->
	<!-- start: BODY -->
	<body>
		<!-- start: SLIDING BAR (SB) -->
		<?php include("../../parts/part-sliding_bar.php");?>
		<!-- end: SLIDING BAR -->
		<div class="main-wrapper">
			<!-- start: TOPBAR -->
			<?php include("../../parts/part-top_bar.php");?>
			<!-- end: TOPBAR -->
			<!-- start: PAGESLIDE LEFT -->
			<?php include("../../parts/part-pageslide_left.php");?>
			<!-- end: PAGESLIDE LEFT --> 
			<!-- start: PAGESLIDE RIGHT -->
			<?php include("../../parts/part-pageslide_right.php");?>
			<!-- end: PAGESLIDE RIGHT -->
			<!-- start: MAIN CONTAINER -->
			<div class="main-container inner">
				<!-- start: PAGE -->
				<div class="main-content">
					
					<div class="container">
						<!-- start: PAGE HEADER -->
						<div class="toolbar row">
							<div class="col-sm-6">
								<div class="page-header">
									<h1>
										Banner Management 
										<small>Editing your banner here.</small>
									</h1>
								</div>
							</div>
							<div class="col-sm-6 col-xs-12"></div>
						</div>
						<!-- end: PAGE HEADER -->
						<!-- start: BREADCRUMB -->
						<div class="row">
							<div class="col-md-12">
								<ol class="breadcrumb">
									<li class="active">
										Banner Management
									</li>
								</ol>
							</div>
						</div>
						<!-- end: BREADCRUMB -->
						<!-- start: PAGE CONTENT -->
						<div class="row">
							<div class="col-md-12">
								<div class="panel panel-white">
								
									<div class="panel-body">
										<div style="text-align:right;margin: 10px 0;">
											Total Data : <span class="label label-info"><?=$total_data;?></span>
										</div>
										<div class="table-responsive">
											<table class="table table-striped table-bordered">
											  	<tr>
											  		<th class="text-center">#</th>
											  		<th>Page</th>
											  		<th>Position</th>
											  		<th>Title</th>
											  		<th>Image</th>
											  		<th>Publish</th>
											  		<th>Action</th>
											  	</tr>
											  	<?php if(is_array($datas)) { $num=1;foreach($datas as $data) { ?>
											  	<tr>
											  		<td class="text-center"><?=$num;?>.</td>
											  		<td><?=$data['banner_page'];?></td>
											  		<td><?=$data['banner_position'];?></td>
											  		<td><?=$data['banner_title'];?></td>
											  		<td>
											  			<a class="fancybox" href="<?=$global['absolute-url'].$data['banner_img'];?>">
											  				<img style="width: 40px;" class="img-circle" src="<?=$global['absolute-url'].$data['banner_imgThmb'];?>">
											  			</a>
											  		</td>
											  		<td><?=$data['banner_publish'];?></td>
											  		<td>
											  			<div class="text-center">
															<a href="<?=$path['banner-edit'].$data['banner_ID'];?>" class="btn btn-xs btn-blue tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
														</div>
											  		</td>
											  	</tr>
											  	<?php $num++;} } else { ?>
											  	<tr class="warning">
											  		<td colspan="5" class="bold">There is no data right now!</td>
											  	</tr>
											  	<?php } ?>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- end: PAGE CONTENT-->
					</div>
					<div class="subviews">
						<div class="subviews-container"></div>
					</div>		
				</div>
				<!-- end: PAGE -->
			</div>
			<!-- end: MAIN CONTAINER -->
			<!-- start: FOOTER -->
			<?php include("../../parts/part-footer.php");?>
			<!-- end: FOOTER -->
			<!-- start: SUBVIEW SAMPLE CONTENTS -->
			<?php include("../../parts/part-sample_content.php");?>
			<!-- end: SUBVIEW SAMPLE CONTENTS -->
		</div>
		<?php include("../../packages/footer-js.php");?>
		<!-- Add fancyBox -->
		<script type="text/javascript" src="<?=$global['absolute-url-admin'];?>packages/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
		<script type="text/javascript">
			$(document).ready(function() {
				$(".fancybox").fancybox({
					padding : 0
				});
			});
		</script>
		<script type="text/javascript">
			<?php if($message != "") { ?>
			//use session here for alert success/failed
			var alertText = "<?=$message;?>"; //teks for alert
			
				<?php if($alert != "success"){ ?>
					//error alert
					errorAlert(alertText);
				<?php } else { ?>
					//success alert
					successAlert(alertText); 
				<?php } ?>
			 
			<?php } ?>
		</script>
	</body>
	<!-- end: BODY -->
</html>