<?php 
include("../../packages/require.php");
include("../../packages/check_login.php");//USED BY ALL PAGE BUT index.php
include("../../controller/controller_content_email.php");
$curpage="content";
?>
<!DOCTYPE html>
<html lang="en">
<!-- start: HEAD -->
<head>
	<title><?=$title['content'];?></title>
	<?php include("../../packages/module-head.php");?>
</head>
<!-- end: HEAD -->
<!-- start: BODY -->
<body>
	<!-- start: SLIDING BAR (SB) -->
	<?php include("../../parts/part-sliding_bar.php");?>
	<!-- end: SLIDING BAR -->
	<div class="main-wrapper">
		<!-- start: TOPBAR -->
		<?php include("../../parts/part-top_bar.php");?>
		<!-- end: TOPBAR -->
		<!-- start: PAGESLIDE LEFT -->
		<?php include("../../parts/part-pageslide_left.php");?> 
		<!-- end: PAGESLIDE LEFT -->
		<!-- start: PAGESLIDE RIGHT -->
		<?php include("../../parts/part-pageslide_right.php");?>
		<!-- end: PAGESLIDE RIGHT -->
		<!-- start: MAIN CONTAINER -->
		<div class="main-container inner">
			<!-- start: PAGE -->
			<div class="main-content">
				<div class="container">
					<!-- start: PAGE HEADER -->
					<div class="toolbar row">
						<div class="col-sm-6">
							<div class="page-header">
								<h1>
									Content Management 
									<small>Manage / editing your content website here</small>
								</h1>
							</div>
						</div>
						<div class="col-sm-6 col-xs-12"></div>
					</div>
					<!-- end: PAGE HEADER -->
					<!-- start: BREADCRUMB -->
					<div class="row">
						<div class="col-md-12">
							<ol class="breadcrumb">
								<li class="active">
									Content Management
								</li>
							</ol>
						</div>
					</div>
					<!-- end: BREADCRUMB -->
					<!-- start: PAGE CONTENT -->
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-white">
								<div class="panel-heading border-light">
									<h4 class="panel-title">E-Mail Content</span></h4>
								</div>
								<form name="updateContent" action="email.php?action=update" enctype="multipart/form-data" method="post" >
									<div class="panel-body">
										<div class="form-body">
											<div class="row">
												<div class="col-sm-offset-1 col-sm-10 col-xs-12 pad0">
													<div class="row">
														<div class="col-xs-12 up1">
															<div class="form-label bold text-left">Register Info</div>
															<small>
																<span class="help-block up05">
																	<i class="fa fa-info-circle"></i> (Received by Customer upon registration in the website, this short message in e-mail will appear below Dear "name")
																</span>
															</small>
														</div>
													</div>
													<div class="row">
														<div class="col-xs-12 up1">
															<textarea name="register" class="form-control" rows="6"><?=$J_register;?></textarea>
														</div>
													</div>
													<div class="row">
														<div class="col-xs-12 up15">
															<div class="form-label bold text-left">Order Info</div>
															<small>
																<span class="help-block up05">
																	<i class="fa fa-info-circle"></i> (Received by Admin and Customer when Customer click "COMPLETE MY ORDER" in the Checkout page, this short message in e-mail will appear below payment info)
																</span>
															</small>
														</div>
													</div>
													<div class="row">
														<div class="col-xs-12 up1">
															<textarea name="order" class="form-control" rows="6"><?=$J_order;?></textarea>
														</div>
													</div>
													<div class="row">
														<div class="col-xs-12 up15">
															<div class="form-label bold text-left">Shipment Complete Info</div>
															<small>
																<span class="help-block up05">
																	<i class="fa fa-info-circle"></i> (Received by Customer when Admin set order status to "COMPLETE" , this short message in e-mail will appear in the footer)
																</span>
															</small>
														</div>
													</div>
													<div class="row">
														<div class="col-xs-12 up1">
															<textarea name="shipment" class="form-control" rows="6"><?=$J_shipment;?></textarea>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="panel-footer">
										<div class="row">
											<div class="col-sm-offset-1 col-sm-10 col-xs-12 text-right">
												<div class="btn-group">
													<a href="<?=$path['content-email'];?>" type="reset" class="btn btn-default">
														<i class="fa fa-times"></i> Cancel
													</a>
													<button type="submit" class="btn btn-success">
														<i class="fa fa-check fa fa-white"></i> Update
													</button>
												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					<!-- end: PAGE CONTENT-->
				</div>
				<div class="subviews">
					<div class="subviews-container"></div>
				</div>
			</div>
			<!-- end: PAGE -->
		</div>
		<!-- end: MAIN CONTAINER -->
		<!-- start: FOOTER -->
		<?php include("../../parts/part-footer.php");?>
		<!-- end: FOOTER -->
		<!-- start: SUBVIEW SAMPLE CONTENTS -->
		<?php include("../../parts/part-sample_content.php");?>
		<!-- end: SUBVIEW SAMPLE CONTENTS -->
	</div>
	<?php include("../../packages/footer-js.php");?>
	<script type="text/javascript">
		<?php if($message != "") { ?>
			//use session here for alert success/failed
			var alertText = "<?=$message;?>"; //teks for alert
			
				<?php if($alert != "success"){ ?>
					//error alert
					errorAlert(alertText);
				<?php } else { ?>
					//success alert
					successAlert(alertText); 
				<?php } ?>
			 
			<?php } ?>
		</script>
	</body>
	<!-- end: BODY -->
	</html>