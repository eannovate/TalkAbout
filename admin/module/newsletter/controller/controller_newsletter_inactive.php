<?php 
require_once($global['root-url']."model/Connection.php");
$obj_con = new Connection();
require_once($global['root-url']."model/Newsletter.php");
$obj_newsletter = new Newsletter(); 
if(!isset($_GET['action'])){
    $obj_con->up();
    
    $O_page = 1;
    if(isset($_GET['page'])){
        $O_page = mysql_real_escape_string(check_input($_GET['page']));
    }
    $O_stat = 0;
    $datas = $obj_newsletter->get_data_by_page($O_page,$O_stat);
    if(is_array($datas)){
        $total_data = $datas[0]['total_data_all'];
        $total_page = $datas[0]['total_page'];
    }else{
        $total_data = 0;
        $total_page = 0;
    }
    if(isset($_SESSION['status'])){
        $message = $_SESSION['status'];
        unset($_SESSION['status']);
    } else {
        $message = "";
    }
    if(isset($_SESSION['alert'])){
        $alert = $_SESSION['alert'];
        unset($_SESSION['alert']);
    } else {
        $alert = "";
    }
    
    $obj_con->down();
} else if(isset($_GET['action'])){
    if($_GET['action'] == "update"){
        $obj_con->up();
        $O_id = mysql_real_escape_string(check_input($_GET['id']));
        if($_GET["status"]=='0'){
            $stat = '1';
        }
        else {
            $stat = '0';
        }
        $result = $obj_newsletter->set_status($O_id,$stat);
        if($result <= 0){
            $message = "Something is incorrect with the Subscriber's changes.<br />";
            $_SESSION['alert'] = "error";
        }else if($result == 1){
            if($stat == 0){
                $message = "The Email for Newsletter has been set as inactive.<br />";
            }
            else{
                $message = "The Email for Newsletter has been set as active.<br />";
            }
            $_SESSION['alert'] = "success";
        }
        $_SESSION['status'] = $message;
        header("Location:inactive.php");
        $obj_con->down();
    }
}
?>