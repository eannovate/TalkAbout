<?php 
require_once($global['root-url']."model/Connection.php");
$obj_con = new Connection();

require_once($global['root-url']."model/Newsletter.php");
$obj_newsletter = new Newsletter(); 

if(!isset($_GET['action'])){
    $obj_con->up();

    $data_inactives = $obj_newsletter->get_data_inactive();
    if(is_array($data_inactives)){
        $total_inactive = $data_inactives[0]['total_data_all'];
    }else{
        $total_inactive = 0;
    }

    $data_actives = $obj_newsletter->get_data_active();
    if(is_array($data_actives)){
        $total_active = $data_actives[0]['total_data_all'];
    }else{
        $total_active = 0;
    }

    $total_data = $total_inactive + $total_active;

    if(isset($_SESSION['status'])){
        $message = $_SESSION['status'];
        unset($_SESSION['status']);
    } else {
        $message = "";
    }

    if(isset($_SESSION['alert'])){
        $alert = $_SESSION['alert'];
        unset($_SESSION['alert']);
    } else {
        $alert = "";
    }

    $obj_con->down();

} else if(isset($_GET['action'])){
    if($_GET['action'] == "update"){
        $obj_con->up();
        $O_id = mysql_real_escape_string(check_input($_GET['id']));

        if($_GET["status"]=='0'){
            $stat = '1';
        } else {
            $stat = '0';
        }

        $result = $obj_newsletter->set_status($O_id,$stat);
        if($result <= 0){
            $message = "Something is incorrect with the Subscriber's changes.<br />";
            $_SESSION['alert'] = "error";
        }else if($result == 1){
            if($stat == 0){
                $message = "The Email for Newsletter has been set as inactive.<br />";
            }else{
                $message = "The Email for Newsletter has been set as active.<br />";
            }
            $_SESSION['alert'] = "success";
        }

        $_SESSION['status'] = $message;
        header("Location:index.php");
        $obj_con->down();
    }
}
?>