<header class="topbar navbar navbar-inverse navbar-fixed-top inner">
	<!-- start: TOPBAR CONTAINER -->
	<div class="container">
		<div class="navbar-header">
			<a class="sb-toggle-left hidden-md hidden-lg" href="#main-navbar">
				<i class="fa fa-bars"></i>
			</a>
			<!-- start: LOGO -->
			<a class="navbar-brand" href="index.html">
				<img src="<?=$global['absolute-url-admin'];?>assets/images/logo.png" alt="Eannovate"/>
			</a>
			<!-- end: LOGO -->
		</div>
		<div class="topbar-tools">
			<!-- start: TOP NAVIGATION MENU -->
			<ul class="nav navbar-right">
				<!-- start: USER DROPDOWN -->
				<li class="dropdown current-user">
					<a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" data-close-others="true" href="#">
						<img src="<?=$global['absolute-url-admin'];?>assets/images/avatar-1-small.jpg" class="img-circle hide" alt=""> <span class="username hidden-xs"><?=$_SESSION['admin_username'];?></span> <i class="fa fa-caret-down "></i>
					</a>
					<ul class="dropdown-menu dropdown-dark">
						<li>
							<a href="<?=$path['user-edit'].$_SESSION['admin_id'];?>">
								My Profile
							</a>
						</li>				
						
						
						<li>
							<a href="<?=$path['logout'];?>">
								Log Out
							</a>
						</li>
					</ul>
				</li>
				<!-- end: USER DROPDOWN -->
				
			</ul>
			<!-- end: TOP NAVIGATION MENU -->
		</div>
	</div>
	<!-- end: TOPBAR CONTAINER -->
</header>