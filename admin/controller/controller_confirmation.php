<?php
require_once("../../../model/Connection.php");
$obj_con = new Connection();

require_once("../../../model/PO.php");
$obj_po = new PO();

require_once("../../../model/Confirm.php");
$obj_confirm = new Confirm();

if(!isset($_GET['action'])){
    $obj_con->up();
    $O_page = 1;
    if(isset($_GET['page'])){
        $O_page = mysql_real_escape_string(check_input($_GET['page']));
    }
    $datas = $obj_confirm->get_data_by_page($O_page);
    //var_dump($datas);
    if(is_array($datas)){
        $total_data = $datas[0]['total_data_all'];
        $total_page = $datas[0]['total_page'];
    }else{
        $total_data = 0;
        $total_page = 0;
    }
    if(isset($_SESSION['status'])){
        $message = $_SESSION['status'];
        unset($_SESSION['status']);
    } else {
        $message = "";
    }
    if(isset($_SESSION['alert'])){
        $alert = $_SESSION['alert'];
        unset($_SESSION['alert']);
    } else {
        $alert = "";
    }
    
    $obj_con->down();

} else if(isset($_GET['action'])){

    if($_GET['action'] == "update"){
        $obj_con->up();
        $N_confID = mysql_real_escape_string(check_input($_POST['conf_ID']));
        $N_confPoID = mysql_real_escape_string(check_input($_POST['conf_poID']));
        $N_confDate = mysql_real_escape_string(check_input($_POST['conf_date']));
        $N_confName = mysql_real_escape_string(check_input($_POST['conf_name']));
        $N_confPoName = mysql_real_escape_string(check_input($_POST['conf_poName2']));
        $N_confCustName = mysql_real_escape_string(check_input($_POST['conf_custName2']));
        $N_confAmountDue = mysql_real_escape_string(check_input($_POST['conf_amountDue']));
        $N_confAmountPaid = mysql_real_escape_string(check_input($_POST['conf_amountPaid']));
        $N_confBank = mysql_real_escape_string(check_input($_POST['conf_bank']));
        $N_confNoRek = mysql_real_escape_string(check_input($_POST['conf_noRek']));
        $N_confRekName = mysql_real_escape_string(check_input($_POST['conf_rekName']));
        $N_confNote = mysql_real_escape_string(check_input($_POST['conf_note']));
        $N_confStat = mysql_real_escape_string(check_input($_POST['conf_stat']));
        $N_confEmail = mysql_real_escape_string(check_input($_POST['cust_Email']));
        $N_rejected = mysql_real_escape_string(check_input($_POST['conf_reason']));

        $O_bodyCustConf= " 
        Dear ".$N_confCustName.",<br/>
        We would like to inform you that your payment for order <b>".$N_confPoName."</b> has been confirmed.<br/>
        <b>Your order is being process.</b><br><br>  

        Kind Regards,<br><br>
        Cheveux Cosmetics<br><br><br>  
        *Do not reply to this e-mail.<br>
        Thank you!
        ";

        $O_bodyAdminConf= "
        Dear Cheveux administrator,<br/>
        We would like to inform you that payment for order <b>".$N_confPoName."</b> has been confirmed.<br/>
        <b>Please process the order immediately.</b><br><br>  

        Kind Regards,<br><br>
        Cheveux Cosmetics<br><br><br>  
        *Do not reply to this e-mail.<br>
        Thank you!
        ";

        $O_subjectCustConf = "Your payment for order ".$N_confPoName." has been confirmed";
        $O_subjectAdminConf = "New payment for order ".$N_confPoName." has been confirmed";    


        $O_bodyCustReject= " 
        Dear ".$N_confCustName.",<br/>
        We would like to inform you that your payment for order <b>".$N_confPoName."</b> has been rejected.<br/><br/>
        <b>Here is the reason:</b><br>
        ".$N_rejected."<br><br> 


        Kind Regards,<br><br>
        Cheveux Cosmetics<br><br><br>  
        *Do not reply to this e-mail.<br>
        Thank you!
        ";

        $O_bodyAdminReject= "
        Dear Cheveux administrator,<br/>
        We would like to inform you that payment for order <b>".$N_confPoName."</b> has been rejected.<br/><br/>
        <b>Here is the reason:</b><br>
        ".$N_rejected."<br><br>  

        Kind Regards,<br><br>
        Cheveux Cosmetics<br><br><br>  
        *Do not reply to this e-mail.<br>
        Thank you!
        ";

        $O_subjectCustReject = "Your payment for order ".$N_confPoName." has been rejected";
        $O_subjectAdminReject = "New payment for order ".$N_confPoName." has been rejected";  

        $toCust = $N_confEmail;

        $file_json = "../../../uploads/json/company_profile.json";
        $json = json_decode(file_get_contents($file_json),TRUE);
        $J_Company_email = $json['email1'];
        $O_toCompany = $J_Company_email;

        if( $N_confStat == 2 ){
            $statusPaid = 1;
            $statusText = "CONFIRMED";
            $Npo_Status = "ON PROGRESS";
            $sentCust = smtpmailer($toCust, $smtp['url'], $smtp['from'], $smtp['password'], $seo['company-name'], $O_subjectCustConf, $O_bodyCustConf);
            $sentAdmin = smtpmailer($O_toCompany, $smtp['url'], $smtp['from'], $smtp['password'], $seo['company-name'], $O_subjectAdminConf, $O_bodyAdminConf);
        } else if ( $N_confStat == 1 ){
            $statusPaid = 0;
            $statusText = "REJECTED";
            $Npo_Status = "VERIFY PAYMENT";
            $sentCust = smtpmailer($toCust, $smtp['url'], $smtp['from'], $smtp['password'], $seo['company-name'], $O_subjectCustReject, $O_bodyCustReject);
            $sentAdmin = smtpmailer($O_toCompany,  $smtp['url'], $smtp['from'], $smtp['password'], $seo['company-name'], $O_subjectAdminReject, $O_bodyAdminReject);
        } else{
            $statusPaid = 0;
            $statusText = "PENDING";
            $Npo_Status = "VERIFY PAYMENT";
        }

        $result = $obj_confirm->update_data($N_confID, $N_confDate, $N_confAmountPaid, $N_confBank, $N_confNoRek, $N_confRekName, $N_confNote, $N_confStat);
        if($result <= 0){
            $message = "There is no change to confirm.<br />";
            $_SESSION['alert'] = "error";
        }else if($result == 1){
            $message = "<div>Confirmation customer <i>$N_confCustName</i> with PO name $N_confPoName has been updated.</div><br />";
            $result = $obj_po->update_paid($N_confPoID, $statusPaid);
            if($result == 1){               
                $message .= "<div>The order status has been set as <i>$Npo_Status</i>.</div><br /><div>The payment status has been set as <i>$statusText</i>.</div><br />";
            }
            $_SESSION['alert'] = "success";
        }else{
            $_SESSION['alert'] = "error";
            die();
        }       
      
        $_SESSION['status'] = $message;               
        $obj_con->down();
        header("Location:index.php"); 
    }
}
?> 