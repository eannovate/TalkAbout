<?php
require_once("../../../model/Connection.php");
$obj_con = new Connection();

require_once("../../../model/PO.php");
$obj_po = new PO(); 

if(!isset($_GET['action'])){
    $obj_con->up();
    $O_page = 1;
    if(isset($_GET['page'])){
        $O_page = mysql_real_escape_string(check_input($_GET['page']));
    }

    $datas = $obj_po->get_order_history($O_page);
    
    //var_dump($datas);
    if(is_array($datas)){
        $total_data = $datas[0]['total_data_all'];
        $total_page = $datas[0]['total_page'];
    }else{
        $total_data = 0;
        $total_page = 0;
    }
    if(isset($_SESSION['status'])){
        $message = $_SESSION['status'];
        unset($_SESSION['status']);
    } else {
        $message = "";
    }
    if(isset($_SESSION['alert'])){
        $alert = $_SESSION['alert'];
        unset($_SESSION['alert']);
    } else {
        $alert = "";
    }

    $obj_con->down();

} else if(isset($_GET['action'])){

    if($_GET['action'] == "resi"){
        $obj_con->up();

        $headerURL = "index.php";
        $N_orderPoID = mysql_real_escape_string(check_input($_POST['PO_ID']));
        $N_orderPoResi = mysql_real_escape_string(check_input($_POST['PO_resi']));
        $N_orderPoName = mysql_real_escape_string(check_input($_POST['PO_name']));
        $O_toCust = mysql_real_escape_string(check_input($_POST['Cust_email']));
        $N_custName = mysql_real_escape_string(check_input($_POST['Cust_name']));

        $file_json = "../../../uploads/json/company_profile.json";
        $json = json_decode(file_get_contents($file_json),TRUE);
        $J_Company_email = $json['email1'];
        $O_toCompany = $J_Company_email;

        $O_subjectCust = "Volia order ".$N_orderPoName." Shipment Confirmation";
        $O_messageCust = "
        Dear ".$N_custName.",<br/>
        We would like to inform you that your order <b>".$N_orderPoName."</b> has been shipped via jne.<br/>
        The tracking number is <b>".$N_orderPoResi."</b>.<br/>

        Kind Regards,<br><br>
        Volia<br><br><br>  
        *Do not reply to this e-mail.<br>
        Thank you!
        ";

        $O_subjectAdmin = "Volia order ".$N_orderPoName." Shipment Confirmation";
        $O_messageAdmin = "
        Dear Administrator,<br/>
        We would like to inform you that the order <b>".$N_orderPoName."</b> has been shipped via jne.<br/>
        The tracking number is <b>".$N_orderPoResi."</b>.<br/>

        Kind Regards,<br><br>
        Volia<br><br><br>  
        *Do not reply to this e-mail.<br>
        Thank you!
        ";

        if($N_orderPoResi == '0'){
            $headerURL = "index.php";
        }
        else if($N_orderPoResi == '1'){
            $N_orderPoResi = 'PICK UP';
        }
        else{
            $N_orderPoResi = strtoupper($N_orderPoResi);
        }

        $result = $obj_po->update_resi($N_orderPoID, $N_orderPoResi, 4);
        if($result <= 0){
            $message = "Something is wrong with your submission.<br />";
            $_SESSION['alert'] = "error";
        }else if($result == 1){
            $sentCust = smtpmailer($O_toCust, $smtp['url'], $smtp['from'], $smtp['password'], $seo['company-name'], $O_subjectCust, $O_messageCust);
            $sentAdmin = smtpmailer($O_toCompany, $smtp['url'], $smtp['from'], $smtp['password'], $seo['company-name'], $O_subjectAdmin, $O_messageAdmin);
            $message = "The resi order has been updated.<br />";
            $_SESSION['alert'] = "success";
        }else{
            $_SESSION['alert'] = "error";
            die();
        }
        $_SESSION['status'] = $message;
        header("Location: $headerURL");
        $obj_con->down();
    }
}
?>