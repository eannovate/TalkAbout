<?php 

require_once("../../../model/Connection.php");

$obj_con = new Connection();



require_once("../../../model/PO.php");

$obj_po = new PO(); 



if(!isset($_GET['action']) && $_GET['id'] != ""){

    $obj_con->up();

    

    $O_id = mysql_real_escape_string(check_input($_GET['id']));

    $datas = $obj_po->get_order_detail($O_id);



    //var_dump($datas);

    if(isset($_SESSION['status'])){

        $message = $_SESSION['status'];

        unset($_SESSION['status']);

    } else {

        $message = "";

    }



    if(isset($_SESSION['alert'])){

        $alert = $_SESSION['alert'];

        unset($_SESSION['alert']);

    } else {

        $alert = "";

    }



    $obj_con->down();



} else if(isset($_GET['action'])){





    $file_json = "../../../uploads/json/content_email.json";

    $json = json_decode(file_get_contents($file_json),TRUE);

    

    $J_register = $json['register'];

    $J_order = $json['order'];

    $J_shipment = $json['shipment'];



	if($_GET['action'] == "resi"){

        function send_email($custEmail, $custName, $orderPoResi, $orderPoName, $textShipment){

        //mail

        $to = $custEmail; 

        $subject = "Shipment Complete for order $orderPoName";



        $message = "

        <!DOCTYPE html>

        <html>

        <head>

        <title></title>

        <link rel='stylesheet' href='//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css'>

        <script src='//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js'></script>

        </head>

        <body>

        <table align='center' border='0' cellpadding='0' cellspacing='0' width='600' style='border: solid 2px #f2786b;'>

            <tr>

                <td style='padding: 40px 30px 40px 30px;'>

                    <table border='0' cellpadding='0' cellspacing='0' width='100%'>

                        <tr style='padding: 20px 0 20px 0;'>

                            <td>

                                Dear $custName,<br/>

                                We would like to inform you that your order <b>$orderPoName</b> has been shipped.<br/>

                                The tracking number is <b>$orderPoResi</b>.

                            </td>

                        </tr>

                        <tr>

                            <td style='padding: 20px 0 30px 0;'>

                                $textShipment

                            </td>

                        </tr>

                        <tr>

                            <td style='padding: 20px 0 30px 0;'>

                                Please do not hesitate to contact us if you require any assistance.<br/>

                                Thank you,<br/><br/>

                                Odetta.<br/><br/><br/>

                                *this e-mail completes your order

                            </td>

                        </tr>

                    </table>

                </td>

            </tr>

        <tr>

            <td>

                <table style='width:100%;'>

                    <tr style='background-color:#f2786b;'>

                        <td style='padding: 10px 30px 10px 30px;' >

                            <h3 style='color:#fff;'>

                            Thank you <br/>

                            Do not forget to Subscribe to our newsletter to get the latest info.

                            </h3>

                        </td>

                    </tr>

                </table>

            </td>

        </tr>

        </table>

        </body>

        </html>

        ";





        // Always set content-type when sending HTML email

        $headers = "MIME-Version: 1.0" . "\r\n";

        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";



        // More headers

        $headers .= "From: <donotreply@odettastyle.com>" . "\r\n";



        $result = mail($to,$subject,$message,$headers);

        return $result; 

        }



        $obj_con->up();

        $N_orderPoID = mysql_real_escape_string(check_input($_POST['PO_ID']));

        $N_orderPoResi = mysql_real_escape_string(check_input($_POST['PO_resi']));

        $N_orderPoName = mysql_real_escape_string(check_input($_POST['PO_name']));

        $N_custEmail = mysql_real_escape_string(check_input($_POST['Cust_email']));

        $N_custName = mysql_real_escape_string(check_input($_POST['Cust_name']));

        $headerURL = "edit.php?id=$N_orderPoID";



        if($N_orderPoResi == '0'){

            $headerURL = "index.php?action=cancel&po_ID=$N_orderPoID";

        }

        else if($N_orderPoResi == '1'){

            $N_orderPoResi = 'PICK UP';

        }

        else{

            $N_orderPoResi = strtoupper($N_orderPoResi);

        }



        $result = $obj_po->update_resi($N_orderPoID, $N_orderPoResi,5);

        if($result <= 0){

            $message = "Something is wrong with your submission.<br />";

            $_SESSION['alert'] = "error";

        }else if($result == 1){

            $result = send_email($N_custEmail, $N_custName, $N_orderPoResi, $N_orderPoName, $J_shipment);

            $message = "The resi order has been updated.<br />";

            $_SESSION['alert'] = "success";

        }else{

            $_SESSION['alert'] = "error";

            die();

        }

        $_SESSION['status'] = $message;

        header("Location: $headerURL");

        $obj_con->down();

    }

    else if($_GET['action'] == "addNote"){

        $obj_con->up();

        $N_poID = mysql_real_escape_string(check_input($_POST['PO_ID']));

        $N_poNote = mysql_real_escape_string(check_input($_POST['PO_note']));

        $headerURL = "edit.php?id=$N_poID";



        $result = $obj_po->update_note($N_poID, $N_poNote);

        if($result <= 0){

            $message = "There has been error when saving note.<br />";

            $_SESSION['alert'] = "error";

        }else if($result == 1){

            $message = "Note has been successfully created.<br />";

            $_SESSION['alert'] = "success";

        }else{

            $_SESSION['alert'] = "error";

            die();

        }

        $_SESSION['status'] = $message;

        header("Location: $headerURL");

        $obj_con->down();

    }

    else if($_GET['action'] == "cancel"){

        $obj_con->up();

        $N_poID = mysql_real_escape_string(check_input($_GET['po_id']));

        $headerURL = "edit.php?id=$N_poID";



        $result = $obj_po->cancel_order($N_poID);

        if($result <= 0){

            $message = "There has been error when updating the order.<br />";

            $_SESSION['alert'] = "error";

        }else if($result == 1){

            $message = "The order has been canceled succesfully.<br />";

            $_SESSION['alert'] = "success";

        }else{

            $_SESSION['alert'] = "error";

            die();

        }

        $_SESSION['status'] = $message;

        header("Location: $headerURL");

        $obj_con->down();

    }

    

    else if($_GET['action'] == "paid"){

        $obj_con->up();

        $N_poID = mysql_real_escape_string(check_input($_GET['po_id']));

        $N_poPaid = mysql_real_escape_string(check_input($_GET['po_isPaid']));

        $headerURL = "edit.php?id=$N_poID";



        if($N_poPaid == 1){

	    	$N_paidStatus = 0;

	    	$saveText = "NOT PAID";            

	    }else if($N_poPaid == 0){

	    	$N_paidStatus = 1;

	    	$saveText = "PAID";            

	    }



        $result = $obj_po->update_paid($N_poID, $N_paidStatus);

        if($result <= 0){

            $message = "There has been error when updating the order.<br />";

            $_SESSION['alert'] = "error";

        }else if($result == 1){

            $message = "The order has been set as $saveText.<br />";

            $_SESSION['alert'] = "success";

        }else{

            $_SESSION['alert'] = "error";

            die();

        }

        $_SESSION['status'] = $message;

        header("Location: $headerURL");

        $obj_con->down();

    }

}





function convert_status($status){

    $result = "";

    if($status==6){

        $result='<span class="label label-danger">CANCELED</span>' ;

    }elseif($status == 5){

        $result='<span class="label label-success">COMPLETED</span>' ;

    }elseif($status == 4){

        $result='<span class="label label-success">SHIPPED</span>' ;                                               

    }elseif($status == 3) {

        $result='<span class="label label-info">ON PROGRESS</span>' ;             

    }elseif($status == 2) {

        $result='<span class="label label-warning">VERIFY PAYMENT</span>' ;                            

    }else {

        $result='<span class="label label-default">AWAITING PAYMENT</span>' ;

    } 

    return $result ;                                       

}

?> 