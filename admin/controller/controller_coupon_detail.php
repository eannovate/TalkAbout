<?php 
require_once("../../../model/Connection.php");
$obj_con = new Connection();
require_once("../../../model/Coupon.php");
$obj_coupon = new Coupon(); 

if(!isset($_GET['action']) && $_GET['id'] != ""){
    $obj_con->up();    

    $O_id = mysql_real_escape_string(check_input($_GET['id']));
    $datas = $obj_coupon->get_data_detail($O_id);
    $obj_con->down();

} else if(isset($_GET['action'])){
    if($_GET['action'] == "edit"){
        $obj_con->up();
        $N_id = mysql_real_escape_string(check_input($_POST['id']));
        $N_name = mysql_real_escape_string(check_input($_POST['name']));       
        $N_price = mysql_real_escape_string(check_input($_POST['price']));
        $N_publish = mysql_real_escape_string(check_input($_POST['publish']));

        $result = $obj_coupon->update_data($N_id, $N_name, $N_price, $N_publish);

        if($result <= 0){
            $message = "Something is wrong with your submission.<br />";
            $_SESSION['alert'] = "error";
        }else if($result == 1){
            $message = "Coupon<i><b>'" . $N_name . "'</b></i> has been succesfully edited.<br />";
            $_SESSION['alert'] = "success";
        }else{
            $_SESSION['alert'] = "error";
            die();
        }   

        $_SESSION['status'] = $message;
        header("Location:index.php");
        $obj_con->down();
    }
}
?>