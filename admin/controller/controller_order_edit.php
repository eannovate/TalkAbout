<?php
require_once("../../../model/Connection.php");
$obj_con = new Connection();

require_once("../../../model/PO.php");
$obj_po = new PO();

require_once("../../../model/Customer.php");
$obj_customer = new Customer(); 

if(!isset($_GET['action']) && $_GET['id'] != ""){
    $obj_con->up();
    
    $O_id = mysql_real_escape_string(check_input($_GET['id']));
    $datas = $obj_po->get_order_detail($O_id);
    //var_dump($datas);
    if(isset($_SESSION['status'])){
        $message = $_SESSION['status'];
        unset($_SESSION['status']);
    } else {
        $message = "";
    }
    if(isset($_SESSION['alert'])){
        $alert = $_SESSION['alert'];
        unset($_SESSION['alert']);
    } else {
        $alert = "";
    }

    $obj_con->down();

} else if(isset($_GET['action'])){

    $file_json = "../../../uploads/json/content_email.json";
    $json = json_decode(file_get_contents($file_json),TRUE);
    
    $J_register = $json['register'];
    $J_order = $json['order'];
    $J_shipment = $json['shipment'];

	if($_GET['action'] == "resi"){
        $obj_con->up();

        $N_orderPoID = mysql_real_escape_string(check_input($_POST['PO_ID']));
        $N_orderPoResi = mysql_real_escape_string(check_input($_POST['PO_resi']));
        $N_orderPoName = mysql_real_escape_string(check_input($_POST['PO_name']));
        $O_to = mysql_real_escape_string(check_input($_POST['Cust_email']));
        $N_custName = mysql_real_escape_string(check_input($_POST['Cust_name']));

        $O_subject = "Shipment Complete Volia";
        $O_message = "
        Dear ".$N_custName.",<br/>
        We would like to inform you that your order <b>".$N_orderPoName."</b> has been shipped to address.<br/>
        The tracking number is <b>".$N_orderPoResi."</b>.<br/>
        *this e-mail completes your order.<br/><br/><br/>

        Kind Regards,<br><br>
        Volia<br><br><br>  
        *Do not reply to this e-mail.<br>
        Thank you!
        ";

        $headerURL = "edit.php?id=$N_orderPoID";
        if($N_orderPoResi == '0'){
            $headerURL = "index.php?action=cancel&po_ID=$N_orderPoID";
        }
        else if($N_orderPoResi == '1'){
            $N_orderPoResi = 'PICK UP';
        }
        else{
            $N_orderPoResi = strtoupper($N_orderPoResi);
        }

        $result = $obj_po->update_resi($N_orderPoID, $N_orderPoResi,5);
        if($result <= 0){
            $message = "Something is wrong with your submission.<br />";
            $_SESSION['alert'] = "error";
        }else if($result == 1){
            $sent = smtpmailer($O_to, $smtp['url'], $smtp['from'], $smtp['password'], $seo['company-name'], $O_subject, $O_message);
            $message = "The resi order has been updated.<br />";
            $_SESSION['alert'] = "success";
        }else{
            $_SESSION['alert'] = "error";
            die();
        }

        $_SESSION['status'] = $message;
        header("Location: $headerURL");
        $obj_con->down();
    }
    else if($_GET['action'] == "addNote"){
        $obj_con->up();

        $N_poID = mysql_real_escape_string(check_input($_POST['PO_ID']));
        $N_poNote = mysql_real_escape_string(check_input($_POST['PO_note']));
        $headerURL = "edit.php?id=$N_poID";
        $result = $obj_po->update_note($N_poID, $N_poNote);
        if($result <= 0){
            $message = "There has been error when saving note.<br />";
            $_SESSION['alert'] = "error";
        }else if($result == 1){
            $message = "Note has been successfully created.<br />";
            $_SESSION['alert'] = "success";
        }else{
            $_SESSION['alert'] = "error";
            die();
        }

        $_SESSION['status'] = $message;
        header("Location: $headerURL");
        $obj_con->down();
    }
    else if($_GET['action'] == "cancel"){
        $obj_con->up();

        $N_poID = mysql_real_escape_string(check_input($_GET['po_id']));
        $headerURL = "edit.php?id=$N_poID";
        $result = $obj_po->cancel_order($N_poID);
        if($result <= 0){
            $message = "There has been error when updating the order.<br />";
            $_SESSION['alert'] = "error";
        }else if($result == 1){
            $message = "The order has been canceled succesfully.<br />";
            $_SESSION['alert'] = "success";
        }else{
            $_SESSION['alert'] = "error";
            die();
        }

        $_SESSION['status'] = $message;
        header("Location: $headerURL");
        $obj_con->down();
    }
    
    else if($_GET['action'] == "paid"){
        $obj_con->up();

        $N_poID = mysql_real_escape_string(check_input($_GET['po_id']));
        $N_poPaid = mysql_real_escape_string(check_input($_GET['po_isPaid']));
        $N_custID = mysql_real_escape_string(check_input($_GET['customer_id']));
        $N_poName = mysql_real_escape_string(check_input($_GET['po_name']));

        $data_cust = $obj_customer->get_data_detail($N_custID);
        $cust_fname = $data_cust[0]['customer_fname'];
        $O_to = $data_cust[0]['customer_email'];

        $O_subject = "Payment Confirmation";
        $O_message = "
        Dear ".$cust_fname.",<br>
        We would like to inform you that your Payment <b>".$N_poName."</b> has been Confirm.<br/><br/><br/>

        Kind Regards,<br><br>
        Volia<br><br><br>  
        *Do not reply to this e-mail.<br>
        Thank you!
        ";

        $headerURL = "edit.php?id=$N_poID";
        if($N_poPaid == 1){
	    	$N_paidStatus = 0;
	    	$saveText = "NOT PAID";            
	    }else if($N_poPaid == 0){
	    	$N_paidStatus = 1;
            $sent = smtpmailer($O_to, $smtp['url'], $smtp['from'], $smtp['password'], $seo['company-name'], $O_subject, $O_message);
	    	$saveText = "PAID";            
	    }

        $result = $obj_po->update_paid($N_poID, $N_paidStatus);
        if($result <= 0){
            $message = "There has been error when updating the order.<br />";
            $_SESSION['alert'] = "error";
        }else if($result == 1){
            $message = "The order has been set as $saveText.<br />";
            $_SESSION['alert'] = "success";
        }else{
            $_SESSION['alert'] = "error";
            die();
        }

        $_SESSION['status'] = $message;
        header("Location: $headerURL");
        $obj_con->down();
    }
}
?> 