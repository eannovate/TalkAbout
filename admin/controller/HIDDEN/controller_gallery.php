<?php 
require_once("../../../model/Connection.php");
$obj_con = new Connection();

require_once("../../../model/Portfolio.php");
$obj_port = new Portfolio(); 

if(!isset($_GET['action'])){
    $obj_con->up();
    
    $O_page = 1;
    if(isset($_GET['page'])){
        $O_page = mysql_real_escape_string(check_input($_GET['page']));
    }

    $datas = $obj_port->get_data_by_page($O_page);
    //var_dump($datas);
    if(is_array($datas)){
        $total_data = $datas[0]['total_data_all'];
        $total_page = $datas[0]['total_page'];
    }else{
        $total_data = 0;
        $total_page = 0;
    }

    if(isset($_SESSION['status'])){
        $message = $_SESSION['status'];
        unset($_SESSION['status']);
    } else {
        $message = "";
    }

    if(isset($_SESSION['alert'])){
        $alert = $_SESSION['alert'];
        unset($_SESSION['alert']);
    } else {
        $alert = "";
    }
    
    $obj_con->down();

} else if(isset($_GET['action'])){

    if($_GET['action'] == "add"){
        $obj_con->up();

        $N_title = mysql_real_escape_string(check_input($_POST['title']));
        $N_desc = mysql_real_escape_string(check_input($_POST['desc']));

        $ran = rand();
        $timestamp = time();
        $Photo_ImgLink = "uploads/gallery/" . $timestamp . $ran . cleanSpace($_FILES["image"]["name"]);
        $ThmbPhoto_ImgLink = "uploads/gallery-thmb/" . $timestamp . $ran . cleanSpace($_FILES["image"]["name"]);

        if ((($_FILES["image"]["type"] == "image/gif") || ($_FILES["image"]["type"] == "image/jpeg") || ($_FILES["image"]["type"] == "image/jpg") || ($_FILES["image"]["type"] == "image/JPG") || ($_FILES["image"]["type"] == "image/png")) && ($_FILES["image"]["size"] < 10048576) && ($_FILES["image"]["name"] != "")) {
        require_once("../../packages/imageProcessor.php");
        if ($_FILES["image"]["error"] > 0) {
            //echo "Return Code: " . $_FILES["file"]["error"][$k] . "<br />";
        } else {
            $file = $_FILES["image"]["tmp_name"];
            //Resizing images
            if (true !== ($pic_error = @resampimage(1300, 600, "$file", "../../../$Photo_ImgLink", 0))) {
                echo $pic_error;
                unlink($Photo_ImgLink);
            }

            if (true !== ($pic_error1 = @resampimagethmb("$file", "../../../$ThmbPhoto_ImgLink", 0))) {
                echo $pic_error1;
                unlink($ThmbPhoto_ImgLink);
            }

            $result = $obj_port->insert_data($N_title, $Photo_ImgLink, $ThmbPhoto_ImgLink, $N_desc);
            if($result <= 0){
                $message = "Something is wrong with your submission.<br />";
                $_SESSION['alert'] = "error";
            }else if($result == 1){
                $message = "Gallery <i><b>'" . $N_title . "'</b></i> has been succesfully added as a new gallery image.<br />";
                $_SESSION['alert'] = "success";
            }else{
                $_SESSION['alert'] = "error";
                die();
            }
        }
    }else if ($_FILES["image"]["name"] != "") {
        $message = "Something is wrong with your uploaded gallery image, please make sure that the format is (jpg, png, gif) and the size is less than 9.5MB.<br />";
        $_SESSION['alert'] = "error";
    }

    $_SESSION['status'] = $message;
    header("Location:index.php");
    $obj_con->down();

    } else if($_GET['action'] == "delete"){
        $obj_con->up();
        $O_id = mysql_real_escape_string(check_input($_GET['id']));
        $O_title = mysql_real_escape_string(check_input($_GET['title']));
        
        $result = $obj_port->delete_data($O_id);
        if($result <= 0){
            $message = "Something is wrong while deleting the Data<br />";
            $_SESSION['alert'] = "error";
        }else if($result == 1){
            $message = "Gallery <b><i>'" . $O_title . "'</i></b> has been deleted successfully.<br />";
            $_SESSION['alert'] = "success";
        }

        $_SESSION['status'] = $message;
        header("Location:index.php");
        $obj_con->down();
    }
}
?>