<?php 
require_once("../../../model/Connection.php");
$obj_con = new Connection();

require_once("../../../model/Ongkir.php");
$obj_ongkir = new Ongkir(); 

if(!isset($_GET['action'])){
   
    if(isset($_GET['q']) && $_GET['q'] != ""){
         $obj_con->up();
    
        $O_resi = mysql_real_escape_string($_REQUEST['q']);
        $O_id = mysql_real_escape_string($_REQUEST['id']);
        $data_resi = $obj_ongkir->trackingResi($O_resi);
        if($data_resi != "error"){
            $datas = json_decode($data_resi, true);
            $data_stat = $datas['rajaongkir']['status'];
            if($data_stat['code'] != "400"){
                $data_summary = $datas['rajaongkir']['result']['summary'];
                $data_detail = $datas['rajaongkir']['result']['details'];
                $data_delivery = $datas['rajaongkir']['result']['delivery_status'];
                $data_manifest = $datas['rajaongkir']['result']['manifest'];
            }
        }
        $obj_con->down();
    } else {
        header("Location:index.php");
    }
    
    $obj_con->down();
}
?>