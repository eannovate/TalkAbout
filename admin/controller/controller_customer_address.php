<?php 
require_once("../../../model/Connection.php");
$obj_con = new Connection();
require_once("../../../model/Address.php");
$obj_add = new Address(); 
if(!isset($_GET['action']) && $_GET['id'] != ""){
    $obj_con->up();
    $O_id = mysql_real_escape_string(check_input($_GET['id']));
    $O_name = mysql_real_escape_string(check_input($_GET['name']));
    $datas = $obj_add->get_data_address($O_id);
    //var_dump($datas);

    if(isset($_SESSION['status'])){
        $message = $_SESSION['status'];
        unset($_SESSION['status']);
    } else {
        $message = "";
    }
    if(isset($_SESSION['alert'])){
        $alert = $_SESSION['alert'];
        unset($_SESSION['alert']);
    } else {
        $alert = "";
    }    
    $obj_con->down();

} else if(isset($_GET['action'])){
    if($_GET['action'] == "add"){
        $obj_con->up();
        $N_cust_id = mysql_real_escape_string(check_input($_POST['cust_id']));
        $N_cust_name = mysql_real_escape_string(check_input($_POST['cust_name']));
        $N_name = mysql_real_escape_string(check_input($_POST['name']));
        $N_address1 = mysql_real_escape_string(check_input($_POST['address1']));
        $N_address2 = mysql_real_escape_string(check_input($_POST['address2']));
        $N_cityid = mysql_real_escape_string(check_input($_POST['cityid']));
        $N_city = mysql_real_escape_string(check_input($_POST['city']));
        $N_stateid = mysql_real_escape_string(check_input($_POST['stateid']));
        $N_state = mysql_real_escape_string(check_input($_POST['state']));
        $N_zip = mysql_real_escape_string(check_input($_POST['zip']));
        $N_country = mysql_real_escape_string(check_input($_POST['country']));

        $result = $obj_add->insert_data($N_cust_id, $N_name, $N_address1, $N_address2, $N_cityid, $N_city, $N_state, $N_stateid, $N_zip, $N_country);
        if($result <= 0){
            $message = "Something is wrong with your submission.<br />";
            $_SESSION['alert'] = "error";
        }else if($result == 1){
            $message = "Address <i><b>'" . $N_address1 . "'</b></i> has been succesfully added.<br />";
            $_SESSION['alert'] = "success";
        }else{
            $_SESSION['alert'] = "error";
            die();
        }
    
        $_SESSION['status'] = $message;
        header("Location:address.php?id=".$N_cust_id."&name=".$N_cust_name);
        $obj_con->down();
    }else if($_GET['action'] == "edit"){
        $obj_con->up();

        $N_id = mysql_real_escape_string(check_input($_POST['Address_id']));
        $N_cust_id = mysql_real_escape_string(check_input($_POST['Address_cust_id']));
        $N_name = mysql_real_escape_string(check_input($_POST['Address_name']));
        $N_address1 = mysql_real_escape_string(check_input($_POST['Address_line1']));
        $N_address2 = mysql_real_escape_string(check_input($_POST['Address_line2']));
        $N_cityid = mysql_real_escape_string(check_input($_POST['Address_cityid']));
        $N_city = mysql_real_escape_string(check_input($_POST['Address_city']));
        $N_stateid = mysql_real_escape_string(check_input($_POST['Address_stateid']));
        $N_state = mysql_real_escape_string(check_input($_POST['Address_state']));
        $N_zip = mysql_real_escape_string(check_input($_POST['Address_zip']));
        $N_country = mysql_real_escape_string(check_input($_POST['Address_country']));
        $N_cust_name = mysql_real_escape_string(check_input($_POST['cust_name']));

        $result = $obj_add->update_data($N_id, $N_name, $N_address1, $N_address2, $N_cityid, $N_city, $N_state, $N_stateid, $N_zip, $N_country);
        if($result <= 0){
            $message = "Something is wrong with your submission.<br />";
            $_SESSION['alert'] = "error";
        }else if($result == 1){
            $message = "Address <i><b>'" . $N_address1 . "'</b></i> has been succesfully edited.<br />";
            $_SESSION['alert'] = "success";
        }else{
            $_SESSION['alert'] = "error";
            die();
        }
    
        $_SESSION['status'] = $message;
        header("Location:address.php?id=".$N_cust_id."&name=".$N_cust_name);
        $obj_con->down();

    }else if($_GET['action'] == "delete"){
        $obj_con->up();
        $O_id = mysql_real_escape_string(check_input($_GET['id']));
        $O_cust_id = mysql_real_escape_string(check_input($_GET['cust_id']));
        $O_title = mysql_real_escape_string(check_input($_GET['title']));
        $O_cust_name = mysql_real_escape_string(check_input($_GET['name']));
        
        $result = $obj_add->delete_data($O_id);
        if($result <= 0){
            $message = "Something is wrong while deleting the Data<br />";
            $_SESSION['alert'] = "error";
        }else if($result == 1){
            $message = "Address <b><i>'" . $O_title . "'</i></b> has been deleted successfully.<br />";
            $_SESSION['alert'] = "success";
        }

        $_SESSION['status'] = $message;
        header("Location:address.php?id=".$O_cust_id."&name=".$O_cust_name);
        $obj_con->down();
    }
}
?>