<?php 
//include("packages/require.php");
//include("controller/controller_home.php");
$curpage='home';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo $seo['title-home'];?></title>
    <meta name="keywords" content="<?php echo $seo['keyword-home'];?>">
    <meta name="description" content="<?php echo $seo['desc-home'];?>">
    <?php include("packages/head.php");?>
    <link rel="stylesheet" href="<?php echo $global['absolute-url'];?>packages/swiper/css/swiper.min.css"/>
    <script src="<?php echo $global['absolute-url'];?>packages/swiper/js/swiper.min.js"></script>
</head>
<body>
	<div class="device-max">
		<!-- START SECTION HEADER -->
		<?php include("parts/part-header.php");?>
		<!-- END SECTION HEADER -->
		<!-- START SECTION BODY -->	
		<div class="section-body up2">
			<div class="container" style="width: 100%">
				<div class="row">
					<div class="col-xs-12">
						<div class="title center border-top border-bottom font16px padding-top-5 padding-bottom-5 MontserratRegular">
							BLOG
						</div>
					</div>
				</div>
			</div>
			<div class="container" style="width: 100%">
				<div class="row">
					<div class="col-xs-12 pad0">
						<div class="up4">
							<ol class="breadcrumb background-white MontserratLight">
								<li class="breadcrumb-item"><a href="index2.php">HOME</a></li>
							  	<li class="breadcrumb-item active">BLOG</li>
							</ol>
						</div>
					</div>
				</div>
			</div>
			<div class="container" style="width: 100%">
				<div class="row down3 margin-bottom-xs-20">
					<div class="col-xs-12">
						<div class="background-artikel-big background-artikel-xs" style="background-image: url(<?php echo $global['absolute-url'];?>img/Blog/pexels-photo-27696-cropped.jpg);">
							<div class="artikel-big artikel-xs">
								<div class="artikel-title-big artikel-title-xs white height-xs-32 line-height-15"><strong>Summer color that catches your eyes</strong></div>
								<div class="artikel-description-big artikel-description-xs white">Breath in and live out the feast of Summer</div>
								<div class="artikel-more-big artikel-more-xs">
									<a href="blog-content.php">
										Read More
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="container" style="width: 100%">
				<div class="row down3">
					<div class="col-sm-6 col-xs-12 margin-bottom-xs-20">
						<div class="background-artikel-small background-artikel-xs" style="background-image: url(<?php echo $global['absolute-url'];?>img/Blog/pexels-photo-47401.jpg);">
							<div class="artikel-small artikel-xs">
								<div class="artikel-title-small artikel-title-xs white height-50 height-xs-32 line-height-15"><strong>The Definitive Order Of All Your Holiday Beauty Prep</strong></div>
								<div class="artikel-description-small artikel-description-xs white">Time to show yourself</div>
								<div class="artikel-more-small artikel-more-xs">
									<a href="#">
										Read More
									</a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-xs-12">
						<div class="background-artikel-small background-artikel-xs" style="background-image: url(<?php echo $global['absolute-url'];?>img/Blog/pexels-photo-199165.jpg);">
							<div class="artikel-small artikel-xs">
								<div class="artikel-title-small artikel-title-xs white height-50 height-xs-32 line-height-15"><strong>It's never too much sexyness</strong></div>
								<div class="artikel-description-small artikel-description-xs white">Red is the new sexy says Carlous Breytnod</div>
								<div class="artikel-more-small artikel-more-xs">
									<a href="#">
										Read More
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<center>
						<div class="col-xs-12 pad0">
							<ul class="pagination-blog pad0 MontserratLight">
							    <li>
							      	<a href="#" aria-label="Previous" style="border-style: none;" class="pagination-list">
							        	<span aria-hidden="true">
							        		<div class="glyphicon glyphicon-menu-left"></div>
							        	</span>
							      	</a>
							    </li>
							    <li><a href="#" class="pagination-list">...</a></li>
							    <li><a href="#" class="pagination-list">1</a></li>
							    <li><a href="#" class="pagination-list">2</a></li>
							    <li><a href="#" class="pagination-list">3</a></li>
							    <li><a href="#" class="pagination-list">4</a></li>
							    <li><a href="#" class="pagination-list">5</a></li>
							    <li><a href="#" class="pagination-list">...</a></li>
							    <li>
							      	<a href="#" aria-label="Next" style="border-style: none;" class="pagination-list">
							        	<span aria-hidden="true">
							        	<div class="glyphicon glyphicon-menu-right"></div>
							        	</span>
							      	</a>
							    </li>
						  	</ul>
						</div>
					</center>
				</div>
			</div>
				
		</div>
		<!-- END SECTION BODY -->
		<!-- START SECTION FOOTER -->
		<?php include("parts/part-footer.php");?>
		<!-- END SECTION FOOTER -->
	</div> 
    <script src="<?php echo $global['absolute-url'];?>js/global.js"></script>
    <script src="<?php echo $global['absolute-url'];?>js/home.js"></script>
</body>	
</html>