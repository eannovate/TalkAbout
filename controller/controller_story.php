<?php 
require_once("model/Connection.php");
$obj_connect = new Connection();

require_once("model/Work.php");
$obj_work= new Work();

require_once("model/Story.php");
$obj_story = new Story();

require_once("model/Category.php");
$obj_category = new Category();

if(!isset($_GET['action'])){
    $obj_connect->up();

    if(isset($_GET['cat_id'])){
    	$O_catid = mysql_real_escape_string(check_input($_GET['cat_id'])); //id work
    	$data_cat = $obj_category->get_data_detail($O_catid);
	} else {
		$O_catid = "";
	}
    $data_featureds = $obj_story->get_featured_story();
    $data_categorys = $obj_category->get_data_story();

    $obj_connect->down();
} 
?>