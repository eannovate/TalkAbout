<?php 
require_once("model/Connection.php");
$obj_connect = new Connection();

require_once("model/Customer.php") ;
$obj_cust = new Customer() ; 

require_once("model/Address.php");
$obj_address = new Address();

if(!isset($_GET['action'])){
	
	$obj_connect->up();
	
	if(isset($_SESSION['loginStatus'])){
        $loginStatus = $_SESSION['loginStatus'];
        unset($_SESSION['loginStatus']);
    } else {
        $loginStatus = "";
    }
    if(isset($_SESSION['loginMessage'])){
        $loginMessage = $_SESSION['loginMessage'];
        unset($_SESSION['loginMessage']);
    } else {
        $loginMessage = "";
    }

	$obj_connect->down();

} else if(isset($_GET['action'])){ 

	if($_GET['action'] == 'login'){
		$obj_connect->up();

		$O_email = mysql_real_escape_string($_POST['email']);;
        $O_password = mysql_real_escape_string(check_input($_POST['password']));
        
        $result = $obj_cust->login($O_email, $O_password);      

    	if($result == 1){
            header("Location:{$path['account']}");
    	}else{
            $message = "Failed To Login<br/>Please Try Again !";
            $status = "failed";    	    
            header("Location:{$path['login']}");
    	}
        $_SESSION['loginStatus'] = $status;
        $_SESSION['loginMessage'] = $message;
    	$obj_connect->down();

	} else if ($_GET['action'] == 'register'){

        $obj_connect -> up();
        $O_fname = mysql_real_escape_string(check_input($_POST['name']));        
        $O_email = mysql_real_escape_string(check_input($_POST['email']));
        $O_pass = mysql_real_escape_string(check_input($_POST['password']));        
        $O_phone= mysql_real_escape_string(check_input($_POST['phone']));
        $O_address = mysql_real_escape_string(check_input($_POST['address'])); 
        $O_city = mysql_real_escape_string(check_input($_POST['city']));
        $O_cityid = mysql_real_escape_string(check_input($_POST['cityid']));       
        $O_state = mysql_real_escape_string(check_input($_POST['state']));  
        $O_stateid = mysql_real_escape_string(check_input($_POST['stateid']));
        $O_zip = mysql_real_escape_string(check_input($_POST['zip']));
        $O_addressName = "Default";
        $O_country = "Indonesia";  
        $m_page = $path['login'];

        $O_bodyCust = " <!DOCTYPE html>
                        <html>
                        <head>
                        <title></title>
                        </head>
                        <body>
                        <span style='font-size:12px;'><em>*This is a message from Volia</em></span>
                        <br/>
                        <br/><strong> Your account has been successfully created.</strong><br/>
                        Thank you for register as customer in our website.
                        <br/><br/>
                        <b>Name</b> : $O_fname
                        <br/><br/>
                        <b>E-mail</b> : $O_email
                        <br/><br/>      
                        <b>Phone</b> : $O_phone
                        <br/><br/>
                        <b>Adress</b> : $O_address
                        <br/><br/>
                        <b>City</b> : $O_city
                        <br/><br/>
                        Please follow this link for login : 
                        <a href='".$path['login']."'>Login</a>
                        <br/><br/>
                        <em>*Do not reply to this e-mail.<br/><br/>Thank you!</em>
                        </body>
                        </html>
                        ";

        $O_bodyAdmin = " <!DOCTYPE html>
                        <html>
                        <head>
                        <title></title>
                        </head>
                        <body>
                        <span style='font-size:12px;'><em>*This is a message from Volia</em></span>
                        <br/>
                        <br/><strong> New account has been successfully created.</strong><br/>
                        <br/><br/>
                        <b>Name</b> : $O_fname
                        <br/><br/>
                        <b>E-mail</b> : $O_email
                        <br/><br/>      
                        <b>Phone</b> : $O_phone
                        <br/><br/>
                        <b>Adress</b> : $O_address
                        <br/><br/>
                        <b>City</b> : $O_city
                        <br/><br/
                        <em>*Do not reply to this e-mail.<br/><br/>Thank you!</em>
                        </body>
                        </html>
                        ";

       
        $O_toCust = $O_email;

        $file_json = "uploads/json/company_profile.json";
        $json = json_decode(file_get_contents($file_json),TRUE);
        $J_Company_email = $json['email1'];
        $O_toCompany = $J_Company_email;

        $O_subjectCust = 'Volia Website account registration';
        $O_subjectAdmin = 'New account registered';

        $result_cust = $obj_cust->insert_cust($O_fname, $O_email, $O_pass, $O_phone);
        if($result_cust){
            $result_address = $obj_address->insert_address($result_cust, $O_addressName, $O_address, $O_cityid, $O_city, $O_state, $O_stateid, $O_zip, $O_country);
            $sentCust = smtpmailer($O_toCust, $smtp['url'], $smtp['from'], $smtp['password'], $seo['company-name'], $O_subjectCust, $O_bodyCust); 
            $sentAdmin = smtpmailer($O_toCompany, $smtp['url'], $smtp['from'], $smtp['password'], $seo['company-name'], $O_subjectAdmin, $O_bodyAdmin);                  
            $message = "Thank you for registering at Volia.";
            $status = "success";                 
        } 
        else {
            $message = "Failed To Register. Email already exist!";
            $status = "failed";
        }


        $_SESSION['loginStatus'] = $status;
        $_SESSION['loginMessage'] = $message;
        header("Location:{$m_page}");
        $obj_connect -> down();

    }else if($_GET['action'] == 'forgot'){
        $obj_connect->up();

        $N_email = $_POST['email'];
        $m_page = $path['login'];
        $O_subject = 'Forget password Volia Website';
        
        $check = $obj_cust->check_forget_password($N_email);
        //var_dump($check);

        if(is_array($check)){
            $N_code = $obj_cust->generate_reset_code($check[0]['customer_email']);
            
            $O_body = "
            Hi ".$check[0]['customer_fname'].",<br>
            To change your password, please click the link below.<br><br>
            <a target='_blank' href='".$path['reset-password'].$check[0]['customer_fname']."/".$N_code."/'><strong>Change my password</strong></a><br><br><br>
            Kind Regards,<br><br>
            Volia<br><br><br>  
            *Do not reply to this e-mail.<br>
            Thank you!
                ";

            $sent = smtpmailer($N_email, $smtp['url'], $smtp['from'], $smtp['password'], $seo['company-name'], $O_subject, $O_body);
            if($sent){
                $message = "E-Mail konfirmasi telah berhasil di kirim. Tolong check E-Mail anda untuk konfirmasi reset password.";
                $status = "success";
            }else{
                $message = "E-mail yang anda masukan tidak ditemukan.2";
                $status = "failed";
            }
        }else{
            $message = "E-mail yang anda masukan tidak ditemukan.1";
            $status = "failed";
        }    
        //var_dump($N_code);
        $_SESSION['loginStatus'] = $status;
        $_SESSION['loginMessage'] = $message;
        header("Location:{$m_page}");
        $obj_connect->down();           
    }
}
?>