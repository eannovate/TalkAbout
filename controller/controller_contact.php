<?php 
require_once("model/Connection.php");
$obj_connect = new Connection();

if(!isset($_GET['action'])){
    $obj_connect->up();

    if(isset($_SESSION['contactStatus'])){
        $contactStatus = $_SESSION['contactStatus'];
        unset($_SESSION['contactStatus']);
    } else {
        $contactStatus = "";
    }
    if(isset($_SESSION['contactMessage'])){
        $contactMessage = $_SESSION['contactMessage'];
        unset($_SESSION['contactMessage']);
    } else {
        $contactMessage = "";
    }

    if(isset($_SESSION['result-name'])){
        $result_name = $_SESSION['result-name'];
        unset($_SESSION['result-name']);
    } else {
        $result_name = "";
    }
    if(isset($_SESSION['result-email'])){
        $result_email = $_SESSION['result-email'];
        unset($_SESSION['result-email']);
    } else {
        $result_email = "";
    }
   
    if(isset($_SESSION['result-phone'])){
        $result_phone = $_SESSION['result-phone'];
        unset($_SESSION['result-phone']);
    } else {
        $result_phone = "";
    }  

    if(isset($_SESSION['result-subject'])){
        $result_subject = $_SESSION['result-subject'];
        unset($_SESSION['result-subject']);
    } else {
        $result_subject = "";
    }

    if(isset($_SESSION['result-message'])){
        $result_message = $_SESSION['result-message'];
        unset($_SESSION['result-message']);
    } else {
        $result_message = "";
    }

    $obj_connect->down();
} else if(isset($_GET['action'])){

    if($_GET['action'] == 'send'){
        $obj_connect -> up();
        $O_name = mysql_real_escape_string(check_input($_POST['name']));
        $O_email = mysql_real_escape_string(check_input($_POST['email']));        
        $O_phone = mysql_real_escape_string(check_input($_POST['phone']));       
        $O_subject = mysql_real_escape_string(check_input($_POST['subject']));
        $O_message = mysql_real_escape_string(check_input($_POST['message']));
        
        $file_json = "uploads/json/company_profile.json";
        $json = json_decode(file_get_contents($file_json),TRUE);
        $J_Company_email = $json['email1'];

        $O_to = $J_Company_email;
        //$O_to = "riandyfebrianto@gmail.com";
        $O_contactCaptcha = $_POST['captcha'];
        $m_page = $path['contact'];
        
        $O_body = "
        <!DOCTYPE html>
        <html>
        <head>
        <title></title>
        </head>
        <body>
        <span style='font-size:12px;'><em>*This is a Visitor message email from website</em></span>
        <br/><br/>
        <b>Visitor Name</b> : ".$O_name."
        <br/><br/>
        <b>Visitor E-mail</b> : ".$O_email."
        <br/><br/>              
        <b>Phone</b> : ".$O_phone."
        <br/><br/>
        <b>Subject</b> : ".$O_subject."
        <br/><br/>         
        <b>Message</b> : <br/>
        ".str_replace('\r\n','<br/>',$O_message)."     
        <br/><br/>
        <em>*Do not reply to this e-mail.<br/><br/>Thank you!</em>
        </body>
        </html>
        ";

        if($O_contactCaptcha == $_SESSION['captcha_session']){
            $sent = smtpmailer($O_to, $smtp['url'], $smtp['from'], $smtp['password'], $seo['company-name'], $O_subject, $O_body);
            $message = "Thanks For Sending message to Us.";
            $status = "success";
        } else {
            $message = "Failed To Sending message Please Check Your Input!";
            $status = "failed";
            //create session failed
            $_SESSION['result-name'] = $O_name;
            $_SESSION['result-email'] = $O_email;            
            $_SESSION['result-phone'] = $O_phone;
            $_SESSION['result-subject'] = $O_subject;
            $_SESSION['result-message'] = $O_message;
        }

        $_SESSION['contactStatus'] = $status;
        $_SESSION['contactMessage'] = $message;
        header("Location:{$m_page}");
        $obj_connect -> down();
    }
}
?>