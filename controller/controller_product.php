<?php 
require_once("model/Connection.php");
$obj_connect = new Connection();

require_once("model/Product.php");
$obj_product = new Product();

if(!isset($_GET['action'])){
    $obj_connect->up();
    
    $O_title = mysql_real_escape_string(check_input($_GET['title']));
    $O_page = isset($_GET['page']) ? mysql_real_escape_string(check_input($_GET['page'])) : 1;
    $datas = $obj_product->get_product_by_category($O_page, $O_title);
    $data_bestsellers = $obj_product->get_product_bestseller_by_category($O_title);
    //var_dump($datas);
    $total_data = is_array($datas) ? $datas[0]['total_data_all'] : 0;
    $total_page = is_array($datas) ? $datas[0]['total_page'] : 0;

    $obj_connect->down();

}
?>