<?php 
require_once("model/Connection.php");
$obj_connect = new Connection();

require_once("model/Category.php");
$obj_category = new Category();

require_once("model/Product.php");
$obj_product = new Product();

require_once("model/Banner.php");
$obj_banner = new Banner();

if(!isset($_GET['action'])){
    $obj_connect->up();
    $O_id = isset($_GET['id']) ? mysql_real_escape_string(check_input($_GET['id'])) : 0;
    
    $center_top = $obj_banner->get_by_page('home','center-top');
    $center_left = $obj_banner->get_by_page('home','center-left');
    $center_right = $obj_banner->get_by_page('home','center-right');
    $bottom = $obj_banner->get_by_page('home','bottom');

    $datas = $obj_category->get_category_by_parent($O_id);
    if(is_array($datas)){
    	$strid = "";
        get_id_by_parent($strid, $datas[0]['category_ID']);
        $category_id = $strid != "" ? substr($strid, 1) : 0;
        
        $data_best_seller = $obj_product->get_product_feature_by_category("best_seller", $category_id);
        $data_trending = $obj_product->get_product_feature_by_category("trending", $category_id);
        
    }
    
    $obj_connect->down();
} 
?>