<?php 
require_once("model/Connection.php");
$obj_connect = new Connection();

require_once("model/Homepage.php");
$obj_homepage = new Homepage();

require_once("model/Newsletter.php");
$obj_newsletter = new Newsletter();

require_once("model/Banner.php");
$obj_banner = new Banner();

require_once("model/Product.php");
$obj_product = new Product();

if(!isset($_GET['action'])){
    $obj_connect->up();

    $data_sliders = $obj_homepage->get_index();

    $center_left = $obj_banner->get_by_page('home','center-left');
    $center_right = $obj_banner->get_by_page('home','center-right');

    $product_bests = $obj_product->get_home_best();
    //$product_trends = $obj_product->get_home_trend();


    $obj_connect->down();
} else if(isset($_GET['action'])){

    if($_GET['action'] == 'subscribe' ){

        $obj_connect->up();

        $N_email = mysql_real_escape_string(check_input($_POST['subscribe']));
        $m_page = $_POST['url'];

        $result_mail = $obj_newsletter->check_data($N_email);
        if($result_mail <= 0){
            $result = $obj_newsletter->insert_data_client($N_email);
            if($result <= 0){
                $message = "Something Error With Subscribe Input!";
                $status = "error";
            }else if($result == 1){
                $message = "Thanks For Subscribe Our Website!";
                $status = "success";
            }else{
                $message = "Something Error With Subscribe Input!";
                $status = "error";
            }
        }else if($result_mail >= 1){
            $message = "Email Already Exist as Subscriber!";
            $status = "error";
        }
        

        $_SESSION['infoStatus'] = $status;
        $_SESSION['infoMessage'] = $message;
        header("Location:$m_page");
        $obj_connect -> down();

    } else if($_GET['action'] == 'logout'){
        if(isset($_SESSION['customer_email'])){
            unset($_SESSION['customer_email']);
        }
        if(isset($_SESSION['customer_name'])){
            unset($_SESSION['customer_name']);
        }
        if(isset($_SESSION['customer_id'])){
            unset($_SESSION['customer_id']);
        }
        header("Location:{$path['home']}");
    }

}
?>