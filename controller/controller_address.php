<?php 
require_once("model/Connection.php");
$obj_connect = new Connection();
	
require_once("model/Customer.php");
$obj_customer = new Customer();

require_once("model/Address.php");
$obj_address = new Address();

if(isset($_SESSION['customer_id'])){

    if(!isset($_GET['action'])){
        $obj_connect->up();

        $cust_id = $_SESSION['customer_id'];
        $datas = $obj_address->get_address_by_customer($cust_id);

        if(isset($_SESSION['addressStatus'])){
            $addressStatus = $_SESSION['addressStatus'];
            unset($_SESSION['addressStatus']);
        } else {
            $addressStatus = "";
        }

        if(isset($_SESSION['addressMessage'])){
            $addressMessage = $_SESSION['addressMessage'];
            unset($_SESSION['addressMessage']);
        } else {
            $addressMessage = "";
        }

        $obj_connect->down();

    } else if(isset($_GET['action'])){

    	if($_GET['action'] == "add"){
            $obj_connect->up();

            $N_cust_id = mysql_real_escape_string(check_input($_POST['cust_id']));
            $N_name = mysql_real_escape_string(check_input($_POST['name']));
            $N_address1 = mysql_real_escape_string(check_input($_POST['address1']));
            $N_address2 = mysql_real_escape_string(check_input($_POST['address2']));
            $N_cityid = mysql_real_escape_string(check_input($_POST['cityid']));
            $N_city = mysql_real_escape_string(check_input($_POST['city']));
            $N_stateid = mysql_real_escape_string(check_input($_POST['stateid']));
            $N_state = mysql_real_escape_string(check_input($_POST['state']));
            $N_zip = mysql_real_escape_string(check_input($_POST['zip']));
            $N_country = "Indonesia";

            $result = $obj_address->insert_data($N_cust_id, $N_name, $N_address1, $N_address2, $N_cityid, $N_city, $N_state, $N_stateid, $N_zip, $N_country);
            if ($result <= 0){
                $message = "Failed adding new address!";
                $status = "failed";
            } else if ($result == 1){
                $message = "Success adding new address!";
                $status = "success";
            } else {
                $message = "Failed adding new address!";
                $status = "failed";
            }

            $_SESSION['addressStatus'] = $status;
            $_SESSION['addressMessage'] = $message;
            header("Location:".$path['address']);
            $obj_connect->down();

        } else if ($_GET['action'] == "edit"){
            $obj_connect->up();

            $N_id = mysql_real_escape_string(check_input($_POST['Address_id']));
            $N_cust_id = mysql_real_escape_string(check_input($_POST['Address_cust_id']));
            $N_name = mysql_real_escape_string(check_input($_POST['Address_name']));
            $N_address1 = mysql_real_escape_string(check_input($_POST['Address_line1']));
            $N_address2 = mysql_real_escape_string(check_input($_POST['Address_line2']));
            $N_cityid = mysql_real_escape_string(check_input($_POST['Address_cityid']));
            $N_city = mysql_real_escape_string(check_input($_POST['Address_city']));
            $N_stateid = mysql_real_escape_string(check_input($_POST['Address_stateid']));
            $N_state = mysql_real_escape_string(check_input($_POST['Address_state']));
            $N_zip = mysql_real_escape_string(check_input($_POST['Address_zip']));
            $N_country = "Indonesia";

            $result = $obj_address->update_data($N_id, $N_name, $N_address1, $N_address2, $N_cityid, $N_city, $N_state, $N_stateid, $N_zip, $N_country);
            if($result <= 0){
                $message = "Failed to update address!";
                $status = "failed";
            }else if($result == 1){
                $message = "Success to update address!";
                $status = "success";
            }else{
                $message = "Failed editing address!";
                $status = "failed";
            }

            $_SESSION['addressStatus'] = $status;
            $_SESSION['addressMessage'] = $message;
            header("Location:".$path['address']);
            $obj_connect->down();

        } else if ( $_GET['action'] == "delete"){
            $obj_connect->up();
            $O_id = mysql_real_escape_string(check_input($_GET['id']));
            
            $result = $obj_address->delete_data($O_id);
            if($result <= 0){
                $message = "Failed to delete address!";
                $status = "failed";
            }else if($result == 1){
                $message = "Delete address success!";
                $status = "success";
            }

            $_SESSION['addressStatus'] = $status;
            $_SESSION['addressMessage'] = $message;
            header("Location:".$path['address']);
            $obj_connect->down();
        }
    }
} else {
    $_SESSION['loginStatus'] = "failed";
    $_SESSION['loginMessage'] = "You Must Login First!";
    header("Location:".$path['login']);
}
?>