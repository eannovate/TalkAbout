<?php 
require_once("model/Connection.php");
$obj_connect = new Connection();

require_once("model/PO.php") ;
$obj_po = new PO() ; 

if(isset($_SESSION['customer_id'])){
    if(!isset($_GET['action'])){
    	$obj_connect->up();
        $O_page = isset($_GET['page']) ? mysql_real_escape_string(check_input($_GET['page'])) : 1;
        $O_custID = $_SESSION['customer_id'];

        $datas = $obj_po->get_order_customer($O_page, $O_custID);
        //var_dump($datas);
        $total_data = is_array($datas) ? $datas[0]['total_data_all'] : 0;
        $total_page = is_array($datas) ? $datas[0]['total_page'] : 0;
        //var_dump($datas);
       
    	$obj_connect->down();

    } else if(isset($_GET['action'])){ 
    	header("Location:{$path['404']}");
    }
} else {
    $_SESSION['loginStatus'] = "failed";
    $_SESSION['loginMessage'] = "You Must Login First!";
    header("Location:{$path['login']}");
}
?>