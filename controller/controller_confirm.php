<?php 
require_once("model/Connection.php");
$obj_connect = new Connection();

require_once("model/Confirm.php");
$obj_confirm = new Confirm();

require_once("model/PO.php");
$obj_po = new PO();

if(isset($_SESSION['customer_id'])){
	if(!isset($_GET['action'])){
		$obj_connect->up();
		$customer_id = $_SESSION['customer_id'];

		$datas = $obj_po->get_po_order($customer_id);

		if(isset($_SESSION['confirmStatus'])){
	        $confirmStatus = $_SESSION['confirmStatus'];
	        unset($_SESSION['confirmStatus']);
	    } else {
	        $confirmStatus = "";
	    }
	    if(isset($_SESSION['confirmMessage'])){
	        $confirmMessage = $_SESSION['confirmMessage'];
	        unset($_SESSION['confirmMessage']);
	    } else {
	        $confirmMessage = "";
	    }

		$obj_connect->down(); 

	} else if(isset($_GET['action'])){
		if($_GET['action'] == "payment"){
			$obj_connect->up();
	  		
		    $confirm_custID= mysql_real_escape_string(check_input($_POST['confirm_custID']));
		    $confirm_poName = mysql_real_escape_string(check_input($_POST['confirm_poName']));
		    $confirm_poID = mysql_real_escape_string(check_input($_POST['confirm_poID']));
		    $confirm_date = $_POST['confirm_date'];
		    $confirm_amount = mysql_real_escape_string(check_input($_POST['confirm_amount']));
		    $confirm_bank = mysql_real_escape_string(check_input($_POST['confirm_bank']));
		    $confirm_acc= mysql_real_escape_string(check_input($_POST['confirm_acc']));
		    $confirm_behalf= mysql_real_escape_string(check_input($_POST['confirm_behalf']));
		    $confirm_note= mysql_real_escape_string(check_input($_POST['confirm_note']));

		   
		    $toCust = $_SESSION['customer_email'];

		    $file_json = "uploads/json/company_profile.json";
	        $json = json_decode(file_get_contents($file_json),TRUE);
	        $J_Company_email = $json['email1'];
	        $O_toCompany = $J_Company_email;

		    $O_subject = "Volia website $confirm_poName payment Confirmation";

		    $O_bodyCust = " <!DOCTYPE html>
                        <html>
                        <head>
                        <title></title>
                        </head>
                        <body>
                        <span style='font-size:12px;'><em>*This is a Message from Volia Website</em></span>
                        <br/>
                        <br/><b> Thank you for your confirmation payment.</b> Here is your confirmation payment info:<br/>
                        <br/><br/>
                        <b>Order Number</b> : $confirm_poName
                        <br/><br/>
                        <b>Payment Date </b> : $confirm_date
                        <br/><br/> 
                        <b>Paid Amount </b> : $confirm_amount
                        <br/><br/>
                        <b>From Bank </b> : $confirm_bank
                        <br/><br/> 
                        <b>Account No </b> : $confirm_acc
                        <br/><br/>
                        <b>Name on behalf </b> : $confirm_behalf
                        <br/><br/> 
                        <b>Note </b> : $confirm_note
                        <br/><br/><br/>
                        <b>We will notify you when your payment is confirmed.</b>  
                        <br/><br/>
                        <em>*Do not reply to this e-mail.<br/><br/>Thank you!</em>
                        </body>
                        </html>
                        ";
		    $O_bodyAdmin= " <!DOCTYPE html>
                        <html>
                        <head>
                        <title></title>
                        </head>
                        <body>
                        <span style='font-size:12px;'><em>*This is a Message from Volia Website</em></span>
                        <br/>
                        <br/> <b>New confirmation payment has been made by customer. Please check the confirmation order! </b>
                        <br/>
                        Here is the confirmation payment info:<br/>
                        <br/><br/>
                        <b>Order Number</b> : $confirm_poName
                        <br/><br/>
                        <b>Payment Date </b> : $confirm_date
                        <br/><br/> 
                        <b>Paid Amount </b> : $confirm_amount
                        <br/><br/>
                        <b>From Bank </b> : $confirm_bank
                        <br/><br/> 
                        <b>Account No </b> : $confirm_acc
                        <br/><br/>
                        <b>Name on behalf </b> : $confirm_behalf
                        <br/><br/> 
                        <b>Note </b> : $confirm_note
                        <br/><br/>    
                        <em>*Do not reply to this e-mail.<br/><br/>Thank you!</em>
                        </body>
                        </html>
                        ";

		    $confirm_name = $obj_confirm->generate_confirm();
		    if($confirm_name != ""){
				$result_confirm = $obj_confirm->insert_data($confirm_name, $confirm_custID, $confirm_poID, $confirm_amount, $confirm_date, $confirm_bank, $confirm_acc, $confirm_behalf, $confirm_note);
				if (!$result_confirm) {
			        $message = "Failed To Confirm Payment<br/>Please Try Again!";
			        $status = "failed";
			        $m_page = $path['confirm'];
			    } 
			    else if ($result_confirm) {
			    	$result = $obj_po->update_po_status($confirm_poID, 2);
			    	$sentCust = smtpmailer($toCust, $smtp['url'], $smtp['from'], $smtp['password'], $seo['company-name'], $O_subject, $O_bodyCust);
			    	$sentAdmin = smtpmailer($O_toCompany,  $smtp['url'], $smtp['from'], $smtp['password'], $seo['company-name'], $O_subject, $O_bodyAdmin);
				    $message = "Thank you for Confirming Your Payment.";
				    $status = "success";
			        $m_page = $path['confirm-success'].$confirm_name.'/';
			    }
			} else {
				$message = "Failed To Confirm payment<br/>Please Try Again !";
			    $status = "failed";
			    $m_page = $path['confirm'];
			}

		    $_SESSION['confirmStatus'] = $status;
		    $_SESSION['confirmMessage'] = $message;
		    header("Location:{$m_page}");
			$obj_connect->down();
		}

	} 
} else {
	$_SESSION['loginStatus'] = "failed";
	$_SESSION['loginMessage'] = "You Must Login First Before <br/>Confirming Payment!";
	header("Location:{$path['login']}");
}
?>