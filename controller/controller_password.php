<?php
require_once("model/Connection.php");
$obj_connect = new Connection();

require_once("model/Customer.php") ;
$obj_cust = new Customer() ; 

if(isset($_SESSION['customer_id'])){

    if(!isset($_GET['action'])){
    	$obj_connect->up();
    	
    	if(isset($_SESSION['passwordStatus'])){
            $passwordStatus = $_SESSION['passwordStatus'];
            unset($_SESSION['passwordStatus']);
        } else {
            $passwordStatus = "";
        }

        if(isset($_SESSION['passwordMessage'])){
            $passwordMessage = $_SESSION['passwordMessage'];
            unset($_SESSION['passwordMessage']);
        } else {
            $passwordMessage = "";
        }

    	$obj_connect->down();

    } else if(isset($_GET['action'])){ 
    
    	if($_GET['action'] == 'password'){
    		$obj_connect->up();

            $O_custID = $_SESSION['customer_id'];
    		$O_oldpassword = $_POST['oldpassword'];
            $O_newpassword = $_POST['password'];
            
            $resultCheck = $obj_cust->check_password($O_custID, $O_oldpassword);      
        	if($resultCheck == 1){
                $result = $obj_cust->update_password($O_custID, $O_newpassword);
                if($resultCheck == 1){
                    $message = "Password Successfully Changed!";
                    $status = "success";
                } else  {
                    $message = "Password Change failed!";
                    $status = "failed";
                }
        	}else{
                $message = "Your Old Password is Incorrect !";
                $status = "failed";
        	}

            $_SESSION['passwordStatus'] = $status;
            $_SESSION['passwordMessage'] = $message;
            header("Location:".$path['password']);
        	$obj_connect->down();
    	}
    }
} else {
    $_SESSION['loginStatus'] = "failed";
    $_SESSION['loginMessage'] = "You Must Login First!";
    header("Location:".$path['login']);
}
?>