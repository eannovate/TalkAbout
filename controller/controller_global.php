<?php
require_once("model/Connection.php");
$obj_connect = new Connection();

require_once("model/Category.php");
$obj_category = new Category();

if(!isset($_GET['action'])){
    $obj_connect->up();
    
    $data_categorys = $obj_category->get_index();

    //$brands = $obj_brand->get_brand_by_alphabet();
    //var_dump($brands);

    //get data for company profile
	$file_profile = "uploads/json/company_profile.json";
	$json_profile = json_decode(file_get_contents($file_profile),TRUE);
	$JP_company = $json_profile['company_name'];
    $JP_address1 = $json_profile['address1'];
    
    $JP_city = $json_profile['city'];
    $JP_province = $json_profile['province'];
    $JP_zip = $json_profile['zip'];
    $JP_country = $json_profile['country'];
    $JP_email2 = $json_profile['email2']; 
    $JP_phone1 = $json_profile['phone1'];
    $JP_short_desc = $json_profile['short-desc'];

    $JP_website = $json_profile['website'];
    //$JP_promo = $json_profile['promo'];
    $JP_bank = $json_profile['bank'];

    //get data for social
    $file_social = "uploads/json/social.json";
    $json_social = json_decode(file_get_contents($file_social),TRUE);

    $JS_facebook = $json_social['facebook'];
    $JS_instagram = $json_social['instagram'];
    $JS_gplus = $json_social['gplus'];
    $JS_twitter = $json_social['twitter'];
    
    if(isset($_SESSION['infoStatus'])){
        $infoStatus = $_SESSION['infoStatus'];
        unset($_SESSION['infoStatus']);
    } else {
        $infoStatus = "";
    }

    if(isset($_SESSION['infoMessage'])){
        $infoMessage = $_SESSION['infoMessage'];
        unset($_SESSION['infoMessage']);
    } else {
        $infoMessage = "";
    }
    
	$obj_connect->down();

} else if($_GET['action'] == 'login'){
    create_session(28, "Kevin Pratama", "yungyung131331@yahoo.com", "");
    header("Location:".$path['home']);

} else if($_GET['action'] == 'logout'){
    if(isset($_SESSION['customer_id'])){
        end_session();
    }
    header("Location:".$path['home']);
}
?>