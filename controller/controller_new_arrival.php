<?php 
require_once("model/Connection.php");
$obj_connect = new Connection();

require_once("model/Homepage.php");
$obj_homepage = new Homepage();

require_once("model/Newsletter.php");
$obj_newsletter = new Newsletter();

require_once("model/Banner.php");
$obj_banner = new Banner();

require_once("model/Product.php");
$obj_product = new Product();

if(!isset($_GET['action'])){
    $obj_connect->up();
    $O_page = isset($_GET['page']) ? mysql_real_escape_string(check_input($_GET['page'])) : 1;
    
    $data_new_arrivals = $obj_product->get_latest_by_page($O_page);
    $total_data = is_array($data_new_arrivals) ? $data_new_arrivals[0]['total_data_all'] : 0;
    $total_page = is_array($data_new_arrivals) ? $data_new_arrivals[0]['total_page'] : 0;

    $obj_connect->down();
}
?>