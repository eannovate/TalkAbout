<?php
session_start();

include("Facebook/Facebook_key.php"); // include api facebook key
require_once("Facebook/autoload.php"); // added in v4.0.0
require_once("../packages/front_config.php");

require_once("../model/Connection.php");
$obj_con = new Connection();

require_once("../model/Third_party.php");
$obj_fb = new Third_party();

// added facebook sdk v4
require_once('Facebook/FacebookSession.php');
require_once('Facebook/FacebookRedirectLoginHelper.php');
require_once('Facebook/FacebookRequest.php');
require_once('Facebook/FacebookResponse.php');
require_once('Facebook/FacebookSDKException.php');
require_once('Facebook/FacebookRequestException.php');
require_once('Facebook/FacebookAuthorizationException.php');
require_once('Facebook/GraphObject.php');
require_once('Facebook/Entities/AccessToken.php');
require_once('Facebook/HttpClients/FacebookHttpable.php');
require_once('Facebook/HttpClients/FacebookCurlHttpClient.php');
require_once('Facebook/HttpClients/FacebookCurl.php');

// use namespace
use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\FacebookSDKException;
use Facebook\FacebookRequestException;
use Facebook\FacebookAuthorizationException;
use Facebook\GraphObject;
use Facebook\Entities\AccessToken;
use Facebook\HttpClients\FacebookCurlHttpClient;
use Facebook\HttpClients\FacebookHttpable;

// init app with app id and secret
FacebookSession::setDefaultApplication($app_id, $app_secret);
// login helper with redirect_uri
$helper = new FacebookRedirectLoginHelper($redirect_uri);
try{
  $session = $helper->getSessionFromRedirect();
}catch(FacebookRequestException $ex){
  // When Facebook returns an error
}catch(Exception $ex){
  // When validation fails or other local issues
}

// see if we have a session
if(isset($session)){
  $obj_con->up();

  // graph api request for user data
  $request = new FacebookRequest($session, 'GET', '/me?fields=id,name,email,birthday,about,gender,first_name,last_name');
  $response = $request->execute();
  // get response
  $graphObject = $response->getGraphObject();

  // check reference field in https://developers.facebook.com/docs/graph-api/reference/user
  $fb_id              = $graphObject->getProperty('id');         // To Get Facebook ID
  $fb_firstname       = $graphObject->getProperty('first_name'); // To Get Facebook first name
  $fb_lastname        = $graphObject->getProperty('last_name');  // To Get Facebook last name
 	$fb_fullname        = $graphObject->getProperty('name');       // To Get Facebook full name
	$fb_email           = $graphObject->getProperty('email');      // To Get Facebook email ID
  $fb_birthday        = $graphObject->getProperty('birthday');   // To Get Facebook birthday
  $fb_profile_picture = "https://graph.facebook.com/".$fb_id."/picture?type=large"; // To Get Profile Picture
  $fb_provider        = "Facebook"; // Provider Name

  if($fb_email == NULL){
    $req = new FacebookRequest($session, 'DELETE', '/'.$fb_id.'/permissions'); //revoke permissions access app
    $res = $req->execute();
    //redirect facebook login url
    $location = $helper->getLoginUrl(array('scope'=>$scope));
  }else{
    $check = $obj_fb->check_primary_email($fb_email); // check e-mail 
    if(is_array($check)){
      $result = $obj_fb->user_login($check[0]['customer_email']);
      if(is_array($result)){
        $location = $path['account']; //redirect url
      }else{
        $location = $path['home']; //redirect url
      }
    }else{
      $N_fname    = $fb_firstname." ".$fb_lastname;
      $N_email    = $fb_email;
      $N_provider = $fb_provider;

      //START VARIABLE SEND E-MAIL
      $O_bodyCust = " <!DOCTYPE html>
        <html>
        <head>
        <title></title>
        </head>
        <body>
        <span style='font-size:12px;'><em>*This is a message from Volia</em></span>
        <br/>
        <br/><strong> Your account has been successfully created.</strong><br/>
        Thank you for register as customer in our website.
        <br/><br/>
        <b>Name</b> : ".$N_fname."
        <br/><br/>
        <b>E-mail</b> : ".$N_email."
        <br/><br/>      
        <b>Register via</b> : ".$N_provider."
        <br/><br/>
        Please follow this link for view your account : 
        <a href='".$path['account']."'>Your account</a>
        <br/><br/>
        <em>*Do not reply to this e-mail.<br/><br/>Thank you!</em>
        </body>
        </html>
      ";

      $O_bodyAdmin = " <!DOCTYPE html>
        <html>
        <head>
        <title></title>
        </head>
        <body>
        <span style='font-size:12px;'><em>*This is a message from Volia</em></span>
        <br/>
        <br/><strong> New account has been successfully created.</strong><br/>
        <br/><br/>
        <b>Name</b> : ".$N_fname."
        <br/><br/>
        <b>E-mail</b> : ".$N_email."
        <br/><br/>      
        <b>Register via</b> : ".$N_provider."
        <br/><br/>
        <em>*Do not reply to this e-mail.<br/><br/>Thank you!</em>
        </body>
        </html>
      ";

      $O_toCust = $N_email;
      $file_json = $global['root-url']."uploads/json/company_profile.json";
      $json = json_decode(file_get_contents($file_json),TRUE);
      $J_Company_email = $json['email1'];
      $O_toCompany = $J_Company_email;

      $O_subjectCust = "Volia Website account registration";
      $O_subjectAdmin = "New account registered";
      //END VARIABLE SEND E-MAIL

      $result_create = $obj_fb->insert_data($N_fname, $N_email, $N_provider);
      //var_dump($result_create);
      if($result_create){
        $sentCust = smtpmailer($O_toCust, "info@volia.id", "Volia System", $O_bodyCust, $O_subjectCust); 
        $sentAdmin = smtpmailer($O_toCompany, "info@volia.id", "Volia System", $O_bodyAdmin, $O_subjectAdmin);
        $location = $path['account'];
      }else if(!$result_create){
        $location = $path['home'];
      }
    }
  }
  header("Location:".$location);
  $obj_con->down();
} 
else{
  $loginUrl = $helper->getLoginUrl(array('scope'=>$scope));
  header("Location: ".$loginUrl);
}
?>