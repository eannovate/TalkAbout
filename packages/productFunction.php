<?php
//function recursive for product detail breadcrumb in product-detail.php
function product_detail_breadcrumb($category_id, $path_parent, $path_child){
    require_once("model/Connection.php");
    $obj_con = new Connection();
    $obj_con->up();

    $query = "SELECT category_ID, category_name, category_parentID FROM T_CATEGORY WHERE category_ID = '$category_id'";
    $sql = mysql_query($query);
    $row = mysql_fetch_assoc($sql);
    $name = $row['category_name'];
    if($row['category_parentID'] == 0){
        $url = $path_parent.encode($row['category_name'])."_".$row['category_ID'].".html";
        echo "<a href='".$url."'>".$name."</a><i class='glyphicon glyphicon-menu-right'></i>";
    }else{
        $url = $path_child."?page=1&id=".$row['category_ID'];
        echo product_detail_breadcrumb($row['category_parentID'], $path_parent, $path_child)."<a href='".$url."'>".$name."</a><i class='glyphicon glyphicon-menu-right'></i>";
    }

    $obj_con->down();
}
?>