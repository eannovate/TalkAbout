<script type="text/javascript">
	function createAddress(){
		if( validateFormAddress() ){
			$("#btn-address").text("Loading...");

			var url = "<?=$api['address-insert'];?>";
			var n_customer = $("#input-customer").val();
			var n_name = $("#input-name").val();
			var n_address = $("#input-address").val();
			var n_state = $("#input-state").val();
			var n_statename = $("#input-statename").val();
			var n_city = $("#input-city").val();
			var n_cityname = $("#input-cityname").val();
			var n_zip = $("#input-zip").val();
			var n_country = $("#input-country").val();
			var resultHTML = "";

			var data = {
				customer : n_customer,
				name : n_name,
				address : n_address,
				cityid : n_city,
				city : n_cityname,
				stateid : n_state,
				state : n_statename,
				zip : n_zip,
				country : n_country,
			}
			$.ajax({url: url,data : data, success:function(result){
				if(result != 0){
					$('#modal-address').modal('hide');
					$("#btn-address").text("Create Address");
					resultHTML += "<div class='col-sm-6 col-md-4'>";
					resultHTML += "<div class='radio'>";
					resultHTML += "<label style='display:block;'>";
					resultHTML += "<input type='radio' class='radio-address' name='address' checked='false' value='"+result+"' data-city='"+n_city+"'>";
					resultHTML += "<div class='thumbnail thumbnail-address'>";
					resultHTML += "<address>";
					resultHTML += "<strong style='text-transform:uppercase;'>"+n_name+"</strong><br>";
					resultHTML += n_address+"<br>";
					resultHTML += n_cityname+". "+n_statename+".<br>";
					resultHTML += n_country+". "+n_zip+".";                      
					resultHTML += "</address>";
					resultHTML += "</div>";
					resultHTML += "</label>";
					resultHTML += "</div>";
					resultHTML += "</div>";
					$('#panel-address').prepend(resultHTML);
					$("#po-address").val(result);
					$("#po-cityid").val(n_city);
					window.location.href = "<?=$path['checkout'];?>";
				}
			}});
		}
	}
	function countOngkir(service,cityname){
		var destination = $("#po-cityid").val();
		var sumweight = $("#po-weight").val();
		var weight = eval(sumweight) * 1000;
		var promo_active = "true";//activate promo function
		if(service != ""){
			$("#btn-checkout").hide();
			$("#table-ship").text("Loading...");
			$.ajax({
				url:'<?=$api['ongkir-count'];?>&destination='+destination+'&weight='+weight+'&service='+service,
				dataType:'json',
				success:function(response){
					if(response != 0){
						$("#ship-row").show();
						$(".btn-checkout").show();
						$("#po-shipping").val(response);
						var shipping_price = convertToRupiah(response);
						$("#table-ship").text(shipping_price);
						checkTotal();
						// checkOngkirPromo(promo_active,subtotal,service,cityname,response);
					} else {
						$(".btn-checkout").hide(); 
						$("#po-shipping").val(0);
						$("#po-shipping").val("no");
						$("#ship-row").show();
						$("#table-ship").text("No Service");
						checkTotal();
					}
				},
				error:function(){
					$(".btn-checkout").hide(); 
					$("#po-shipping").val(0);
					$("#po-shipping").val("no");
					$("#ship-row").hide();
					checkTotal();
				}
			});
		} else {
			$(".btn-checkout").hide(); 
			$("#po-shipping").val(0);
			$("#po-shipping").val("no");
			$("#ship-row").hide();
			checkTotal();
		}
	}
	function checkTotal(){
		var promo_active = $("#promo-active").val();
		var subtotal = $("#po-subtotal").val();
		var ship = $("#po-shipping").val();
		var disc_cost = $("#po-disc").val();
		var cityname = $("#po-cityname").val();
		var valid_city = checkPromoCity(cityname);
		var service = $("#po-service").val();
		var promo = 500000;//promo price
		if(promo_active == "true"){
			if(subtotal >= promo){
				if(valid_city != 0){
					if(service == "REG"){
						var totalprice = eval(subtotal) - eval(disc_cost);
						var formatprice = convertToRupiah(totalprice);
						$("#po-promo").val("yes");
						$("#table-ship").text("Promo Free Shipping");
						$("#table-total").text(formatprice);
					} else {
						var shipping_price = convertToRupiah(ship);
						var totalprice = eval(subtotal) + eval(ship);
						var finalprice = eval(totalprice) - eval(disc_cost);
						var formatprice = convertToRupiah(finalprice);
						$("#po-promo").val("no");
						$("#table-total").text(formatprice);
					}
				} else {
					var shipping_price = convertToRupiah(ship);
					var totalprice = eval(subtotal) + eval(ship);
					var finalprice = eval(totalprice) - eval(disc_cost);
					var formatprice = convertToRupiah(finalprice);
					$("#po-promo").val("no");
					$("#table-total").text(formatprice);
				}
			} else {
				var shipping_price = convertToRupiah(ship);
				var totalprice = eval(subtotal) + eval(ship);
				var finalprice = eval(totalprice) - eval(disc_cost);
				var formatprice = convertToRupiah(finalprice);
				$("#po-promo").val("no");
				$("#table-total").text(formatprice);
			}
		} else {
			var shipping_price = convertToRupiah(ship);
			var totalprice = eval(subtotal) + eval(ship);
			var finalprice = eval(totalprice) - eval(disc_cost);
			var formatprice = convertToRupiah(finalprice);
			$("#po-promo").val("no");
			$("#table-total").text(formatprice);
		}
	}
	function loadProvince(){
		$.ajax({
			url:'<?=$api['ongkir-province'];?>',
			dataType:'json',
			success:function(response){
				$("#input-state").html("");
				province = '';
				$("#input-state").append("<option value=''>--Choose Province--</option>");
				$.each(response['rajaongkir']['results'], function(i,n){
					province = '<option value="'+n['province_id']+'">'+n['province']+'</option>';
					province = province + '';
					$("#input-state").append(province);
				});
			},
			error:function(){
				$("#input-state").html('ERROR');
			}
		});
	}
	function loadCity(provinces){
		$.ajax({
			url:'<?=$api['ongkir-city'];?>&province_id='+provinces,
			dataType:'json',
			success:function(response){
				$("#input-city").html("");
				var province = '';
				$("#input-city").append("<option value=''>--Choose City--</option>");
				$.each(response['rajaongkir']['results'], function(i,n){
					province = '<option value="'+n['city_id']+'">'+n['city_name']+'</option>';
					province = province + '';
					$("#input-city").append(province);
				});
			},
			error:function(){
				$("#input-city").html('ERROR');
			}
		});
	}
	$("#btn-voucher").click(function(){
	    var voucher = $("#input-voucher").val();
	    var subtotal = $("#po-subtotal").val();
	    if($("#input-voucher").hasClass("voucher-submit")){
	        //remove code
	        resetVoucher();
	        $(".alert-voucher").hide();
	        $(".alert-voucher").text("");
	        $("#input-voucher").removeClass("voucher-submit");
	        $("#btn-voucher").removeClass("btn-danger");
	        $("#btn-voucher").addClass("btn-info");
	        $("#btn-voucher").html("Apply Voucher");
	        $("#input-voucher").val("");
	    } else {
	        if(voucher != ""){
	            //submit code
	            $.ajax({
	                url: '<?=$api['get-coupon'];?>&coupon='+voucher,
	                dataType:'json',
	                success:function(response){
	                    if(response != 0){
	                        if(response.status != "400"){
	                            var coupon_id = response.data[0].coupon_id;
	                            var coupon_code = response.data[0].coupon_code;
	                            var coupon_disc = response.data[0].coupon_disc;
	                            var coupon_min = response.data[0].coupon_price;
	                            if(eval(subtotal) >= eval(coupon_min)){
	                                $("#disc-row").show();
	                                $("#price-disc").text("-"+convertToRupiah(coupon_disc));
	                                $("#po-couponid").val(coupon_id);
	                                $("#po-coupon").val(coupon_code);
	                                $("#po-disc").val(coupon_disc);
	                                checkTotal();
	                                $(".alert-voucher").show();
	                                $(".alert-voucher").addClass('green');
	                                $(".alert-voucher").text("Congratulation your voucher is valid!");
	                            } else {
	                                resetVoucher();
	                                $(".alert-voucher").show();
	                                $(".alert-voucher").removeClass('green');
	                                $(".alert-voucher").text("Sorry voucher code not meet the minimum purchase");
	                            }
	                        } else {
	                            resetVoucher();
	                            $(".alert-voucher").show();
	                            $(".alert-voucher").removeClass('green');
	                            $(".alert-voucher").text("Voucher is not valid!");
	                        }
	                    } else {
	                        resetVoucher();
	                        $(".alert-voucher").show();
	                        $(".alert-voucher").removeClass('green');
	                        $(".alert-voucher").text("Voucher is not valid!");
	                    }
	                },
	                error:function(){
	                    resetVoucher();
	                    $(".alert-voucher").show();
	                    $(".alert-voucher").removeClass('green');
	                    $(".alert-voucher").text("Voucher is not valid!");
	                }
	            });
	            $(".alert-voucher").hide();
	            $(".alert-voucher").text("");
	            $("#input-voucher").addClass("voucher-submit");
	            $("#btn-voucher").addClass("btn-danger");
	            $("#btn-voucher").removeClass("btn-info");
	            $("#btn-voucher").html("Remove");
	        } else {
	            $(".alert-voucher").show();
	            $(".alert-voucher").removeClass('green');
	            $(".alert-voucher").text("Please insert voucher code to proceed");
	        }
	    }
	});
	function resetVoucher(){
	    $("#disc-row").hide();
	    $("#price-disc").text("-");
	    $("#po-couponid").val("");
	    $("#po-coupon").val("");
	    $("#po-disc").val("0");
	    checkTotal();
	}
</script>