<?php 
$global['absolute-url'] = "https://www.eannovate.com/dev/talkabout/";
$global['root-url'] = $_SERVER['DOCUMENT_ROOT']."/dev/talkabout/";
//$global['snap-js'] = "https://app.sandbox.veritrans.co.id/snap/snap.js"; //use this for develop
$global['snap-js'] = "https://app.midtrans.com/snap/snap.js"; //use this for production
$global['api'] = $global['absolute-url']."api/";
$global['absolute-url-captcha'] = $global['absolute-url']."packages/captcha/captcha.php";
$global['absolute-url-admin'] = $global['absolute-url']."admin/";
$global['path-head'] = "packages/head.php";
$global['path-config'] = "packages/front_config.php";
$global['favicon'] = $global['absolute-url']."img/icon/favicon.ico";
$global['logo-mobile'] = $global['absolute-url']."img/logo/volia.png";
$global['logo-desktop'] = $global['absolute-url']."img/logo/volia.png";
$global['logo-white'] = $global['absolute-url']."img/logo/volia.png";
$global['logo-footer'] = $global['absolute-url']."img/logo/volia-footer2.png";
$seo['company-name'] = "A They Talk About";
$global['privacy'] = "Privacy & Security | Terms & Conditions.";
$global['copyright'] = date('Y')." Copyright Volia. All Rights Reserved.";
$global['engineered'] = "Engineered by <a href='https://www.eannovate.com/' target='_blank' style='color: #555;'>EANNOVATE</a>";
$seo['robot_yes'] = "index, follow";
$seo['robot_no'] = "noindex, nofollow";
//config for smtp email
$smtp['url'] = "www.eannovate.com";
$smtp['to'] = "dean11.cliff@gmail.com";
$smtp['from'] = "developer@eannovate.com";
$smtp['password'] = "Developer88";

$path['home'] = $global['absolute-url'];
$path['contact'] = $global['absolute-url']."contact.html";
$path['about'] = $global['absolute-url']."about.html";
$path['videos'] = $global['absolute-url']."videos.html";
$path['terms'] = $global['absolute-url']."terms-condition.html";
$path['faq'] = $global['absolute-url']."faq.html";
$path['return'] = $global['absolute-url']."return.html";
$path['how'] = $global['absolute-url']."how-to-order.html";
$path['payment'] = $global['absolute-url']."payment-method.html";
$path['shipping'] = $global['absolute-url']."shipping.html";
$path['search'] = $global['absolute-url']."search.html";
$path['login'] = $global['absolute-url']."login.html";
$path['cart'] = $global['absolute-url']."cart.html";
$path['checkout'] = $global['absolute-url']."checkout.html";
$path['confirm-payment'] = $global['absolute-url']."confirm-payment.html";
$path['confirm-success'] = $global['absolute-url']."confirm-success/";
$path['404'] = $global['absolute-url']."404.html";
$path['account'] = $global['absolute-url']."account.html";
$path['wishlist'] = $global['absolute-url']."wishlist.html";
$path['address'] = $global['absolute-url']."address.html";
$path['password'] = $global['absolute-url']."password.html";
$path['reset-password'] = $global['absolute-url']."reset-password/";
$path['history'] = $global['absolute-url']."order-history/";
$path['history-detail'] = $global['absolute-url']."order-detail/";
$path['logout'] = $global['absolute-url']."?action=logout";
$path['product'] = $global['absolute-url']."product/";
$path['new-arrival'] = $global['absolute-url']."new-arrival/";
$path['product-detail'] = $global['absolute-url']."detail/";
$path['thank'] = $global['absolute-url']."thank-you/";
$path['tracking'] = $global['absolute-url']."tracking.html";
$path['midtrans-finish'] = $global['absolute-url']."thank.php";
$path['midtrans-unfinish'] = $global['absolute-url']."payment-unfinish.php";
$path['midtrans-error'] = $global['absolute-url']."payment-failed.php";

$seo['slogan'] = "Your Beauty Destination";

//seo tracking
$seo['title-tracking'] = $seo['company-name']." | Tracking Resi";
$seo['keyword-tracking'] = "";
$seo['desc-tracking'] = "";

//seo home
$seo['title-home'] = $seo['company-name']." | ".$seo['slogan'];
$seo['keyword-home'] = "";
$seo['desc-home'] = "";

//seo about
$seo['title-about'] = $seo['company-name']." | About Us";
$seo['keyword-about'] = "";
$seo['desc-about'] = "";

//seo videos
$seo['title-videos'] = $seo['company-name']." | Videos";
$seo['keyword-videos'] = "";
$seo['desc-videos'] = "";

//seo terms condition
$seo['title-terms'] = $seo['company-name']." | Terms Condition";
$seo['keyword-terms'] = "";
$seo['desc-terms'] = "";

//seo product
$seo['title-product'] = $seo['company-name']." | Product";
$seo['keyword-product'] = "";
$seo['desc-product'] = "";

//seo new arrival
$seo['title-new'] = $seo['company-name']." | New Arrival";
$seo['keyword-new'] = "";
$seo['desc-new'] = "";

//seo contact
$seo['title-contact'] = $seo['company-name']." | Contact Us";
$seo['keyword-contact'] = "";
$seo['desc-contact'] = "";

//seo faq
$seo['title-faq'] = $seo['company-name']." | FAQ";
$seo['keyword-faq'] = "";
$seo['desc-faq'] = "";

//seo return
$seo['title-return'] = $seo['company-name']." | Return & Exchange Policy";
$seo['keyword-return'] = "";
$seo['desc-return'] = "";

//seo shipping
$seo['title-shipping'] = $seo['company-name']." | Shipping & Delivery";
$seo['keyword-shipping'] = "";
$seo['desc-shipping'] = "";

//seo payment
$seo['title-payment'] = $seo['company-name']." | Payment Method";
$seo['keyword-payment'] = "";
$seo['desc-payment'] = "";

//seo how
$seo['title-how'] = $seo['company-name']." | How To Order";
$seo['keyword-how'] = "";
$seo['desc-how'] = "";

//seo cart
$seo['title-cart'] = $seo['company-name']." | My Cart";
$seo['keyword-cart'] = "";
$seo['desc-cart'] = "";

//seo checkout
$seo['title-checkout'] = $seo['company-name']." | Checkout";
$seo['keyword-checkout'] = "";
$seo['desc-checkout'] = "";

//seo search
$seo['title-search'] = $seo['company-name']." | Search For ";
$seo['keyword-search'] = "";
$seo['desc-search'] = "";

//seo login
$seo['title-login'] = $seo['company-name']." | Login & Register ";
$seo['keyword-login'] = "";
$seo['desc-login'] = "";

//seo confirm payment
$seo['title-confirm-payment'] = $seo['company-name']." | Confirm Payment ";
$seo['keyword-confirm-payment'] = "";
$seo['desc-confirm-payment'] = "";

//seo 404
$seo['title-404'] = "Page Not Found 404";
$seo['keyword-404'] = "";
$seo['desc-404'] = "";

//seo account
$seo['title-account'] = $seo['company-name']." | My Account ";
$seo['keyword-account'] = "";
$seo['desc-account'] = "";

//seo address
$seo['title-address'] = $seo['company-name']." |  Address";
$seo['keyword-address'] = "";
$seo['desc-address'] = "";

//seo password
$seo['title-password'] = $seo['company-name']." | Password";
$seo['keyword-password'] = "";
$seo['desc-password'] = "";

//seo history
$seo['title-history'] = $seo['company-name']." | Order History";
$seo['keyword-history'] = "";
$seo['desc-history'] = "";

//seo produt detail
$seo['title-product-detail'] = $seo['company-name']." | Product";
$seo['keyword-product-detail'] = "";
$seo['desc-product-detail'] = "";

//seo thank
$seo['title-thank'] = $seo['company-name']." | Thank You For Purchase";
$seo['keyword-thank'] = "";
$seo['desc-thank'] = "";

//seo sitemap
$seo['title-sitemap'] = $seo['company-name']." | Sitemap";
$seo['keyword-sitemap'] = "";
$seo['desc-sitemap'] = "";

//third party
$path['login-with-facebook'] = $global['absolute-url']."src/login-with-facebook.php";
$path['login-with-google'] = $global['absolute-url']."src/login-with-google.php";
?>