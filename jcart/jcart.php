<?php

// jCart v1.3
// http://conceptlogic.com/jcart/

//error_reporting(E_ALL);

// Cart logic based on Webforce Cart: http://www.webforcecart.com/
class Jcart {

	public $config     = array();
	private $items     = array();
	private $names     = array();
	private $weights   = array();
	private $prices    = array();
	private $secprices = array();
	private $qtys      = array();
	private $urls      = array();
	private $images    = array();
	private $subtotal  = 0;
	private $itemCount = 0;

	function __construct() {

		// Get $config array
		include_once('config-loader.php');
		$this->config = $config;
	}

	/**
	* Get cart contents
	*
	* @return array
	*/
	public function get_contents() { 
		$items = array();
		foreach($this->items as $tmpItem) {
			$item = null;
			$item['id']       = $tmpItem;
			$item['name']     = $this->names[$tmpItem];
			$item['weight']   = $this->weights[$tmpItem];
			$item['price']    = $this->prices[$tmpItem];
			$item['secprice'] = $this->secprices[$tmpItem];
			$item['qty']      = $this->qtys[$tmpItem];
			$item['url']      = $this->urls[$tmpItem];
			$item['image']    = $this->images[$tmpItem];
			$item['subtotal'] = $item['price'] * $item['qty'];
			$items[]          = $item;
		}
		return $items;
	}

	/**
	* Add an item to the cart
	*
	* @param string $id
	* @param string $name
	* @param float $price
	* @param mixed $qty
	* @param string $url
	* @param string $image
	* @param string $weight
	*
	* @return mixed
	*/
	private function add_item($id, $name, $price, $secprice, $qty = 1, $url, $image, $weight) {

		$validPrice = false;
		$validQty = false;

		// Verify the price is numeric
		if (is_numeric($price)) {
			$validPrice = true;
		}

		// If decimal quantities are enabled, verify the quantity is a positive float
		if ($this->config['decimalQtys'] === true && filter_var($qty, FILTER_VALIDATE_FLOAT) && $qty > 0) {
			$validQty = true;
		}
		// By default, verify the quantity is a positive integer
		elseif (filter_var($qty, FILTER_VALIDATE_INT) && $qty > 0) {
			$validQty = true;
		}


		// Add the item
		if ($validPrice !== false && $validQty !== false) {

			
				// If the item is already in the cart, increase its quantity
				if($this->qtys[$id] > 0) {
					$this->qtys[$id] += $qty;
					$this->update_subtotal();
				}
				// This is a new item
				else {
					$this->items[]     		= $id;
					$this->names[$id]  		= $name;
					$this->weights[$id]   	= $weight;
					$this->prices[$id] 		= $price;
					$this->secprices[$id] 	= $secprice;
					$this->qtys[$id]   		= $qty;
					$this->urls[$id]   		= $url;
					$this->images[$id]   	= $image;
				}
			$this->update_subtotal();
			return true;
		}
		elseif ($validPrice !== true) {
			$errorType = 'price';
			return $errorType;
		}
		elseif ($validQty !== true) {
			$errorType = 'qty';
			return $errorType;
		}
	}

	/**
	* Update an item in the cart
	*
	* @param string $id
	* @param mixed $qty
	*
	* @return boolean
	*/
	private function update_item($id, $qty) {

		// If the quantity is zero, no futher validation is required
		if ((int) $qty === 0) {
			$validQty = true;
		}
		// If decimal quantities are enabled, verify it's a float
		elseif ($this->config['decimalQtys'] === true && filter_var($qty, FILTER_VALIDATE_FLOAT)) {
			$validQty = true;
		}
		// By default, verify the quantity is an integer
		elseif (filter_var($qty, FILTER_VALIDATE_INT))	{
			$validQty = true;
		}

		// If it's a valid quantity, remove or update as necessary
		if ($validQty === true) {
			if($qty < 1) {
				$this->remove_item($id);
			}
			else {
				$this->qtys[$id] = $qty;
			}
			$this->update_subtotal();
			return true;
		}
	}

	/* Using post vars to remove items doesn't work because we have to pass the
	id of the item to be removed as the value of the button. If using an input
	with type submit, all browsers display the item id, instead of allowing for
	user-friendly text. If using an input with type image, IE does not submit
	the	value, only x and y coordinates where button was clicked. Can't use a
	hidden input either since the cart form has to encompass all items to
	recalculate	subtotal when a quantity is changed, which means there are
	multiple remove	buttons and no way to associate them with the correct
	hidden input. */

	/**
	* Reamove an item from the cart
	*
	* @param string $id	*
	*/
	private function remove_item($id) {
		$tmpItems = array();

		unset($this->names[$id]);
		unset($this->weights[$id]);
		unset($this->prices[$id]);
		unset($this->secprices[$id]);
		unset($this->qtys[$id]);
		unset($this->urls[$id]);
		unset($this->images[$id]);

		// Rebuild the items array, excluding the id we just removed
		foreach($this->items as $item) {
			if($item != $id) {
				$tmpItems[] = $item;
			}
		}
		$this->items = $tmpItems;
		$this->update_subtotal();
	}

	/**
	* Empty the cart
	*/
	public function empty_cart() {
		$this->items     	= array();
		$this->names     	= array();
		$this->weights   	= array();
		$this->prices    	= array();
		$this->secprices    = array();
		$this->qtys      	= array();
		$this->urls      	= array();
		$this->images    	= array();
		$this->subtotal  	= 0;
		$this->itemCount 	= 0;
	}

	/**
	* Update the entire cart
	*/
	public function update_cart() {

		// Post value is an array of all item quantities in the cart
		// Treat array as a string for validation
		if (is_array($_POST['jcartItemQty'])) {
			$qtys = implode($_POST['jcartItemQty']);
		}

		// If no item ids, the cart is empty
		if ($_POST['jcartItemId']) {

			$validQtys = false;

			// If decimal quantities are enabled, verify the combined string only contain digits and decimal points
			if ($this->config['decimalQtys'] === true && preg_match("/^[0-9.]+$/i", $qtys)) {
				$validQtys = true;
			}
			// By default, verify the string only contains integers
			elseif (filter_var($qtys, FILTER_VALIDATE_INT) || $qtys == '') {
				$validQtys = true;
			}

			if ($validQtys === true) {

				// The item index
				$count = 0;

				// For each item in the cart, remove or update as necessary
				foreach ($_POST['jcartItemId'] as $id) {

					$qty = $_POST['jcartItemQty'][$count];

					if($qty < 1) {
						$this->remove_item($id);
					}
					else {
						$this->update_item($id, $qty);
					}

					// Increment index for the next item
					$count++;
				}
				return true;
			}
		}
		// If no items in the cart, return true to prevent unnecssary error message
		elseif (!$_POST['jcartItemId']) {
			return true;
		}
	}

	/**
	* Recalculate subtotal
	*/
	private function update_subtotal() {
		$this->itemCount = 0;
		$this->subtotal  = 0;

		if(sizeof($this->items > 0)) {
			foreach($this->items as $item) {
				$this->subtotal += ($this->qtys[$item] * $this->prices[$item]);

				// Total number of items
				$this->itemCount += $this->qtys[$item];
			}
		}
	}

	public function get_total(){
		return $this->itemCount;
	}

	public function get_total_weight(){
		if($this->itemCount > 0) {
			foreach($this->get_contents() as $item)	{
				$total_weight = $item['weight'] * $item['qty'];
				$sum_weight = $total_weight + $sum_weight;
			}
			echo $sum_weight;
		}
	}
	/**
	* Process and show total in header
	*/
	public function relay_cart(){
		$config = $this->config; 
		$errorMessage = null;

		// Simplify some config variables
		$checkout = $config['checkoutPath'];
		$priceFormat = $config['priceFormat'];

		$id    		= $config['item']['id'];
		$name  		= $config['item']['name'];
		$weight   	= $config['item']['weight'];
		$price 		= $config['item']['price'];
		$secprice 	= $config['item']['secprice'];
		$qty   		= $config['item']['qty'];
		$url   		= $config['item']['url'];
		$image   	= $config['item']['image'];
		$add   		= $config['item']['add'];

		// Use config values as literal indices for incoming POST values
		// Values are the HTML name attributes set in config.json
		$id    		= $_POST[$id];
		$name  		= $_POST[$name];
		$weight   	= $_POST[$weight];
		$price 		= $_POST[$price];
		$secprice 	= $_POST[$secprice];
		$qty   		= $_POST[$qty];
		$url   		= $_POST[$url];
		$image   	= $_POST[$image];

		// Optional CSRF protection, see: http://conceptlogic.com/jcart/security.php
		$jcartToken = $_POST['jcartToken'];

		// Only generate unique token once per session
		if(!$_SESSION['jcartToken']){
			$_SESSION['jcartToken'] = md5(session_id() . time() . $_SERVER['HTTP_USER_AGENT']);
		}
		// If enabled, check submitted token against session token for POST requests
		if ($config['csrfToken'] === 'true' && $_POST && $jcartToken != $_SESSION['jcartToken']) {
			$errorMessage = 'Invalid token!' . $jcartToken . ' / ' . $_SESSION['jcartToken'];
		}

		// Sanitize values for output in the browser
		$id    = filter_var($id, FILTER_SANITIZE_SPECIAL_CHARS, FILTER_FLAG_STRIP_LOW);
		$name  = filter_var($name, FILTER_SANITIZE_SPECIAL_CHARS, FILTER_FLAG_STRIP_LOW);
		$url   = filter_var($url, FILTER_SANITIZE_URL);

		// Round the quantity if necessary
		if($config['decimalPlaces'] === true) {
			$qty = round($qty, $config['decimalPlaces']);
		}

		// Add an item
		if ($_POST[$add]) {
			$itemAdded = $this->add_item($id, $name, $price, $secprice, $qty, $url, $image, $weight);
			// If not true the add item function returns the error type
			if ($itemAdded !== true) {
				$errorType = $itemAdded;
				switch($errorType) {
					case 'qty':
						$errorMessage = $config['text']['quantityError'];
						break;
					case 'price':
						$errorMessage = $config['text']['priceError'];
						break;
				}
			}
		}

		// Update a single item
		if ($_POST['jcartUpdate']) {
			$itemUpdated = $this->update_item($_POST['itemId'], $_POST['itemQty']);
			if ($itemUpdated !== true)	{
				$errorMessage = $config['text']['quantityError'];
			}
		}

		// Update all items in the cart
		if($_POST['jcartUpdateCart'] || $_POST['jcartCheckout'])	{
			$cartUpdated = $this->update_cart();
			if ($cartUpdated !== true)	{
				$errorMessage = $config['text']['quantityError'];
			}
		}
		
		// Remove an item
		/* After an item is removed, its id stays set in the query string,
		preventing the same item from being added back to the cart in
		subsequent POST requests.  As result, it's not enough to check for
		GET before deleting the item, must also check that this isn't a POST
		request. */
		if($_GET['jcartRemove'] && !$_POST) {
			$this->remove_item($_GET['jcartRemove']);
		}

		// Empty the cart
		if($_POST['jcartEmpty']) {
			$this->empty_cart();
		}

		// Determine which text to use for the number of items in the cart
		$itemsText = $config['text']['multipleItems'];
		if ($this->itemCount == 1) {
			$itemsText = $config['text']['singleItem'];
		}

		// If this error is true the visitor updated the cart from the checkout page using an invalid price format
		// Passed as a session var since the checkout page uses a header redirect
		// If passed via GET the query string stays set even after subsequent POST requests
		if ($_SESSION['quantityError'] === true) {
			$errorMessage = $config['text']['quantityError'];
			unset($_SESSION['quantityError']);
		}

		////////////////////////////////////////////////////////////////////////
		// Output the cart

		// If any items in the cart
		if($this->itemCount > 0) {
			echo $this->itemCount;
		} else {
			echo "0";
		}

	}
	/**
	* end Process and show total in header
	*/

	/**
	* Process and show total in header
	*/
	public function show_bag(){
		$config = $this->config;

		// Optional CSRF protection, see: http://conceptlogic.com/jcart/security.php
		if(isset($_POST['jcartToken'])){
			$jcartToken = $_POST['jcartToken'];
		} else {
			$jcartToken = "";
		}

		// Only generate unique token once per session
		if(!$_SESSION['jcartToken']){
			$_SESSION['jcartToken'] = md5(session_id() . time() . $_SERVER['HTTP_USER_AGENT']);
		}
		// If enabled, check submitted token against session token for POST requests
		if ($config['csrfToken'] === 'true' && $_POST && $jcartToken != $_SESSION['jcartToken']) {
			$errorMessage = 'Invalid token!' . $jcartToken . ' / ' . $_SESSION['jcartToken'];
		}

		// If any items in the cart
		if($this->itemCount > 0) {
			echo $this->itemCount;
		} else {
			echo "0";
		}

	}
	/**
	* end Process and show total in header
	*/

	/**
	* Process and table cart
	*/
	public function table_cart(){
		$config = $this->config; 
		$errorMessage = null;

		// Simplify some config variables
		$checkout = $config['checkoutPath'];
		$priceFormat = $config['priceFormat'];

		$id    		= $config['item']['id'];
		$name  		= $config['item']['name'];
		$weight   	= $config['item']['weight'];
		$price 		= $config['item']['price'];
		$secprice 	= $config['item']['secprice'];
		$qty   		= $config['item']['qty'];
		$url   		= $config['item']['url'];
		$image   	= $config['item']['image'];
		$add   		= $config['item']['add'];

		// Use config values as literal indices for incoming POST values
		// Values are the HTML name attributes set in config.json
		$id    		= $_POST[$id];
		$name  		= $_POST[$name];
		$weight   	= $_POST[$weight];
		$price 		= $_POST[$price];
		$secprice 	= $_POST[$secprice];
		$qty   		= $_POST[$qty];
		$url   		= $_POST[$url];
		$image   	= $_POST[$image];

		// Optional CSRF protection, see: http://conceptlogic.com/jcart/security.php
		$jcartToken = $_POST['jcartToken'];

		// Only generate unique token once per session
		if(!$_SESSION['jcartToken']){
			$_SESSION['jcartToken'] = md5(session_id() . time() . $_SERVER['HTTP_USER_AGENT']);
		}
		// If enabled, check submitted token against session token for POST requests
		if ($config['csrfToken'] === 'true' && $_POST && $jcartToken != $_SESSION['jcartToken']) {
			$errorMessage = 'Invalid token!' . $jcartToken . ' / ' . $_SESSION['jcartToken'];
		}

		// Sanitize values for output in the browser
		$id    = filter_var($id, FILTER_SANITIZE_SPECIAL_CHARS, FILTER_FLAG_STRIP_LOW);
		$name  = filter_var($name, FILTER_SANITIZE_SPECIAL_CHARS, FILTER_FLAG_STRIP_LOW);
		$url   = filter_var($url, FILTER_SANITIZE_URL);

		// Round the quantity if necessary
		if($config['decimalPlaces'] === true) {
			$qty = round($qty, $config['decimalPlaces']);
		}

		// Add an item
		if ($_POST[$add]) {
			$itemAdded = $this->add_item($id, $name, $price, $secprice, $qty, $url, $image, $weight);
			// If not true the add item function returns the error type
			if ($itemAdded !== true) {
				$errorType = $itemAdded;
				switch($errorType) {
					case 'qty':
						$errorMessage = $config['text']['quantityError'];
						break;
					case 'price':
						$errorMessage = $config['text']['priceError'];
						break;
				}
			}
		}

		// Update a single item
		if ($_POST['jcartUpdate']) {
			$itemUpdated = $this->update_item($_POST['itemId'], $_POST['itemQty']);
			if ($itemUpdated !== true)	{
				$errorMessage = $config['text']['quantityError'];
			}
		}

		// Update all items in the cart
		if($_POST['jcartUpdateCart'] || $_POST['jcartCheckout'])	{
			$cartUpdated = $this->update_cart();
			if ($cartUpdated !== true)	{
				$errorMessage = $config['text']['quantityError'];
			}
		}

		// Remove an item
		/* After an item is removed, its id stays set in the query string,
		preventing the same item from being added back to the cart in
		subsequent POST requests.  As result, it's not enough to check for
		GET before deleting the item, must also check that this isn't a POST
		request. */
		if($_GET['jcartRemove'] && !$_POST) {
			$this->remove_item($_GET['jcartRemove']);
		}

		// Empty the cart
		if($_POST['jcartEmpty']) {
			$this->empty_cart();
		}

		// Determine which text to use for the number of items in the cart
		$itemsText = $config['text']['multipleItems'];
		if ($this->itemCount == 1) {
			$itemsText = $config['text']['singleItem'];
		}

		// Determine if this is the checkout page
		/* First we check the request uri against the config checkout (set when
		the visitor first clicks checkout), then check for the hidden input
		sent with Ajax request (set when visitor has javascript enabled and
		updates an item quantity). */
		$isCheckout = strpos(request_uri(), $checkout);
		if ($isCheckout !== false || $_REQUEST['jcartIsCheckout'] == 'true') {
			$isCheckout = true;
		}
		else {
			$isCheckout = false;
		}

		// Overwrite the form action to post to gateway.php instead of posting back to checkout page
		if ($isCheckout === true) {

			// Sanititze config path
			$path = filter_var($config['jcartPath'], FILTER_SANITIZE_URL);

			// Trim trailing slash if necessary
			$path = rtrim($path, '/');

			$checkout = $path . '/gateway.php';
		}

		// Default input type
		// Overridden if using button images in config.php
		$inputType = 'submit';

		// If this error is true the visitor updated the cart from the checkout page using an invalid price format
		// Passed as a session var since the checkout page uses a header redirect
		// If passed via GET the query string stays set even after subsequent POST requests
		if ($_SESSION['quantityError'] === true) {
			$errorMessage = $config['text']['quantityError'];
			unset($_SESSION['quantityError']);
		}

		$currencySymbol = 'IDR ';

		////////////////////////////////////////////////////////////////////////
		// Output the cart

		// If there's an error message wrap it in some HTML
		if ($errorMessage)	{
			$errorMessage = "<p id='jcart-error'>$errorMessage</p>";
		}
		// If any items in the cart
		echo "<div class='container container-chev'>";
		echo "<div class='table-cart'>";
		echo "<div class='table-responsive'>";
		echo "<table id='table-cart' class='table table-bordered'>";
		echo "<tr>";
		echo "<th>Image</th>";
		echo "<th>Product Name</th>";
		echo "<th>Quantity</th>";
		echo "<th>Unit Price</th>";
		echo "<th>Total</th>";
		echo "</tr>";
		if($this->itemCount > 0) {
		foreach($this->get_contents() as $item)	{
		echo "<tr id='cart-line-".$item['id']."' class='cart-line'>";
		echo "<td>";
		echo "<a href='".$item['url']."' class='cart-img'>";
		echo "<img src='".$item['image']."' alt='".$item['name']."'>";
		echo "</a>";
		echo "</td>";
		echo "<td>";
		echo "<a href='".$item['url']."' class='cart-title'>";
		echo $item['name'];
		echo "</a>";
		echo "</td>";
		echo "<td>";
		echo "<div class='input-group group-cart'>";
		echo "<input id='tcart-qty-".$item['id']."' type='number' class='form-control cart-qty' min='1' value='".$item['qty']."'>";
		echo "<input id='tcart-token' type='hidden' value='".$_SESSION['jcartToken']."'>";
		echo "<input id='tcart-checkout' type='hidden' name='checkCart' value='true'>";
		echo "<input id='tcart-price-".$item['id']."' type='hidden' name='checkCart' value='".$item['price']."'>";
		echo "<div class='input-group-btn'>";
		echo "<button class='btn btn-primary' onclick='update_cart(\"".$item['id']."\")' title='' data-toggle='tooltip' type='submit' data-original-title='Update'>";
		echo "<i class='fa fa-refresh'></i>";
		echo "</button>";
		echo "<button class='btn btn-danger' onclick='remove_cart(\"".$item['id']."\")' title='' data-toggle='tooltip' type='button' data-original-title='Remove'>";
		echo "<i class='fa fa-times-circle'></i>";
		echo "</button>";
		echo "</div>";
		echo "</div>";
		echo "</td>";
		echo "<td>";
		if($item['secprice'] != 0){
		echo "<div class='pdetail-price marg0' style='font-size: 15px;font-family: JosefinRegular;'>";
		echo "<span class='old' style='width: 100%;'>".$currencySymbol. number_format($item['secprice'], 0, $priceFormat['dec_point'], $priceFormat['thousands_sep'])."</span>";
		echo "<span class='new'>".$currencySymbol. number_format($item['price'], 0, $priceFormat['dec_point'], $priceFormat['thousands_sep'])."</span>";
		echo "</div>";
		} else {
		echo "<div class='pdetail-price marg0' style='margin: 0;font-size: 15px;font-family: JosefinRegular;'>";
		echo "<span class='price'>".$currencySymbol. number_format($item['price'], 0, $priceFormat['dec_point'], $priceFormat['thousands_sep'])."</span>";
		echo "</div>";
		}
		echo "</td>";
		echo "<td id='price-line-{$item['id']}'>";
		$price_total = $item['price']*$item['qty'];
		echo $currencySymbol. number_format($price_total, 0, $priceFormat['dec_point'], $priceFormat['thousands_sep']);
		echo "</td>";
		echo "</tr>";
		}
		} else {
			echo "<tr><td colspan='5' class='cart-empty'>Your cart is empty!</td></tr>";
		}
		echo "</table>";
		echo "</div>";
		echo "</div>";
		echo "</div>";

	}
	/**
	* end Process and table cart
	*/	

	/**
	* Process and show checkout
	*/
	public function show_checkout(){
		$config = $this->config; 
		$errorMessage = null;

		// Simplify some config variables
		$priceFormat = $config['priceFormat'];

		$id    		= $config['item']['id'];
		$name  		= $config['item']['name'];
		$weight   	= $config['item']['weight'];
		$price 		= $config['item']['price'];
		$secprice 	= $config['item']['secprice'];
		$qty   		= $config['item']['qty'];
		$url   		= $config['item']['url'];
		$image   	= $config['item']['image'];
		$add   		= $config['item']['add'];

		// Use config values as literal indices for incoming POST values
		// Values are the HTML name attributes set in config.json
		$id    		= $_POST[$id];
		$name  		= $_POST[$name];
		$weight   	= $_POST[$weight];
		$price 		= $_POST[$price];
		$secprice 	= $_POST[$secprice];
		$qty   		= $_POST[$qty];
		$url   		= $_POST[$url];
		$image   	= $_POST[$image];

		// Sanitize values for output in the browser
		$id    = filter_var($id, FILTER_SANITIZE_SPECIAL_CHARS, FILTER_FLAG_STRIP_LOW);
		$name  = filter_var($name, FILTER_SANITIZE_SPECIAL_CHARS, FILTER_FLAG_STRIP_LOW);
		$url   = filter_var($url, FILTER_SANITIZE_URL);

		// Round the quantity if necessary
		if($config['decimalPlaces'] === true) {
			$qty = round($qty, $config['decimalPlaces']);
		}


		$currencySymbol = 'IDR ';

		////////////////////////////////////////////////////////////////////////
		// Output the cart
		
		echo "<div class='table-checkout'>";
		echo "<div class='table-responsive'>";
		echo "<table class='table table-bordered table-hover'>";
		echo "<thead>";
		echo "<tr>";
		echo "<td class='text-left'>Product Name</td>";
		echo "<td class='text-center'>Quantity</td>";
		echo "<td class='text-center'>Weight</td>";
		echo "<td class='text-right'>Unit Price</td>";
		echo "<td class='text-right'>Total</td>";
		echo "</tr>";
		echo "</thead>";
		echo "<tbody>";
		if($this->itemCount > 0) {
			foreach($this->get_contents() as $item)	{
				echo "<tr>";
				echo "<td class='text-left'>";
				echo "<a href='".$item['url']."'>".$item['name']."</a>";
				echo "</td>";
				echo "<td class='text-center'>".$item['qty']."</td>";
				$total_weight = $item['weight'] * $item['qty'];
				$sum_weight = $total_weight + $sum_weight;
				echo "<td class='text-center'>".$total_weight." Kg</td>";
				echo "<td class='text-right'>".$currencySymbol. number_format($item['price'], 0, $priceFormat['dec_point'], $priceFormat['thousands_sep'])."</td>";
				$price_total = $item['price']*$item['qty'];
				echo "<td class='text-right'>".$currencySymbol. number_format($price_total, 0, $priceFormat['dec_point'], $priceFormat['thousands_sep']) . "</td>";
				echo "</tr>";
			}
		echo "</tbody>";
		echo "<tfoot>";
		echo "<tr>";
		echo "<td class='text-right' colspan='4'><strong>Sub-Total:</strong></td>";
		echo "<td class='text-right'>$currencySymbol" . number_format($this->subtotal, 0, $priceFormat['dec_point'], $priceFormat['thousands_sep']) . "</td>";
		echo "</tr>";
		echo "<tr id='ship-row' style='display:none;'>";
		echo "<td class='text-right' colspan='4'><strong>Shipping (".$sum_weight.".Kg) :</strong></td>";
		echo "<td id='table-ship' class='text-right'></td>";
		echo "</tr>";
		echo "<tr id='disc-row' style='display:none;'>";
		echo "<td class='text-right' colspan='4'><strong>Discount :</strong></td>";
		echo "<td id='price-disc' class='text-right'></td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td class='text-right' colspan='4'><strong>Total:</strong></td>";
		echo "<td id='table-total' class='text-right'>$currencySymbol" . number_format($this->subtotal, 0, $priceFormat['dec_point'], $priceFormat['thousands_sep']) . "</td>";
		echo "</tr>";
		echo "</tfoot>";
		echo "</table>";
		echo "</div>";
		} else  {
			echo "<tr><td colspan='5' class='cart-empty'>Your cart is empty!</td></tr>";
			echo "</tbody>";
			echo "</table>";
			echo "</div>";
		}
		echo "</div>";
	}
	/**
	* end Process and show checkout
	*/

	/**
	* Process and input checkout
	*/
	public function input_checkout(){
		$config = $this->config; 
		$errorMessage = null;

		$id    		= $config['item']['id'];
		$name  		= $config['item']['name'];
		$weight   	= $config['item']['weight'];
		$price 		= $config['item']['price'];
		$secprice 	= $config['item']['secprice'];
		$qty   		= $config['item']['qty'];
		$url   		= $config['item']['url'];
		$image   	= $config['item']['image'];
		$add   		= $config['item']['add'];

		// Use config values as literal indices for incoming POST values
		// Values are the HTML name attributes set in config.json
		$id    		= $_POST[$id];
		$name  		= $_POST[$name];
		$weight   	= $_POST[$weight];
		$price 		= $_POST[$price];
		$secprice 	= $_POST[$secprice];
		$qty   		= $_POST[$qty];
		$url   		= $_POST[$url];
		$image   	= $_POST[$image];
		
		
		if($this->itemCount > 0) {
			foreach($this->get_contents() as $item)	{
				echo "<div id='fcheck-out-".$item['id']."' style='display:none;'>";
				echo "<input type='hidden' name='product_ID[]' value='".$item['id']."'>";
				echo "<input type='hidden' name='product_name[]' value='".$item['name']."'>";
				echo "<input type='hidden' name='product_qty[]' value='".$item['qty']."'>";
				$total_weight = $item['weight'] * $item['qty'];
				$sum_weight = $total_weight + $sum_weight;
				echo "<input type='hidden' name='product_weight[]' value='".$total_weight."'>";
				$price_total = $item['price']*$item['qty'];
				echo "<input type='hidden' name='product_price[]' value='".$price_total."'>";
				echo "</div>";
			}
			echo "<input type='hidden' id='po-weight' name='po_weight' value='".$sum_weight."'>";
			echo "<input type='hidden' id='po-subtotal' name='po_subtotal' value='".$this->subtotal."'>";
		} 
	}
	/**
	* end Process and input checkout
	*/

}

// Start a new session in case it hasn't already been started on the including page
@session_start();

// Initialize jcart after session start
$jcart = $_SESSION['jcart'];
if(!is_object($jcart)) {
	$jcart = $_SESSION['jcart'] = new Jcart();
}

// Enable request_uri for non-Apache environments
// See: http://api.drupal.org/api/function/request_uri/7
if (!function_exists('request_uri')) {
	function request_uri() {
		if (isset($_SERVER['REQUEST_URI'])) {
			$uri = $_SERVER['REQUEST_URI'];
		}
		else {
			if (isset($_SERVER['argv'])) {
				$uri = $_SERVER['SCRIPT_NAME'] . '?' . $_SERVER['argv'][0];
			}
			elseif (isset($_SERVER['QUERY_STRING'])) {
				$uri = $_SERVER['SCRIPT_NAME'] . '?' . $_SERVER['QUERY_STRING'];
			}
			else {
				$uri = $_SERVER['SCRIPT_NAME'];
			}
		}
		$uri = '/' . ltrim($uri, '/');
		return $uri;
	}
}
?>