<?php
class Brand{

	private $table = "T_BRAND";

//START FUNCTION FOR CLIENT PAGE
	//function get brand 
	public function get_index_sale($id){
		$result = 0;
		$cond_id = "";

		if($id != "" || $id != null){
			$cond_id = " AND product_ref_categoryID IN(".$id.")";
		} else {
			$cond_id = "";
		}

		$text = "SELECT brand_ID, brand_title, COUNT(product_ID)AS total_product FROM T_PRODUCT LEFT JOIN T_BRAND ON product_ref_brandID = brand_ID WHERE product_publish = 'Publish' AND product_specialPrice != 0 $cond_id GROUP BY brand_ID ORDER BY brand_title ASC";
		$query = mysql_query($text);
		$total_data = mysql_num_rows($query);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		if(is_array($result)){
            $result[0]['total_data'] = $total_data;
        }
		return $result;
	}
	public function get_index_category($id){
		$result = 0;
		$cond_id = "";

		if($id != "" || $id != null){
			$cond_id = " AND product_ref_categoryID IN(".$id.")";
		} else {
			$cond_id = "";
		}

		$text = "SELECT brand_ID, brand_title, COUNT(product_ID)AS total_product FROM T_PRODUCT LEFT JOIN T_BRAND ON product_ref_brandID = brand_ID WHERE product_publish = 'Publish' $cond_id GROUP BY brand_ID ORDER BY brand_title ASC";
		$query = mysql_query($text);
		$total_data = mysql_num_rows($query);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		if(is_array($result)){
            $result[0]['total_data'] = $total_data;
        }
		return $result;
	}
	public function get_brand_search(){
		$result = 0;

		$text = "SELECT brand_title, brand_ID FROM $this->table WHERE brand_publish = 'Publish' 
			 ORDER BY brand_title ASC";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		return $result;
	}
	//function get brand by alphabet in part-dropdown_shop.php
	public function get_brand_by_alphabet(){
		$result = 0;

		$text = "SELECT SUBSTRING(brand_title, 1, 1) AS alpha, brand_ID, brand_title FROM $this->table WHERE brand_publish = 'Publish' 
			GROUP BY SUBSTRING(brand_title, 0, 2), brand_title ORDER BY alpha, brand_title ASC";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				//$result[$row['alpha']][] = $row['brand_title'];
				$result[$row['alpha']][] = $row;
			}
		}
		return $result;
	}

	public function get_index(){
		$result = 0;
		$text = "SELECT * FROM $this->table WHERE brand_publish = 'Publish' ORDER BY brand_title ASC";
		$query = mysql_query($text);
		$total_data = mysql_num_rows($query);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		if(is_array($result)){
            $result[0]['total_data'] = $total_data;
        }
		return $result;
	}
//END FUNCTION FOR CLIENT PAGE

//START FUNCTION FOR ADMIN PAGE
	public function get_data(){
		$result = 0;
		$text = "SELECT * FROM $this->table ORDER BY brand_title ASC";
		$query = mysql_query($text);
		$total_data = mysql_num_rows($query);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		if(is_array($result)){
            $result[0]['total_data'] = $total_data;
        }
		return $result;
	}

	public function get_list(){
		$result = 0;
		
		$text = "SELECT * FROM $this->table WHERE brand_publish = 'Publish' ORDER BY brand_title ASC";
		$query = mysql_query($text);
		$total_data = mysql_num_rows($query);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		return $result;
	}
 
	public function get_data_detail($id){
		$result = 0;
		$text = "SELECT * FROM $this->table WHERE brand_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		return $result;
	}

	public function insert_data($title, $img, $img_thmb, $publish){
		$result = 0;
		$text = "INSERT INTO $this->table (brand_title, brand_img, brand_imgThmb, brand_publish, brand_createDate) VALUES('$title', '$img', '$img_thmb', '$publish', NOW())";
		$query = mysql_query($text);
		if($query){
			$result = 1;
		}
		return $result;
	}

	public function update_data($id, $title, $publish){
		$result = 0;
		$text = "UPDATE $this->table SET brand_title = '$title', brand_publish='$publish' WHERE brand_ID = '$id'";
		$query = mysql_query($text);	
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

	public function update_data_image($id, $title, $img, $img_thmb, $publish){
		$result = 0;
		$this->remove_image($id); //remove image before
		$text = "UPDATE $this->table SET brand_title = '$title', brand_img = '$img', brand_imgThmb = '$img_thmb', brand_publish='$publish' WHERE brand_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

	public function delete_data($id){
		$result = 0;
		$this->remove_image($id); //remove image before
		$text = "DELETE FROM $this->table WHERE brand_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

	public function remove_image($id){
		$result = 0;
		$flag_img = 0;
		$flag_img_thmb = 0;
		$text = "SELECT brand_ID, brand_img, brand_imgThmb FROM $this->table WHERE brand_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_num_rows($query) == 1){
			$row = mysql_fetch_assoc($query);
            $deleteImg = $_SERVER['DOCUMENT_ROOT']."/".$row['brand_img'];
            if (file_exists($deleteImg)) {
                unlink($deleteImg);
                $flag_img = 1;
            }
            $deleteImgThmb = $_SERVER['DOCUMENT_ROOT']."/".$row['brand_imgThmb'];
            if (file_exists($deleteImgThmb)) {
                unlink($deleteImgThmb);
                $flag_img_thmb = 1;
            }
            if($flag_img == 1 && $flag_img_thmb ==1){
            	$result = 1;
            }
		}
		return $result;
	}
//END FUNCTION FOR ADMIN PAGE
}
?>