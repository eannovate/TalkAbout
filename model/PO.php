<?php
class PO{
	private $table = "T_PO";
	private $itemPerPageAdmin = 10;
    private $itemPerPageAdminOrder = 20;
    private $itemPerPageClient = 10;
//START FUNCTION FOR CLIENT PAGE
    //function for get order po in order-detail.php
    public function get_order_po($id){
        $result = 0;
        $text = "SELECT address_ID, address_street1 AS po_addressStreet1, address_street2 AS po_addressStreet2, 
            address_city AS po_addressCity, address_state AS po_addressState, address_country AS po_addressCountry, 
            address_zip AS po_addressZip ,po_ID, po_name, po_date, po_isPaid, po_status, po_note, po_resi, po_amount, 
            po_service, po_totalAmount, po_shippingCost, po_note, po_couponID, po_couponName, po_couponPrice, (SELECT COUNT(atpp_ID) FROM AT_PO_PRODUCT WHERE atpp_poID = '$id') AS counter_product, 
            customer_ID, customer_fname, customer_phone, customer_email, confirm_ID, confirm_status, confirm_amount 
            FROM $this->table LEFT JOIN T_CUSTOMER ON po_custID = customer_ID LEFT JOIN T_CONFIRM ON confirm_poID = po_ID 
            LEFT JOIN T_ADDRESS ON po_addressID = address_ID WHERE po_ID = '$id'";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            $loop = 0;
            while($row = mysql_fetch_assoc($query)){
                $result[$loop] = $row;
                
                $text_child = "SELECT atpp_productID, atpp_productName, atpp_qty, atpp_price, atpp_weight, product_weight 
                    FROM AT_PO_PRODUCT LEFT JOIN T_PRODUCT ON atpp_productID = product_ID WHERE atpp_poID = '$id' 
                    ORDER BY UNIX_TIMESTAMP(atpp_modifyDate) DESC";
                $query_child = mysql_query($text_child);
                if(mysql_num_rows($query_child) >= 1){
                    while($row_child = mysql_fetch_assoc($query_child)){
                        $result[$loop][] = $row_child;
                    }
                }
                $loop++;
            }
        }    
        return $result;
    }
    //function get order customer in order-history.php
    public function get_order_customer($page=1, $customer_id){
        $result = 0;    
        //get total data
        $text_total = "SELECT po_ID FROM $this->table WHERE po_custID = '$customer_id'";
        $query_total = mysql_query($text_total);
        $total_data = mysql_num_rows($query_total);
        if($total_data < 1){$total_data = 0;}
        //get total page
        $total_page = ceil($total_data / $this->itemPerPageClient);
        $limitBefore = $page <= 1 || $page == null ? 0 : ($page-1) * $this->itemPerPageClient;
        /*$text = "SELECT * FROM $this->table LEFT JOIN T_CONFIRM ON confirm_poID = po_ID AND confirm_customerID = po_custID 
            LEFT JOIN T_CUSTOMER ON customer_ID = po_custID WHERE po_custID = '$customer_id' ORDER BY po_date DESC LIMIT 
            $limitBefore, $this->itemPerPageClient";*/
        $text = "SELECT po_ID, po_name, po_date, po_totalAmount, po_status, po_isPaid, po_couponID, po_couponName, po_couponPrice FROM $this->table WHERE po_custID = '$customer_id'
            ORDER BY po_date DESC LIMIT $limitBefore, $this->itemPerPageClient";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }
        if(is_array($result)){
            $result[0]['total_page'] = $total_page;
            $result[0]['total_data_all'] = $total_data;
            $result[0]['total_data'] = count($result);
        }
        return $result;
    }
    //function for set as paid/not in module/order/edit.php, public_html/confirm-payment.php
    public function update_po_status($id, $po_status){
        $result = 0;
        $text = "UPDATE $this->table SET po_status = '$po_status' WHERE po_ID = '$id'";
        $query = mysql_query($text);
        if(mysql_affected_rows() == 1){
            $result = 1;
        }
        return $result;
    }
    //function get po order in confirm-payment.php
    public function get_po_order($customer_id){
        $result = 0;
        $text = "SELECT po_ID, po_name, po_totalAmount, po_date FROM $this->table WHERE po_status = '1' AND po_custID = '$customer_id'";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }
        return $result;
    }

    public function get_by_po($po_name){
        $result = 0;

        $text = "SELECT po_ID, po_name, po_type, po_date, po_service, po_shippingCost, po_totalAmount, 
            po_amount, po_status, po_shippingWeight, po_couponID, po_couponName, po_couponPrice FROM T_PO WHERE po_name = '$po_name'";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }
        return $result;
    }

    public function get_product_by_idpo($po_id){
        $result = 0;
        $text = "SELECT atpp_ID, atpp_qty, atpp_weight, atpp_price, product_ID, product_name FROM `AT_PO_PRODUCT` LEFT JOIN T_PRODUCT ON atpp_productID = product_ID WHERE atpp_poID = '$po_id'";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }
        return $result;
    }

    public function generate_po(){
        $result = 0;
        date_default_timezone_set('Asia/Jakarta');

        $text = "SELECT po_ID FROM T_PO WHERE po_date >= CURDATE()";
        $query = mysql_query($text);
        if($query){
            $total_data = mysql_num_rows($query);
            if($total_data > 0){
                $num = $total_data + 1;
                $result = "PO".date("d").date("m").date("Y")."-".$num;
            } else {
                $result = "PO".date("d").date("m").date("Y")."-1";
            }
        } else {
            $result = "PO".date("d").date("m").date("Y")."-1";
        }
        return $result;
    }

    public function create_atpo($po_ID, $product_ID, $product_qty, $product_weight, $product_price, $product_name){
        $result = 0;
        
        $text = "INSERT INTO AT_PO_PRODUCT (atpp_poID, atpp_productID, atpp_qty, atpp_weight, atpp_price, atpp_productName, atpp_createDate) VALUES('$po_ID', '$product_ID', '$product_qty', '$product_weight', '$product_price', '$product_name', NOW())";
        $query = mysql_query($text);
        if($query){
            $result = 1;
        }
        return $result;
    }

    public function create_po($po_name, $po_type, $po_custID, $po_ship, $po_weight, $po_addressID, $po_subtotal, $po_total, $po_service, $po_couponID, $po_couponName, $po_couponPrice){
        $result = 0;
        date_default_timezone_set('Asia/Jakarta');
        $now = date("Y-m-d H:i:s");
        $po_status = $po_type == "bank" ? 1 : 7;
        $po_paid = $po_type == "bank" ? 0 : 0;

        $text = "INSERT INTO $this->table (po_name, po_type, po_custID, po_shippingCost, po_shippingWeight, po_addressID, po_amount, po_status, po_isPaid, po_totalAmount, po_couponID, po_couponName, po_couponPrice, po_service, po_date) 
            VALUES('$po_name', '$po_type', '$po_custID', '$po_ship', '$po_weight', '$po_addressID', '$po_subtotal', '$po_status', '$po_paid', '$po_total', '$po_couponID', '$po_couponName', '$po_couponPrice', '$po_service', '$now')";
        $query = mysql_query($text);
        if($query){
            $result = mysql_insert_id();
        }
        return $result;
    }

    //function for set transaction status
    public function update_transaction_status($name, $status, $paid){
        $result = 0;
        
        $text = "UPDATE $this->table SET po_isPaid = '$paid', po_status = $status WHERE po_name = '$name'";
        $query = mysql_query($text);
        if(mysql_affected_rows() == 1){
            $result = 1;
        }
        return $result;
    }
//END FUNCTION FOR CLIENT PAGE

//START FUNCTION FOR ADMIN PAGE
    //function for get data order in module/order/edit.php
    public function get_order_detail($id){
        $result = 0;
        $text = "SELECT address_ID, address_street1 AS po_addressStreet1, address_street2 AS po_addressStreet2, address_city AS po_addressCity, address_state AS po_addressState, address_country AS po_addressCountry, address_zip AS po_addressZip ,po_ID, po_name, po_date, po_isPaid, po_status, po_service, po_note, po_resi, po_amount, po_totalAmount, po_shippingCost, po_note, po_couponID, po_couponName, po_couponPrice, (SELECT COUNT(atpp_ID) FROM AT_PO_PRODUCT WHERE atpp_poID = '$id') AS counter_product, customer_ID, customer_fname, customer_phone, customer_email, confirm_ID, confirm_status, confirm_amount FROM $this->table LEFT JOIN T_CUSTOMER ON po_custID = customer_ID LEFT JOIN T_CONFIRM ON confirm_poID = po_ID LEFT JOIN T_ADDRESS ON po_addressID = address_ID WHERE po_ID = '$id'";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            $loop = 0;
            while($row = mysql_fetch_assoc($query)){
                $result[$loop] = $row;
                $text_child = "SELECT atpp_productID, atpp_productName, atpp_qty, atpp_price, product_weight FROM AT_PO_PRODUCT LEFT JOIN T_PRODUCT ON atpp_productID = product_ID WHERE 
                    atpp_poID = '$id' ORDER BY UNIX_TIMESTAMP(atpp_modifyDate) DESC";
                $query_child = mysql_query($text_child);
                if(mysql_num_rows($query_child) >= 1){
                    while($row_child = mysql_fetch_assoc($query_child)){
                        $result[$loop][] = $row_child;
                    }
                }
                $loop++;
            }
        }    
        return $result;
    }
    //function for set as paid/not in module/order/edit.php
    public function update_paid($id, $paid){
        $result = 0;
        if($paid == 1){
            $po_status = 3 ;
        }
        else{
            $po_status = 2 ;
        }
        $text = "UPDATE $this->table SET po_isPaid = '$paid', po_status = $po_status, po_resi='0'  WHERE po_ID = '$id'";
        $query = mysql_query($text);
        if(mysql_affected_rows() == 1){
            $result = 1;
        }
        return $result;
    }
    //function for cancel order in module/order/edit.php
    public function cancel_order($id){
        $result = 0;
        $text = "UPDATE $this->table SET po_status = 6 , po_resi='0' WHERE po_ID = '$id'";
        $query = mysql_query($text);
        if(mysql_affected_rows() == 1){
            $result = 1;
        }
        return $result;
    }
    //function for update note in module/order/edit.php
    public function update_note($id, $note){
        $result = 0;
        $text = "UPDATE $this->table SET po_note='$note' WHERE po_ID = '$id'";
        $query = mysql_query($text);
        if(mysql_affected_rows() == 1){
            $result = 1;
        }
        return $result;
    }
    //function for get data order in module/order/index.php
    public function update_resi($id, $resi, $status){
        $result = 0;
        $text = "UPDATE $this->table SET po_resi='$resi', po_status= $status WHERE po_ID = '$id'";
        $query = mysql_query($text);
        if(mysql_affected_rows() == 1){
            $result = 1;
        }
        return $result;
    }
    public function get_order_history($page=1){
        $result = 0;    
        //get total data
        $text_total = "SELECT Po_ID FROM T_PO LEFT JOIN T_CONFIRM ON confirm_poID = po_ID AND confirm_customerID = po_custID LEFT JOIN T_CUSTOMER ON customer_ID = po_custID";
        $query_total = mysql_query($text_total);
        $total_data = mysql_num_rows($query_total);
        if($total_data < 1){$total_data = 0;}
        //get total page
        $total_page = ceil($total_data / $this->itemPerPageAdminOrder);
        if($page <= 1 || $page == null){
            $limitBefore = 0;
        }else{
            $limitBefore = ($page-1) * $this->itemPerPageAdminOrder;
        }
        $text = "SELECT * FROM T_PO LEFT JOIN T_CONFIRM ON confirm_poID = po_ID AND confirm_customerID = po_custID LEFT JOIN T_CUSTOMER ON customer_ID = po_custID ORDER BY po_date DESC LIMIT $limitBefore, $this->itemPerPageAdminOrder";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }
        
        if(is_array($result)){
            $result[0]['total_page'] = $total_page;
            $result[0]['total_data_all'] = $total_data;
            $result[0]['total_data'] = count($result);
        }
        return $result;
    }
    //function for get data order in module/customer/edit.php
	public function get_po_by_customer($page=1, $cust_id){
        $result = 0;    
        //get total data
        $text_total = "SELECT po_ID FROM $this->table WHERE po_custID = '$cust_id'";
        $query_total = mysql_query($text_total);
        $total_data = mysql_num_rows($query_total);
        if($total_data < 1){$total_data = 0;}
        //get total page
        $total_page = ceil($total_data / $this->itemPerPageAdmin);
        if($page <= 1 || $page == null){
            $limitBefore = 0;
        }else{
            $limitBefore = ($page-1) * $this->itemPerPageAdmin;
        }
        $text = "SELECT * FROM $this->table WHERE po_custID = '$cust_id' ORDER BY UNIX_TIMESTAMP(po_modifyDate) DESC LIMIT $limitBefore, $this->itemPerPageAdmin";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }
        
        if(is_array($result)){
            $result[0]['total_page'] = $total_page;
            $result[0]['total_data_all'] = $total_data;
            $result[0]['total_data'] = count($result);
        }
        //$result = $text;
        return $result;
    }
    //function for get data order in module/customer/order.php
    public function get_data_order($id){
        $result = 0;
        $text = "SELECT po_ID, po_name, po_terms, po_date, po_status, po_note, po_resi, po_amount, po_totalAmount, po_shippingCost, 
            po_addressName, po_addressType, po_couponID, po_couponName, po_couponPrice, po_addressStreet1, po_addressStreet2, po_addressCity, po_addressState, po_addressZip,
            (SELECT COUNT(atpp_ID) FROM AT_PO_PRODUCT WHERE atpp_poID = '$id') AS counter_product, po_addressCountry, customer_ID,
            customer_fname, customer_phone, customer_email FROM $this->table LEFT JOIN T_CUSTOMER ON po_custID = customer_ID 
            WHERE po_ID = '$id'";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            $loop = 0;
            while($row = mysql_fetch_assoc($query)){
                $result[$loop] = $row;
                $text_child = "SELECT atpp_productID, atpp_productName, atpp_qty, atpp_price FROM AT_PO_PRODUCT WHERE 
                    atpp_poID = '$id' ORDER BY UNIX_TIMESTAMP(atpp_modifyDate) DESC";
                $query_child = mysql_query($text_child);
                if(mysql_num_rows($query_child) >= 1){
                    while($row_child = mysql_fetch_assoc($query_child)){
                        $result[$loop][] = $row_child;
                    }
                }
                $loop++;
            }
        }    
        return $result;
    }   
//END FUNCTION FOR ADMIN PAGE
}
?>