<?php
class Photo{
    
	private $table = "T_PHOTO";

    public function insert_data($img, $img_thmb, $product_id, $main, $title){
        $result = 0;
        
        $text = "INSERT INTO $this->table (photo_imgLoc, photo_imgLocThmb, photo_productID, photo_main, photo_title, photo_createDate) VALUES ('$img', '$img_thmb' ,'$product_id', '$main', '$title', NOW())";
        $query = mysql_query($text);
        if($query){
            $result = 1;
        }
        return $result;
    }

    public function insert_sub_data($img, $img_thmb, $product_id, $title){
        $result = 0;

        $text = "INSERT INTO $this->table (photo_imgLoc, photo_imgLocThmb, photo_productID, photo_title, photo_createDate) VALUES ('$img', '$img_thmb' ,'$product_id', '$title', NOW())";
        $query = mysql_query($text);
        if($query){
            $result = 1;
        }
        return $result;
    }

    public function delete_data($product_id, $id, $path){
        $result = 0;
        $this->remove_photo($product_id, $id, $path); //remove photo by product 
        
        $text = "DELETE FROM $this->table WHERE photo_productID = '$product_id' AND photo_ID = '$id'";
        $query = mysql_query($text);
        if(mysql_affected_rows() == 1){
            $result = 1;
        }
        //$result = $text;
        return $result;
    }

    public function remove_photo($product_id, $id, $path){
        $result = 0;
        $flag_img = 0;
        $flag_img_thmb = 0;
        $text = "SELECT photo_imgLoc, photo_imgLocThmb FROM $this->table WHERE photo_productID = '$product_id' AND photo_ID = '$id'";
        $query = mysql_query($text);
        if(mysql_num_rows($query) == 1){
            $row = mysql_fetch_assoc($query);
            $deleteImg = $path.$row['photo_imgLoc'];
            if (file_exists($deleteImg)) {
                unlink($deleteImg);
                $flag_img = 1;
            }
            $deleteImgThmb = $path.$row['photo_imgLocThmb'];
            if (file_exists($deleteImgThmb)) {
                unlink($deleteImgThmb);
                $flag_img_thmb = 1;
            }
            if($flag_img == 1 && $flag_img_thmb ==1){
                $result = 1;
            }
        }
        return $result;
    }

    public function update_title($id, $title){
        $result = 0;

        $text = "UPDATE $this->table SET photo_title = '$title' WHERE photo_ID = '$id'";
        $query = mysql_query($text);
        if(mysql_affected_rows() == 1){
            $result = 1;
        }
        return $result;
    }

    public function set_main($product_id, $main){
        $result = 0;
        $text = "UPDATE $this->table SET photo_main = 1 WHERE photo_productID = '$product_id' AND photo_ID = '$main'";
        $query = mysql_query($text);
        if(mysql_affected_rows() == 1){
            $result = 1;
        }
        return $result;
    }

    public function unset_main($product_id){
        $result = 0;
        $text = "UPDATE $this->table SET photo_main = 0 WHERE photo_productID = '$product_id'";
        $query = mysql_query($text);
        if(mysql_affected_rows() == 1){
            $result = 1;
        }
        return $result;
    }
}
?>