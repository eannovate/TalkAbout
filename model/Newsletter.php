<?php
class Newsletter{

	private $table = "T_MAIL";
	private $itemPerPageAdmin = 10;

//START FUNCTION FOR ADMIN PAGE
    public function check_data($email){
        $result = 0;

        $text = "SELECT * FROM $this->table WHERE mail_email = '$email'";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = 1;
        } else {
            $result = 0;
        }
        return $result;
    }

    public function insert_data_client($email){
        $result = 0;
        date_default_timezone_set('Asia/Jakarta');
        $now = date("Y-m-d H:i:s");

        $text = "INSERT INTO $this->table (mail_email, mail_status, mail_createDate) VALUES('$email', '1', '$now')";
        $query = mysql_query($text);
        if($query){
            $result = 1;
        }
        return $result;
    }
    
    //function get data all by status for export to excel
    public function get_data_all_by_status($status){
        $result = 0;    
        
        $text = "SELECT * FROM $this->table WHERE mail_status = '$status' ORDER BY mail_modifyDate DESC";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }
        //$result = $text;
        return $result;
    }

	public function get_data_inactive(){
        $result = 0;    
    
        //get total data
        $text_total = "SELECT * FROM $this->table WHERE mail_status = '0'";
        $query_total = mysql_query($text_total);
        $total_data = mysql_num_rows($query_total);
        if($total_data < 1){$total_data = 0;}

        $text = "SELECT * FROM $this->table WHERE mail_status = '0' ORDER BY mail_modifyDate DESC LIMIT 0, $this->itemPerPageAdmin";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }

        if(is_array($result)){
            $result[0]['total_data_all'] = $total_data;
        }
        //$result = $text;
        return $result;
    }

    public function get_data_active(){
        $result = 0;    

        //get total data
        $text_total = "SELECT * FROM $this->table WHERE mail_status = '1'";
        $query_total = mysql_query($text_total);
        $total_data = mysql_num_rows($query_total);
        if($total_data < 1){$total_data = 0;}

        $text = "SELECT * FROM $this->table WHERE mail_status = '1' ORDER BY mail_modifyDate DESC LIMIT 0, $this->itemPerPageAdmin";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }

        if(is_array($result)){
            $result[0]['total_data_all'] = $total_data;
        }
        //$result = $text;
        return $result;
    }
    
    public function get_data_by_page($page=1,$stat){
        $result = 0;  

        //get total data
        $text_total = "SELECT * FROM $this->table WHERE mail_status = '$stat'";
        $query_total = mysql_query($text_total);
        $total_data = mysql_num_rows($query_total);
        if($total_data < 1){$total_data = 0;}
        //get total page
        $total_page = ceil($total_data / $this->itemPerPageAdmin);
        if($page <= 1 || $page == null){
            $limitBefore = 0;
        }else{
            $limitBefore = ($page-1) * $this->itemPerPageAdmin;
        }

        $text = "SELECT * FROM $this->table WHERE mail_status = '$stat' ORDER BY mail_createDate DESC LIMIT $limitBefore, $this->itemPerPageAdmin";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }

        if(is_array($result)){
            $result[0]['total_page'] = $total_page;
            $result[0]['total_data_all'] = $total_data;
            $result[0]['total_data'] = count($result);
        }
        //$result = $text;
        return $result;
    }

    public function set_status($id, $stat){
        $result = 0;

        $text = "UPDATE T_MAIL SET mail_status ='$stat' WHERE mail_ID='$id'";
        $query = mysql_query($text);
        if(mysql_affected_rows() == 1){
            $result = 1;
        }
        return $result;
    }
//END FUNCTION FOR ADMIN PAGE
}
?>