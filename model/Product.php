<?php
class Product{

	private $table = "T_PRODUCT";
	private $itemPerPage = 8;
    private $itemPerPageAdmin = 10;
    private $itemPerPageSearch = 12;
    private $join = "LEFT JOIN T_CATEGORY ON product_ref_categoryID = category_ID";
    private $joinBrand = "LEFT JOIN T_BRAND ON product_ref_brandID = brand_ID";
    private $joinPhoto = "LEFT JOIN T_PHOTO ON product_ID = photo_productID";
    
//START FUNCTION FOR CLIENT PAGE
    //function get category product in sitemap.php
    public function get_category_product(){
        $result = 0;

        $text = "SELECT category_ID, category_name, category_slug FROM T_CATEGORY WHERE category_publish = 'Publish'";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            $loop = 0;
            while($row = mysql_fetch_assoc($query)){
                $result[$loop] = $row;

                //product
                $text_product = "SELECT product_ID, product_name FROM $this->table WHERE product_publish = 'Publish' 
                    AND product_ref_categoryID = '{$row['category_ID']}' ORDER BY product_createdDate 
                    DESC LIMIT 0, 4";
                $query_product = mysql_query($text_product);
                if(mysql_num_rows($query_product) >= 1){
                    while($row_product = mysql_fetch_array($query_product, MYSQL_ASSOC)){
                        $result[$loop]['product'][] = $row_product;
                    }
                }
                $loop++;
            }
        }
        //$result = $text_product;
        return $result;
    }
    /*//function get product search-brand in search.php
    public function get_product_by_brand($page=1, $id){
        $result = 0;
        $text_total = "SELECT product_ID, product_name, product_price, product_specialPrice, product_limitedStock, product_desc, product_shortDesc, category_name, 
            brand_title, photo_imgLoc, photo_imgLocThmb, product_publish
            FROM $this->table $this->join $this->joinBrand $this->joinPhoto WHERE product_publish = 'Publish' 
            AND photo_main = 1 AND brand_ID = $id";
        $query_total = mysql_query($text_total);
        $total_data = mysql_num_rows($query_total);
        if($total_data < 1){$total_data = 0;}
       
        if($page == "all"){
            $cond_limit = "";
        }else{
             //get total page
            $total_page = ceil($total_data / $this->itemPerPageSearch);
            $limitBefore = $page <= 1 || $page == null ? 0 : ($page-1) * $this->itemPerPageSearch;

            $cond_limit = "LIMIT ".$limitBefore.",".$this->itemPerPageSearch;

        }
        
        $text = "SELECT product_ID, product_name, product_price, product_specialPrice, product_limitedStock, product_desc, product_shortDesc, category_name, 
            brand_title, photo_imgLoc, photo_imgLocThmb, product_publish
            FROM $this->table $this->join $this->joinBrand $this->joinPhoto WHERE product_publish = 'Publish' 
            AND photo_main = 1 AND brand_ID = $id ORDER BY brand_createDate DESC $cond_limit ";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }
        if(is_array($result)){
            $result[0]['total_page'] = $total_page;
            $result[0]['total_data_all'] = $total_data;
            $result[0]['total_data'] = count($result);
        }
        //$result = $text;
        return $result;
    } */
    //function get product search in search.php by Hans 9/29/2016
    public function get_search_by_page($page=1, $key){
        $result = 0;

        $text_total = "SELECT product_ID, product_name, product_price, product_specialPrice, product_desc, product_limitedStock, product_shortDesc, category_name, 
            photo_imgLoc, photo_imgLocThmb, product_publish,
            IF(product_name LIKE '$key%', 20, IF(product_name LIKE '%$key%', 10, 0))
                + IF(category_name LIKE '%$key%', 5, 0) AS weight
            FROM $this->table $this->join $this->joinPhoto WHERE product_publish = 'Publish' 
            AND photo_main = 1 AND (product_name LIKE '%$key%' OR category_name LIKE '%$key%')";
        $query_total = mysql_query($text_total);
        $total_data = mysql_num_rows($query_total);
        if($total_data < 1){$total_data = 0;}
       
        if($page == "all"){
            $cond_limit = "";
        }else{
             //get total page
            $total_page = ceil($total_data / $this->itemPerPageSearch);
            $limitBefore = $page <= 1 || $page == null ? 0 : ($page-1) * $this->itemPerPageSearch;

            $cond_limit = "LIMIT ".$limitBefore.",".$this->itemPerPageSearch;

        }
        
        $text = "SELECT product_ID, product_name, product_price, product_specialPrice, product_limitedStock, product_desc, product_shortDesc, category_name, 
            photo_imgLoc, photo_imgLocThmb, product_publish,
            IF(product_name LIKE '$key%', 20, IF(product_name LIKE '%$key%', 10, 0))
                + IF(category_name LIKE '%$key%', 5, 0) AS weight
            FROM $this->table $this->join $this->joinPhoto WHERE product_publish = 'Publish' 
            AND photo_main = 1 AND (product_name LIKE '%$key%' OR category_name LIKE '%$key%')
            ORDER BY weight DESC $cond_limit ";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }
        if(is_array($result)){
            $result[0]['total_page'] = $total_page;
            $result[0]['total_data_all'] = $total_data;
            $result[0]['total_data'] = count($result);
        }
        //$result = $text;
        return $result;
    }
    /*
    //function get product sale in sale.php
    public function get_product_sale($page, $category_id, $sort, $view, $brand, $min, $max){
        $result = 0;

        $cond = "";
        if($category_id != ""){
            $cond = "AND product_ref_categoryID IN($category_id)";
        }

        $cond_brand = "";
        if($brand != ""){
            $brand_string = str_replace(";", ",", $brand);
            $cond_brand = "AND brand_ID IN($brand_string)";
        }

        $cond_price = "";
        if($min != "" && $max != ""){
            $cond_price = "AND product_specialPrice BETWEEN '$min' AND '$max'";
        }

        $cond_sort = "";
        switch ($sort) {
            case "recent":
                $cond_sort = "ORDER BY product_createdDate DESC";
                break;
            case "popular":
                $cond_sort = "ORDER BY product_createdDate ASC";
                break;
            case "discount":
                $cond_sort = "ORDER BY product_specialPrice ASC";
                break;
            case "lower_price":
                $cond_sort = "ORDER BY product_price ASC";
                break;
            case "higher_price":
                $cond_sort = "ORDER BY product_price DESC";
                break;
            default:
                $cond_sort = "ORDER BY product_createdDate DESC";
                break;
        }

        $text_total = "SELECT product_ID FROM $this->table $this->join $this->joinBrand WHERE product_publish = 'Publish' AND product_specialPrice != 0 $cond $cond_brand $cond_price";
        $query_total = mysql_query($text_total);
        $total_data = mysql_num_rows($query_total);
        if($total_data < 1){$total_data = 0;}
        //get total page
        $total_page = ceil($total_data / $view);
        if($page!="all"){
            $limitBefore = $page <= 1 || $page == null ? 0 : ($page-1) * $view;
            $cond_page = "LIMIT $limitBefore, $view";
        }else{
            $cond_page = "";
        }
        
        $text = "SELECT product_ID, product_name, product_limitedStock, product_desc, product_price, product_specialPrice, product_shortDesc, photo_imgLoc, photo_imgLocThmb, 
            product_publish, category_ID, category_name, brand_ID, brand_title FROM $this->table $this->join $this->joinPhoto $this->joinBrand WHERE product_publish = 'Publish' 
            AND photo_main = 1 AND product_specialPrice != 0 $cond $cond_brand $cond_price $cond_sort $cond_page";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }
        if(is_array($result)){
            $result[0]['total_page'] = $total_page;
            $result[0]['total_data_all'] = $total_data;
            $result[0]['total_data'] = count($result);
        }
        //$result = $text;
        return $result;
    }

    //function get product brand in search-brand.php
    public function get_product_brand($page, $category_id, $sort, $view, $brand, $min, $max){
        $result = 0;

        $cond = "";
        if($category_id != ""){
            $cond = "AND product_ref_categoryID IN($category_id)";
        }

        $cond_brand = "";
        if($brand != ""){
            $brand_string = str_replace(";", ",", $brand);
            $cond_brand = "AND brand_ID IN($brand_string)";
        }

        $cond_price = "";
        if($min != "" && $max != ""){
            $cond_price = "AND product_price BETWEEN '$min' AND '$max'";
        }

        $cond_sort = "";
        switch ($sort) {
            case "recent":
                $cond_sort = "ORDER BY product_createdDate DESC";
                break;
            case "popular":
                $cond_sort = "ORDER BY product_createdDate ASC";
                break;
            case "discount":
                $cond_sort = "ORDER BY product_specialPrice ASC";
                break;
            case "lower_price":
                $cond_sort = "ORDER BY product_price ASC";
                break;
            case "higher_price":
                $cond_sort = "ORDER BY product_price DESC";
                break;
            default:
                $cond_sort = "ORDER BY product_createdDate DESC";
                break;
        }

        $text_total = "SELECT product_ID FROM $this->table $this->join $this->joinBrand WHERE product_publish = 'Publish' $cond $cond_brand $cond_price";
        $query_total = mysql_query($text_total);
        $total_data = mysql_num_rows($query_total);
        if($total_data < 1){$total_data = 0;}
        //get total page
        $total_page = ceil($total_data / $view);
        if($page!="all"){
            $limitBefore = $page <= 1 || $page == null ? 0 : ($page-1) * $view;
            $cond_page = "LIMIT $limitBefore, $view";
        }else{
            $cond_page = "";
        }
        
        $text = "SELECT product_ID, product_name, product_limitedStock, product_desc, product_price, product_specialPrice, product_shortDesc, photo_imgLoc, photo_imgLocThmb, 
            product_publish, category_ID, category_name, brand_ID, brand_title FROM $this->table $this->join $this->joinPhoto $this->joinBrand WHERE product_publish = 'Publish' 
            AND photo_main = 1 $cond $cond_brand $cond_price $cond_sort $cond_page";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }
        if(is_array($result)){
            $result[0]['total_page'] = $total_page;
            $result[0]['total_data_all'] = $total_data;
            $result[0]['total_data'] = count($result);
        }
        //$result = $text;
        return $result;
    }*/

    //function get product bestseller by category in product.php
    public function get_product_bestseller_by_category($title){
        $result = 0;

        $text = "SELECT product_ID, product_name, product_limitedStock, product_desc, product_price, product_specialPrice, product_shortDesc, product_weight,
            photo_imgLoc, photo_imgLocThmb, photo_title, product_publish, category_ID, category_name, category_img, category_imgThmb FROM $this->table $this->join 
            $this->joinPhoto WHERE product_publish = 'Publish' AND photo_main = 1 AND category_slug = '$title' AND product_bestSeller = 1
            ORDER BY product_createdDate DESC LIMIT 0,1";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }
        //$result = $text;
        return $result;
    }

    //function get product by category in product.php
    public function get_product_by_category($page, $title){
        $result = 0;

        $text_total = "SELECT product_ID FROM $this->table $this->join 
            WHERE product_publish = 'Publish' AND category_slug = '$title'";
        $query_total = mysql_query($text_total);
        $total_data = mysql_num_rows($query_total);
        if($total_data < 1){$total_data = 0;}
        //get total page
        $total_page = ceil($total_data / $this->itemPerPage);
        $limitBefore = $page <= 1 || $page == null ? 0 : ($page-1) * $this->itemPerPage;
        
        $text = "SELECT product_ID, product_name, product_limitedStock, product_desc, product_price, product_specialPrice, product_shortDesc, product_weight, 
            photo_imgLoc, photo_imgLocThmb, photo_title, product_publish, category_ID, category_name FROM $this->table $this->join $this->joinPhoto 
            WHERE product_publish = 'Publish' AND photo_main = 1 AND category_slug = '$title' LIMIT $limitBefore, $this->itemPerPage";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }

        if(is_array($result)){
            $result[0]['total_page'] = $total_page;
            $result[0]['total_data_all'] = $total_data;
            $result[0]['total_data'] = count($result);
        }
        //$result = $text;
        return $result;
    }

    /*//function get product feature by category in category.php
    public function get_product_feature_by_category($feature, $category_id){
        $result = 0;

        $cond = "";
        if($feature == "best_seller"){
            $cond = "AND product_bestSeller = 1";
        }else{
            $cond = "AND product_trending = 1";
        }

        $text = "SELECT product_ID, product_name, product_limitedStock, product_desc, product_price, product_specialPrice, product_shortDesc, photo_imgLoc, photo_imgLocThmb, 
            product_publish, category_ID, category_name, brand_ID, brand_title FROM $this->table $this->join $this->joinPhoto $this->joinBrand WHERE product_publish = 'Publish' 
            AND photo_main = 1 $cond AND product_ref_categoryID IN($category_id) ORDER BY product_createdDate DESC LIMIT 0, 4";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }
        //$result = $text;
        return $result;
    }*/

    //function get product detail in product-detail.php
    public function get_product_detail($id){
        $result = 0;

        $text = "SELECT product_ID, product_name, product_limitedStock, product_desc, product_specialPrice, product_price, 
            product_shortDesc, product_publish, product_weight, category_ID, category_name, category_slug, photo_imgLoc, 
            photo_imgLocThmb, photo_title FROM $this->table $this->join $this->joinPhoto WHERE product_publish = 'Publish' 
            AND photo_main = 1 AND product_ID = '$id'";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            $loop = 0;
            while($row = mysql_fetch_assoc($query)){
                $result[$loop] = $row;

                //product photo
                $text_photo = "SELECT photo_ID, photo_imgLoc, photo_imgLocThmb, photo_title FROM 
                    T_PHOTO WHERE photo_productID = '{$row['product_ID']}' ORDER BY photo_main DESC";
                $query_photo = mysql_query($text_photo);
                if(mysql_num_rows($query_photo) >= 1){
                    while($row_photo = mysql_fetch_array($query_photo, MYSQL_ASSOC)){
                        $result[$loop]['photo'][] = $row_photo;
                    }
                }

                //product related
                $text_related = "SELECT product_ID, product_name, product_limitedStock, product_desc, product_price, 
                    product_specialPrice, product_shortDesc, product_publish, photo_imgLoc, photo_imgLocThmb, photo_title, 
                    category_ID, category_name, category_slug FROM $this->table $this->join $this->joinPhoto WHERE 
                    product_publish = 'Publish' AND photo_main = 1 AND product_ref_categoryID = '{$row['category_ID']}' 
                    AND product_ID != '{$row['product_ID']}' ORDER BY product_createdDate DESC LIMIT 0, 4";
                $query_related = mysql_query($text_related);
                if(mysql_num_rows($query_related) >= 1){
                    while($row_related = mysql_fetch_array($query_related, MYSQL_ASSOC)){
                        $result[$loop]['related'][] = $row_related;
                    }
                }
                $loop++;
            }
        }
        //$result = $text_photo;
        return $result;
    }

    /*public function get_home_trend(){
        $result = 0;
        $text = "SELECT product_ID, product_name, product_desc, product_price, product_shortDesc, product_weight, product_specialPrice, product_trending, product_bestSeller, product_freeShipping, product_limitedStock, product_limitedEdition, photo_imgLoc, photo_imgLocThmb, product_publish, product_ref_categoryID, category_name, brand_title , brand_ID 
            FROM $this->table $this->join $this->joinBrand LEFT JOIN T_PHOTO ON product_ID = photo_productID WHERE product_publish = 'Publish' AND photo_main = 1 AND product_trending = '1' ORDER BY product_createdDate DESC LIMIT 4 ";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            $loop = 0;
            while($row = mysql_fetch_assoc($query)){
                $result[$loop] = $row;                
                $loop++;
            }
        }
        //$result = $text;
        return $result;

    }*/

    public function get_home_best(){
        $result = 0;
        $text = "SELECT product_ID, product_name, product_desc, product_price, product_shortDesc, product_weight, product_specialPrice, product_trending, product_bestSeller, product_freeShipping, product_limitedStock, product_limitedEdition, photo_imgLoc, photo_imgLocThmb, product_publish, product_ref_categoryID, category_name
            FROM $this->table $this->join $this->joinPhoto WHERE product_publish = 'Publish' AND photo_main = 1 AND product_bestSeller = '1' ORDER BY product_createdDate DESC LIMIT 4 ";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            $loop = 0;
            while($row = mysql_fetch_assoc($query)){
                $result[$loop] = $row;                
                $loop++;
            }
        }
        //$result = $text;
        return $result;

    }

    /*public function get_sidebar($id){
        if($id != ""){
            $cond_pro = "AND product_ID != '$id'";
        } else {
            $cond_pro = "";
        }
        $text = "SELECT product_ID, product_name, product_price, product_shortDesc, product_weight, product_specialPrice, product_trending, product_bestSeller, product_freeShipping, product_limitedStock, product_limitedEdition, photo_imgLoc, photo_imgLocThmb, product_publish, product_ref_categoryID
            FROM $this->table LEFT JOIN T_PHOTO ON product_ID = photo_productID WHERE product_publish = 'Publish' AND photo_main = 1 $cond_pro ORDER BY product_createdDate DESC LIMIT 5";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            $loop = 0;
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }   
        return $result;
    }

    public function get_related($cat_id){
        $text = "SELECT product_ID, product_name, product_price, product_shortDesc, product_weight, product_specialPrice, product_trending, product_bestSeller, product_freeShipping, product_limitedStock, product_limitedEdition, photo_imgLoc, photo_imgLocThmb, product_publish, product_ref_categoryID
            FROM $this->table LEFT JOIN T_PHOTO ON product_ID = photo_productID WHERE product_ref_categoryID = '$cat_id' AND product_publish = 'Publish' AND photo_main = 1  ORDER BY product_createdDate DESC LIMIT 8";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            $loop = 0;
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }   
        return $result;
    }

    public function get_photo_by_id($id){
        $result = 0;
        $text = "SELECT photo_ID, photo_productID, photo_title, photo_imgLoc, photo_imgLocThmb, photo_main 
            FROM T_PHOTO WHERE photo_productID = '$id' ORDER BY photo_main DESC, photo_ID DESC";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }
        //$result = $text;
        return $result;
    }

    public function get_product($page,$show,$sort,$catID){
        $cond_sort = "ORDER BY product_createdDate DESC";
        $cond_cat = "";
        if($catID != ""){
            $cond_cat = "AND product_ref_categoryID = '$catID'";
        } else {
            $cond_cat = "";
        }
        if($sort == 'def'){
            $cond_sort = "ORDER BY product_createdDate DESC";
        } else if ($sort == 'asc'){
            $cond_sort = "ORDER BY product_name ASC";
        } else if ($sort == 'desc'){
            $cond_sort = "ORDER BY product_name DESC";
        } else if ($sort == 'priceasc'){
            $cond_sort = "ORDER BY product_price ASC";
        } else if ($sort == 'pricedesc'){
            $cond_sort = "ORDER BY product_price DESC";
        } else {
            $cond_sort = "ORDER BY product_createdDate DESC";
        }
        $text_total = "SELECT product_ID, product_name, product_price, product_shortDesc, product_weight, product_specialPrice, product_trending, product_bestSeller, product_freeShipping, product_limitedStock, product_limitedEdition, photo_imgLoc, photo_imgLocThmb, product_publish, product_ref_categoryID, category_name 
            FROM $this->table $this->join LEFT JOIN T_PHOTO ON product_ID = photo_productID WHERE product_publish = 'Publish' AND photo_main = 1 $cond_cat GROUP BY product_ID";
        $query_total = mysql_query($text_total);
        $total_data = mysql_num_rows($query_total);
        if($total_data < 1){$total_data = 0;}
        //get total page
        $total_page = ceil($total_data / $show);
        if($page <= 1 || $page == null){
            $limitBefore = 0;
        }else{
            $limitBefore = ($page-1) * $show;
        }
        $text = "SELECT product_ID, product_name, product_price, product_shortDesc, product_weight, product_specialPrice, product_trending, product_bestSeller, product_freeShipping, product_limitedStock, product_limitedEdition, photo_imgLoc, photo_imgLocThmb, product_publish, product_ref_categoryID, category_name 
            FROM $this->table $this->join LEFT JOIN T_PHOTO ON product_ID = photo_productID WHERE product_publish = 'Publish' AND photo_main = 1 $cond_cat GROUP BY product_ID $cond_sort LIMIT $limitBefore, $show";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            $loop = 0;
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }   
        if(is_array($result)){
            $result[0]['total_page'] = $total_page;
            $result[0]['total_data_all'] = $total_data;
            $result[0]['total_data'] = count($result);
        }
        return $result;
    }*/

    public function get_latest_by_page($page=1){
        $result = 0;

        //get total data
        $text_total = "SELECT product_ID FROM $this->table WHERE product_publish = 'Publish'";
        $query_total = mysql_query($text_total);
        $total_data = mysql_num_rows($query_total);
        if($total_data < 1){$total_data = 0;}

        //get total page
        $total_page = ceil($total_data / $this->itemPerPage);
        $limitBefore = $page <= 1 || $page == null ? 0 : ($page-1) * $this->itemPerPage;

        $text = "SELECT product_ID, product_name, product_limitedStock, product_desc, product_specialPrice, product_price, 
            product_shortDesc, product_publish, product_weight, category_ID, category_name, category_slug, photo_imgLoc, 
            photo_imgLocThmb, photo_title FROM $this->table $this->join $this->joinPhoto WHERE product_publish = 'Publish' 
            AND photo_main = 1 GROUP BY product_ID ORDER BY product_createdDate DESC LIMIT $limitBefore, $this->itemPerPage";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            $loop = 0;
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }   
        if(is_array($result)){
            $result[0]['total_page'] = $total_page;
            $result[0]['total_data_all'] = $total_data;
            $result[0]['total_data'] = count($result);
        }
        return $result;
    }

    /*public function get_sale(){
        $text = "SELECT product_ID, product_name, product_price, product_shortDesc, product_weight, product_specialPrice, product_trending, product_bestSeller, product_freeShipping, product_limitedStock, product_limitedEdition, photo_imgLoc, photo_imgLocThmb, product_publish, product_ref_categoryID, category_name 
            FROM $this->table $this->join LEFT JOIN T_PHOTO ON product_ID = photo_productID WHERE product_publish = 'Publish' AND  photo_main = 1 AND product_specialPrice != '0.00' GROUP BY product_ID ORDER BY product_createdDate DESC LIMIT 8";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            $loop = 0;
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }   
        return $result;
    }*/
//END FUNCTION FOR CLIENT PAGE

//START FUNCTION FOR ADMIN PAGE
	public function get_data_by_page($page=1, $tag="all", $catID=null){
        $result = 0;  
        if($tag == "productTrending"){
        	$condTag = "AND product_trending = '1'";
        }else if($tag == "productBestSeller"){
        	$condTag = "AND product_bestSeller = '1'";
        }else if($tag == "productFreeShipping"){
        	$condTag = "AND product_freeShipping = '1'";
        }else if($tag == "productOutOfStock"){
        	$condTag = "AND product_limitedStock = '1'";
        }else if($tag == "productlimitedEdition"){
        	$condTag = "AND product_limitedEdition = '1'";
        }else if($tag == "productSpecialPrice"){
        	$condTag = "AND product_specialPrice != '0.00'";
        }else{
        	$condTag = "";
        }   

        if($catID != null){
            $condCat = "AND category_ID = '$catID'";
        }else{
            $condCat = "";
        }

        //get total data
        $text_total = "SELECT product_ID FROM $this->table $this->join WHERE product_ID != 0 $condTag $condCat GROUP BY product_ID";
        $query_total = mysql_query($text_total);
        $total_data = mysql_num_rows($query_total);
        if($total_data < 1){$total_data = 0;}

        //get total page
        $total_page = ceil($total_data / $this->itemPerPageAdmin);
        $limitBefore = $page <= 1 || $page == null ? 0 : ($page-1) * $this->itemPerPageAdmin;

        $text = "SELECT product_ID, product_name, product_price, product_specialPrice, product_trending, product_bestSeller, 
            category_name, product_freeShipping, product_limitedStock, product_limitedEdition, photo_imgLoc, photo_imgLocThmb 
            FROM $this->table $this->joinPhoto $this->join WHERE photo_main = 1 $condTag $condCat GROUP BY product_ID 
            ORDER BY product_ID DESC LIMIT $limitBefore, $this->itemPerPageAdmin";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }
        
        if(is_array($result)){
            $result[0]['total_page'] = $total_page;
            $result[0]['total_data_all'] = $total_data;
            $result[0]['total_data'] = count($result);
        }
        //$result = $text;
        return $result;
    }

	public function get_data_detail($id){
		$result = 0;

		$text = "SELECT product_ID, product_name, product_desc, product_price, product_specialPrice, product_weight, product_shortDesc, 
            product_tag, product_trending, product_bestSeller, product_freeShipping, product_limitedStock, product_limitedEdition, 
            product_publish, product_ref_categoryID FROM $this->table WHERE product_ID = '$id'";  
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
            $loop = 0;
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;

                //query product photo
                $text_photo = "SELECT photo_ID, photo_productID, photo_title, photo_imgLoc, photo_imgLocThmb, photo_main 
                    FROM T_PHOTO WHERE photo_productID = '$id' ORDER BY photo_main DESC, photo_ID DESC";
                $query_photo = mysql_query($text_photo);
                if(mysql_num_rows($query_photo) >= 1){
                    while($row_photo = mysql_fetch_array($query_photo, MYSQL_ASSOC)){
                        $result[$loop]['photo'][] = $row_photo;
                    }
                }
                $loop++;
            }
		}
        //$result = $text;
		return $result;
	}

    /*public function get_data_photo($id){
        $result = 0;

        
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }
        //$result = $text;
        return $result;
    }

    public function get_data_color($id){
        $result = 0;

        $text = "SELECT color_ID, color_productID, color_title, color_name, color_img, color_imgThmb FROM T_COLOR WHERE color_productID = '$id' 
            ORDER BY color_createDate ASC";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }
        //$result = $text;
        return $result;
    }*/

	public function insert_data($category_id, $name, $desc, $price, $special_price, $weight, $short_desc, $tag, $trending, $best_seller, $limitedStock, $publish){
		$result = 0;
        date_default_timezone_set('Asia/Jakarta');
        $now = date("Y-m-d H:i:s");

		$text = "INSERT INTO $this->table (product_ref_categoryID, product_name, product_desc, product_price, product_specialPrice, product_weight, product_shortDesc, product_tag, product_trending, product_bestSeller, product_limitedStock, product_createdDate, product_publish) VALUES('$category_id', '$name', '$desc', '$price', '$special_price', '$weight', '$short_desc', '$tag', '$trending', '$best_seller', '$limitedStock', '$now', '$publish')";
		$query = mysql_query($text);
		if($query){
			$result = mysql_insert_id();
		}
        //$result = $text;
		return $result;
	}

	public function update_index($id, $trending, $best_seller, $limitedStock, $price, $special_price){
		$result = 0;
		$text = "UPDATE $this->table SET product_trending = '$trending', product_bestSeller = '$best_seller', product_limitedStock = '$limitedStock', product_price = '$price', product_specialPrice = '$special_price' WHERE product_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		//$result = $text;
		return $result;
	}

	public function update_data($id, $category_id, $name, $desc, $price, $special_price, $weight, $short_desc, $tag, $trending, $best_seller, $limitedStock, $publish){
		$result = 0;

		$text = "UPDATE $this->table SET product_ref_categoryID = '$category_id', product_name = '$name', product_desc = '$desc', product_price = '$price', product_specialPrice = '$special_price', product_weight = '$weight', product_shortDesc = '$short_desc', product_tag = '$tag', product_trending = '$trending', product_bestSeller = '$best_seller', product_limitedStock = '$limitedStock', product_publish = '$publish' WHERE product_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

	public function delete_data($id, $path){
		$result = 0;
        $this->remove_photo($id, $path); //remove photo product related
        $this->delete_data_photo($id); //delete data photo product related
        //$this->delete_data_category($id); //delete data category product related
        //$this->delete_data_color($id); //delete data color product related

		$text = "DELETE FROM $this->table WHERE product_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

    public function delete_data_photo($id){
        $result = 0;

        $text = "DELETE FROM T_PHOTO WHERE photo_productID = '$id'";
        $query = mysql_query($text);
        if(mysql_affected_rows() == 1){
            $result = 1;
        }
        return $result;
    }

    /*public function delete_data_category($id){
        $result = 0;

        $text = "DELETE FROM AT_CATEGORY WHERE at_productID = '$id'";
        $query = mysql_query($text);
        if(mysql_affected_rows() == 1){
            $result = 1;
        }
        return $result;
    }

    public function delete_data_color($id){
        $result = 0;

        $text = "DELETE FROM T_COLOR WHERE color_productID = '$id'";
        $query = mysql_query($text);
        if(mysql_affected_rows() == 1){
            $result = 1;
        }
        return $result;
    }*/

	public function remove_photo($id, $path){
        $result = 0;
        $flag_img = 0;
        $flag_img_thmb = 0;
        $text = "SELECT photo_imgLoc, photo_imgLocThmb FROM T_PHOTO WHERE photo_productID = '$id'";
        $query = mysql_query($text);
        while ($row = mysql_fetch_array($query, MYSQL_ASSOC)) 
        {
            $deleteImg = $path.$row['photo_imgLoc'];
            if (file_exists($deleteImg)) {
                unlink($deleteImg);
                $flag_img = 1;
            }
            $deleteImgThmb = $path.$row['photo_imgLocThmb'];
            if (file_exists($deleteImgThmb)) {
                unlink($deleteImgThmb);
                $flag_img_thmb = 1;
            }
            if($flag_img == 1 && $flag_img_thmb ==1){
                $result = 1;
            }
        }
        return $result;
    }
//END FUNCTION FOR ADMIN PAGE
}
?>