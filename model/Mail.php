<?php
class Mail{

	public function payment($po_name, $J_bank, $loop, $datas, $data_details, $confirm_payment){
            $content_bank = "";
            if($datas[0]['po_type'] == "bank"){
                $content_bank = "<div><b>Please make your payment to :</b></div>
                    <div style='margin: 20px 0 30px 0 !important;'>
                    ".$J_bank."
                    </div>
                    <div style='margin-bottom:12px;'><b>YOUR ORDER WILL BE EXPIRED WITHIN 2 X 24 HOURS.</b></div>
                    <div style='margin-bottom:12px;'>Already make a payment? Confirm it <a href='".$confirm_payment."' class='here'>here</a></div>
                    </div>";
            }
            if($datas[0]['po_couponName'] != ""){
                $coupon_text = "
                    <tr>
                    <td class='text-right' colspan='3'><strong>Sub-Total:</strong></td>
                    <td class='text-right'>-Rp.".number_format($datas[0]['po_couponPrice'],0,'.',',')."</td>
                    </tr>
                ";
            } else {
                $coupon_text = "";
            }
            $result = "
                <!DOCTYPE html>
                <html>
                <head>
                <title></title>
                <style type='text/css'>
                a{
                    color:#010101 !important;
                }
                .here{
                    color: #fc8da4 !important;
                    text-decoration: underline;
                }
                .cart-wrapper{
                    width: 700px;
                    margin: 0 auto;
                }
                .thank-head {
                    font-size: 24px;
                    text-align: center;
                    color: #fc8da4;
                    margin-bottom: 15px;
                }
                .thank-content strong {
                    color: #fc8da4;
                }
                .thank-content {
                    font-size: 16px;
                }
                .thank-thead {
                    font-size: 18px;
                    font-weight: bold;
                    margin-bottom: 10px;
                    color: #fc8da4;
                }
                .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
                    padding: 8px;
                    line-height: 1.42857143;
                    vertical-align: top;
                    border-top: 1px solid #ddd;
                }
                .table-checkout th {
                    color: #fc8da4;
                    font-weight: normal;
                    font-size: 16px;
                }
                .text-center{
                    text-align: center;
                }
                .text-left{
                    text-align: left;
                }
                .text-right{
                    text-align: right;
                }
                .table{
                    width: 100%;
                    border-spacing: 0;
                }
                .table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
                    border: 1px solid #ddd;
                }
                .thumbnail {
                display: block;
                padding: 4px;
                margin-bottom: 20px;
                line-height: 1.42857143;
                background-color: #fff;
                border: 1px solid #ddd;
                border-radius: 4px;
                min-height: 200px;
                -webkit-transition: border .2s ease-in-out;
                -o-transition: border .2s ease-in-out;
                transition: border .2s ease-in-out;
                }
                .well {
                min-height: 20px;
                padding: 19px;
                margin-bottom: 20px;
                background-color: #f5f5f5;
                border: 1px solid #e3e3e3;
                border-radius: 4px;
                -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
                box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
                }
                </style>
                </head>
                <body>
                <div class='cart-wrapper'>
                <h3 class='thank-head'>Thank You For Your Purchase!</h3>
                <div class='thank-content'>
                <div>Order Number : <strong>".$po_name."</strong></div>
                <div>You can go to your account to check your order status.</div>
                <div><b>We will process your order once we have confirmed you payment.</b></div>
                <div style='display:table;width:100%;margin-top:15px'>
                    <div style='display:table-cell;width:50%;padding-right:15px;'>
                        <div class='thumbnail'>
                            <div class='well bold'>BILLING INFO</div>
                            <div style='padding: 0 20px 20px 20px;''>
                                <div><span class='bold'>Full Name</span> : ".$data_details[0]['customer_fname']." </div>
                                <div class='up1'><span class='bold'>Phone</span> : ".$data_details[0]['customer_phone']." </div>
                                <div class='up1'><span class='bold'>E-Mail</span> : ".$data_details[0]['customer_email']." </div>
                            </div>
                        </div>
                    </div>
                    <div style='display:table-cell;width:50%;padding-left:15px;'>
                        <div class='thumbnail'>
                            <div class='well bold'>SHIP TO</div>
                            <div style='padding: 0 20px 20px 20px;margin: 0;''>
                                ".$data_details[0]['po_addressStreet1']."<br>
                                ".$data_details[0]['po_addressStreet2']."<br>
                                ".$data_details[0]['po_addressCity'].". ".$data_details[0]['po_addressState'].".<br>
                                ".$data_details[0]['po_addressCountry'].". ".$data_details[0]['po_addressZip'].".
                            </div>
                        </div>
                    </div>
                </div>
                ".$content_bank."
                <div class='thank-thead'>Summary Of Your Order</div>
                <div class='table-responsive table-checkout'>
                <table class='table table-bordered table-hover'>
                <thead>
                <tr>
                <th class='text-left'>Product Name</th>
                <th class='text-right'>Quantity</th>
                <th class='text-right'>Unit Price</th>
                <th class='text-right'>Total</th>
                </tr>
                </thead>
                <tbody>
                ".$loop."
                </tbody>
                <tfoot>
                <tr>
                <td class='text-right' colspan='3'><strong>Sub-Total:</strong></td>
                <td class='text-right'>Rp.".number_format($datas[0]['po_amount'],0,'.',',')."</td>
                </tr>
                <tr>
                <td class='text-right' colspan='3'><strong>Shipping ( ".$datas[0]['po_shippingWeight']." kg )( ".$datas[0]['po_service']." ):</strong></td>
                <td class='text-right'>Rp.".number_format($datas[0]['po_shippingCost'],0,'.',',')."</td>
                </tr>
                ".$coupon_text."
                <tr>
                <td class='text-right' colspan='3'><strong>Total:</strong></td>
                <td class='text-right'>Rp.".number_format($datas[0]['po_totalAmount'],0,'.',',')."</td>
                </tr>
                </tfoot>
                </table>
                </div>
                </div>
                </body>
                </html>
                ";
            return $result;
	}

      public function paymentAdmin($po_name, $loop, $datas, $data_details){
            if($datas[0]['po_couponName'] != ""){
                $coupon_text = "
                    <tr>
                    <td class='text-right' colspan='3'><strong>Sub-Total:</strong></td>
                    <td class='text-right'>-Rp.".number_format($datas[0]['po_couponPrice'],0,'.',',')."</td>
                    </tr>
                ";
            } else {
                $coupon_text = "";
            }
            $result = "
                <!DOCTYPE html>
                <html>
                <head>
                <title></title>
                <style type='text/css'>
                a{
                    color:#010101 !important;
                }
                .here{
                    color: #fc8da4 !important;
                    text-decoration: underline;
                }
                .cart-wrapper{
                    width: 700px;
                    margin: 0 auto;
                }
                .thank-head {
                    font-size: 24px;
                    text-align: center;
                    color: #fc8da4;
                    margin-bottom: 15px;
                }
                .thank-content strong {
                    color: #fc8da4;
                }
                .thank-content {
                    font-size: 16px;
                }
                .thank-thead {
                    font-size: 18px;
                    font-weight: bold;
                    margin-bottom: 10px;
                    color: #fc8da4;
                }
                .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
                    padding: 8px;
                    line-height: 1.42857143;
                    vertical-align: top;
                    border-top: 1px solid #ddd;
                }
                .table-checkout th {
                    color: #fc8da4;
                    font-weight: normal;
                    font-size: 16px;
                }
                .text-center{
                    text-align: center;
                }
                .text-left{
                    text-align: left;
                }
                .text-right{
                    text-align: right;
                }
                .table{
                    width: 100%;
                    border-spacing: 0;
                }
                .table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
                    border: 1px solid #ddd;
                }
                .thumbnail {
                display: block;
                padding: 4px;
                margin-bottom: 20px;
                line-height: 1.42857143;
                background-color: #fff;
                border: 1px solid #ddd;
                border-radius: 4px;
                min-height: 200px;
                -webkit-transition: border .2s ease-in-out;
                -o-transition: border .2s ease-in-out;
                transition: border .2s ease-in-out;
                }
                .well {
                min-height: 20px;
                padding: 19px;
                margin-bottom: 20px;
                background-color: #f5f5f5;
                border: 1px solid #e3e3e3;
                border-radius: 4px;
                -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
                box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
                }
                </style>
                </head>
                <body>
                <div class='cart-wrapper'>
                <h3 class='thank-head'>Order Information From Volia!</h3>
                <div class='thank-content'>
                <div>Order Number : <strong>".$po_name."</strong></div>
                <div>New order has been made.</div>
                <div><b>Please check your administration for new order.</b></div>
                <div style='display:table;width:100%;margin-top:15px'>
                    <div style='display:table-cell;width:50%;padding-right:15px;'>
                        <div class='thumbnail'>
                            <div class='well bold'>BILLING INFO</div>
                            <div style='padding: 0 20px 20px 20px;''>
                                <div><span class='bold'>Full Name</span> : ".$data_details[0]['customer_fname']." </div>
                                <div class='up1'><span class='bold'>Phone</span> : ".$data_details[0]['customer_phone']." </div>
                                <div class='up1'><span class='bold'>E-Mail</span> : ".$data_details[0]['customer_email']." </div>
                            </div>
                        </div>
                    </div>
                    <div style='display:table-cell;width:50%;padding-left:15px;'>
                        <div class='thumbnail'>
                            <div class='well bold'>SHIP TO</div>
                            <div style='padding: 0 20px 20px 20px;margin: 0;''>
                                ".$data_details[0]['po_addressStreet1']."<br>
                                ".$data_details[0]['po_addressStreet2']."<br>
                                ".$data_details[0]['po_addressCity'].". ".$data_details[0]['po_addressState'].".<br>
                                ".$data_details[0]['po_addressCountry'].". ".$data_details[0]['po_addressZip'].".
                            </div>
                        </div>
                    </div>
                </div>
                </div>
                <div class='thank-thead'>Summary Of Your Order</div>
                <div class='table-responsive table-checkout'>
                <table class='table table-bordered table-hover'>
                <thead>
                <tr>
                <th class='text-left'>Product Name</th>
                <th class='text-right'>Quantity</th>
                <th class='text-right'>Unit Price</th>
                <th class='text-right'>Total</th>
                </tr>
                </thead>
                <tbody>
                ".$loop."
                </tbody>
                <tfoot>
                <tr>
                <td class='text-right' colspan='3'><strong>Sub-Total:</strong></td>
                <td class='text-right'>Rp.".number_format($datas[0]['po_amount'],0,'.',',')."</td>
                </tr>
                <tr>
                <td class='text-right' colspan='3'><strong>Shipping ( ".$datas[0]['po_shippingWeight']." kg )( ".$datas[0]['po_service']." ):</strong></td>
                <td class='text-right'>Rp.".number_format($datas[0]['po_shippingCost'],0,'.',',')."</td>
                </tr>
                ".$coupon_text."
                <tr>
                <td class='text-right' colspan='3'><strong>Total:</strong></td>
                <td class='text-right'>Rp.".number_format($datas[0]['po_totalAmount'],0,'.',',')."</td>
                </tr>
                </tfoot>
                </table>
                </div>
                </div>
                </body>
                </html>
                ";
            return $result;
      }
}
?>