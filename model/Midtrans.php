<?php
class Midtrans{
    
    function getTransactionStatus($base_url, $order_id, $server_key){
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => $base_url."/".$order_id."/status",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
            "Accept: application/json",
            "Authorization: Basic ".$server_key
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            $result = 'error';
            return 'error';
        } else {
            return json_decode(trim($response), TRUE);
        }
    }

}
?>