<?php
class Banner{
	private $table = "T_BANNER";
		public function get_by_page($webpage, $webposition){
			$result = 0;
			$text = "SELECT * FROM $this->table WHERE banner_page = '$webpage' AND banner_position = '$webposition' AND banner_publish = 'Publish' ";
			$query = mysql_query($text);
			$total_data = mysql_num_rows($query);
			if(mysql_num_rows($query) >= 1){
				$result = array();
				while($row = mysql_fetch_assoc($query)){
					$result[] = $row;
				}
			}
			return $result;
		}
//START FUNCTION FOR ADMIN PAGE
	public function get_data(){
		$result = 0;
		$text = "SELECT * FROM $this->table ORDER BY banner_ID ASC";
		$query = mysql_query($text);
		$total_data = mysql_num_rows($query);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		if(is_array($result)){
            $result[0]['total_data'] = $total_data;
        }
		return $result;
	}
	public function get_data_detail($id){
		$result = 0;
		$text = "SELECT * FROM $this->table WHERE banner_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		return $result;
	}	
	public function insert_data($title, $img, $img_thmb, $link, $position){
		$result = 0;
		$text = "INSERT INTO $this->table (banner_title, banner_img, banner_imgThmb, banner_url, banner_postion, banner_size) VALUES('$title', '$img', '$img_thmb', '$link')";
		$query = mysql_query($text);
		if($query){
			$result = 1;
		}
		return $result;
	}
	public function update_data($id, $title, $link, $publish){
		$result = 0;
		$text = "UPDATE $this->table SET banner_title = '$title', banner_url = '$link', banner_publish='$publish' WHERE banner_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}
	public function update_data_image($id, $title, $img, $img_thmb, $link, $publish){
		$result = 0;
		
		$textImg = "SELECT banner_ID, banner_img, banner_imgThmb FROM $this->table WHERE banner_ID = '$id'";
		$queryImg = mysql_query($textImg);
		if(mysql_num_rows($queryImg) == 1){
			while($row = mysql_fetch_assoc($queryImg)){
				$bannerImage = $row['banner_img'];
			}
		}
		if($bannerImage != ""){
			$this->remove_image($id); //remove image before
		}

		$text = "UPDATE $this->table SET banner_title = '$title', banner_img = '$img', banner_imgThmb = '$img_thmb', banner_url = '$link', banner_publish='$publish' WHERE banner_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}
	public function delete_data($id){
		$result = 0;
		$this->remove_image($id); //remove image before
		$text = "DELETE FROM $this->table WHERE banner_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}
	public function remove_image($id){
		$result = 0;
		$flag_img = 0;
		$flag_img_thmb = 0;
		$text = "SELECT banner_ID, banner_img, banner_imgThmb FROM $this->table WHERE banner_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_num_rows($query) == 1){
			$row = mysql_fetch_assoc($query);
            $deleteImg = $_SERVER['DOCUMENT_ROOT']."/".$row['banner_img'];
            if (file_exists($deleteImg)) {
                unlink($deleteImg);
                $flag_img = 1;
            }
            $deleteImgThmb = $_SERVER['DOCUMENT_ROOT']."/".$row['banner_imgThmb'];
            if (file_exists($deleteImgThmb)) {
                unlink($deleteImgThmb);
                $flag_img_thmb = 1;
            }
            if($flag_img == 1 && $flag_img_thmb ==1){
            	$result = 1;
            }
		}
		return $result;
	}
//END FUNCTION FOR ADMIN PAGE
}
?>