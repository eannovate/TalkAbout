<?php
class Blog{

	private $table = "T_BLOG";
	private $itemPerPageAdmin = 12;
	private $itemPerPageClient = 3;
	private $join = "LEFT JOIN T_BLOG_CATEGORY ON blog_categoryID = blogcat_ID";

//START FUNCTION FOR CLIENT PAGE
	//function get blog in explore.php
	public function get_blog($page=1){
		$result = 0;

		//get total data
        $text_total = "SELECT blog_ID FROM $this->table WHERE blog_publish = 'Publish' ORDER BY blog_createDate DESC";
        $query_total = mysql_query($text_total);
        $total_data = mysql_num_rows($query_total);
        if($total_data < 1){$total_data = 0;}

        //get total page
        $total_page = ceil($total_data / $this->itemPerPageClient);
        $limitBefore = $page <= 1 || $page == null ? 0 : ($page-1) * $this->itemPerPageClient;
       
		$text = "SELECT blog_ID, blog_title, blog_desc, blog_url_video, blog_author, blog_author_link, blog_publish, 
			blog_img, blog_imgThmb, blog_createDate, blogcat_ID, blogcat_name FROM $this->table $this->join 
			WHERE blog_publish = 'Publish' GROUP BY blog_ID ORDER BY blog_createDate DESC LIMIT $limitBefore, 
			$this->itemPerPageClient";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		if(is_array($result)){
            $result[0]['total_page'] = $total_page;
            $result[0]['total_data_all'] = $total_data;
            $result[0]['total_data'] = count($result);
        }
        //$result = $text;
		return $result;
	}

	//function get blog detail in explore-detail.php
    public function get_blog_detail($id){
        $result = 0;

        $text = "SELECT blog_ID, blog_title, blog_desc, blog_publish, blog_author, blog_author_link, blog_url_video,
        	blog_img, blog_imgThmb, blog_createDate, blogcat_ID, blogcat_name FROM $this->table $this->join 
        	WHERE blog_publish = 'Publish' AND blog_ID = '$id'";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            $loop = 0;
            while($row = mysql_fetch_assoc($query)){
                $result[$loop] = $row;

                //popular
                $text_popular = "SELECT blog_ID, blog_title, blog_img, blog_imgThmb FROM $this->table 
                	WHERE blog_publish = 'Publish' AND blog_ID != '{$row['blog_ID']}' ORDER BY blog_num_view DESC LIMIT 0, 4";
                $query_popular = mysql_query($text_popular);
                if(mysql_num_rows($query_popular) >= 1){
                    while($row_popular = mysql_fetch_array($query_popular, MYSQL_ASSOC)){
                        $result[$loop]['popular'][] = $row_popular;
                    }
                }

                //related
                $text_related = "SELECT blog_ID, blog_title, blog_img, blog_imgThmb FROM $this->table 
                	WHERE blog_publish = 'Publish' AND blog_categoryID = '{$row['blogcat_ID']}' AND 
                	blog_ID != '{$row['blog_ID']}' ORDER BY blog_createDate DESC LIMIT 0, 3";
                $query_related = mysql_query($text_related);
                if(mysql_num_rows($query_related) >= 1){
                    while($row_related = mysql_fetch_array($query_related, MYSQL_ASSOC)){
                        $result[$loop]['related'][] = $row_related;
                    }
                }

                $loop++;
            }
        }
        //$result = $text;
        return $result;
    }
//END FUNCTION FOR CLIENT PAGE

//START FUNCTION FOR ADMIN PAGE
	public function get_data_by_page($page=1){
		$result = 0;

		//get total data
        $text_total = "SELECT blog_ID FROM $this->table ORDER BY blog_createDate DESC";
        $query_total = mysql_query($text_total);
        $total_data = mysql_num_rows($query_total);
        if($total_data < 1){$total_data = 0;}

        //get total page
        $total_page = ceil($total_data / $this->itemPerPageAdmin);
        $limitBefore = $page <= 1 || $page == null ? 0 : ($page-1) * $this->itemPerPageAdmin;
       
		$text = "SELECT blog_ID, blog_title, blog_publish, blog_img, blog_imgThmb, blog_createDate, blogcat_name 
			FROM $this->table $this->join ORDER BY blog_createDate DESC LIMIT $limitBefore, $this->itemPerPageAdmin";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		if(is_array($result)){
            $result[0]['total_page'] = $total_page;
            $result[0]['total_data_all'] = $total_data;
            $result[0]['total_data'] = count($result);
        }
        //$result = $text;
		return $result;
	}

	public function get_data_detail($id){
		$result = 0;

		$text = "SELECT * FROM $this->table WHERE blog_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		return $result;
	}

	public function insert_data($category_id, $title, $desc, $writer, $writer_url, $video, $img, $img_thmb, $publish){
		$result = 0;

		$text = "INSERT INTO $this->table (blog_categoryID, blog_title, blog_desc, blog_author, blog_author_link, 
			blog_url_video, blog_img, blog_imgThmb, blog_publish, blog_createDate) VALUES('$category_id', '$title', '$desc', 
			'$writer', '$writer_url', '$video', '$img', '$img_thmb', '$publish', NOW())";
		$query = mysql_query($text);
		if($query){
			$result = mysql_insert_id();
		}
		return $result;
	}

	public function update_data($id, $category_id, $title, $desc, $writer, $writer_url, $video, $publish){
		$result = 0;
	
		$text = "UPDATE $this->table SET blog_categoryID = '$category_id', blog_title = '$title', blog_desc = '$desc', 
			blog_author = '$writer', blog_author_link = '$writer_url', blog_url_video = '$video', blog_publish = '$publish' 
			WHERE blog_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

	public function update_data_image($id, $category_id, $title, $desc, $writer, $writer_url, $video, $img, $img_thmb, $publish){
		$result = 0;
		$this->remove_image($id); //remove image before

		$text = "UPDATE $this->table SET blog_categoryID = '$category_id', blog_title = '$title', blog_desc = '$desc', 
			blog_author = '$writer', blog_author_link = '$writer_url', blog_url_video = '$video', blog_img = '$img', 
			blog_imgThmb = '$img_thmb', blog_publish = '$publish' WHERE blog_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

	public function delete_data($id){
		$result = 0;
		$this->remove_image($id); //remove image before		

		$text = "DELETE FROM $this->table WHERE blog_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}	

	public function remove_image($id){
		$result = 0;
		$flag_img = 0;
		$flag_img_thmb = 0;
		$text = "SELECT blog_ID, blog_img, blog_imgThmb FROM $this->table WHERE blog_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_num_rows($query) == 1){
			$row = mysql_fetch_assoc($query);
            $deleteImg = $_SERVER['DOCUMENT_ROOT']."/".$row['blog_img'];
            if (file_exists($deleteImg)) {
                unlink($deleteImg);
                $flag_img = 1;
            }

            $deleteImgThmb = $_SERVER['DOCUMENT_ROOT']."/".$row['blog_imgThmb'];
            if (file_exists($deleteImgThmb)) {
                unlink($deleteImgThmb);
                $flag_img_thmb = 1;
            }

            if($flag_img == 1 && $flag_img_thmb ==1){
            	$result = 1;
            }
		}
		return $result;
	}
//END FUNCTION FOR ADMIN PAGE
}
?>