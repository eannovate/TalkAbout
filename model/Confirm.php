<?php
class Confirm{
	private $table = "T_CONFIRM";
	private $itemPerPageAdmin = 10;
//START FUNCTION FOR CLIENT PAGE
    public function insert_data($confirm_name, $confirm_customerID, $confirm_poID, $confirm_amount, $confirm_date, $confirm_bank, $confirm_account, $confirm_accountName, $confirm_note){
        $result = 0;
        $text = "INSERT INTO $this->table (confirm_name, confirm_customerID, confirm_poID, confirm_amount, confirm_date, confirm_bank, confirm_account, confirm_accountName, confirm_note, confirm_status, confirm_createDate) 
            VALUES('$confirm_name', '$confirm_customerID', '$confirm_poID', '$confirm_amount', '$confirm_date', '$confirm_bank', '$confirm_account', '$confirm_accountName', '$confirm_note', '0', NOW())";
        $query = mysql_query($text);
        if($query){
            $result = mysql_insert_id();
        }
        return $result;
    }
    //function generate confirm in confirm-payment.php
    public function generate_confirm(){
        $result = 0;
        $rand = strtoupper(substr(md5(microtime()),rand(0,26),5));
        $text = "SELECT confirm_ID FROM T_CONFIRM WHERE confirm_createDate = CURDATE()";
        $query = mysql_query($text);
        if($query){
            $total_data = mysql_num_rows($query);
            if($total_data > 0){
                $num = $total_data + 1;
                $result = "CO".$rand."-".$num;
            } else {
                $result = "CO".$rand."-1";
            }
        } else {
            $result = "CO".$rand."-1";
        }
        return $result;
    }
//END FUNCTION FOR CLIENT PAGE
//START FUNCTION FOR ADMIN PAGE
	public function get_data_by_page($page=1){
        $result = 0;    
        //get total data
        $text_total = "SELECT confirm_ID FROM $this->table";
        $query_total = mysql_query($text_total);
        $total_data = mysql_num_rows($query_total);
        if($total_data < 1){$total_data = 0;}
        //get total page
        $total_page = ceil($total_data / $this->itemPerPageAdmin);
        if($page <= 1 || $page == null){
            $limitBefore = 0;
        }else{
            $limitBefore = ($page-1) * $this->itemPerPageAdmin;
        }
        $text = "SELECT confirm_ID, confirm_name, confirm_customerID, confirm_poID, confirm_amount, confirm_date, confirm_bank, confirm_bank, confirm_account, confirm_accountName, confirm_status, confirm_note, confirm_modifyDate, customer_fname, po_name, (po_amount + po_shippingCost) AS po_amount, customer_email 
        FROM $this->table LEFT JOIN T_CUSTOMER ON customer_ID = confirm_customerID LEFT JOIN T_PO ON po_ID = confirm_poID ORDER BY confirm_ID DESC LIMIT $limitBefore, $this->itemPerPageAdmin";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }
        
        if(is_array($result)){
            $result[0]['total_page'] = $total_page;
            $result[0]['total_data_all'] = $total_data;
            $result[0]['total_data'] = count($result);
        }
        return $result;
    }
    public function update_data($confID, $confDate, $confAmountPaid, $confBank, $confNoRek, $confRekName, $confNote, $confStat){
        $result = 0;
        $text = "UPDATE $this->table SET confirm_date='$confDate', confirm_amount='$confAmountPaid', confirm_bank='$confBank', confirm_account ='$confNoRek', confirm_accountName ='$confRekName', confirm_status ='$confStat', confirm_note = '$confNote' WHERE confirm_ID='$confID'";
        $query = mysql_query($text);
        if(mysql_affected_rows() == 1){
            $result = 1;
        }
        return $result;
    }
//END FUNCTION FOR ADMIN PAGE
}
?>